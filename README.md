# LibraryMgr

*Login and read your favorite books. Compatible with Windows, Unix and Gnu/Linux.*
![screenshot](logo_text.png)


## Build & run
1. You can run ```cmake``` with the [cmake file](librarymgr/CMakeLists.txt) provided
2. you can import the [project file](librarymgr/librarymgr.pro) to Qt and build there
3. (you'd better not) but you can simply ```g++ librarymgr/main.cpp``` from the repository root.


## Tests
As for now the project is too small for serious unit test development, however there are some tests on the core of the app:
- [library management](librarymgr/tests/library.h)
- [user authentication](librarymgr/tests/oauth.h)


## Docs
- browse the html pages [here](docs/html/index.html)
- view the ```.pdf```(s) about class hierarchy [here](docs/latex)

## License
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
