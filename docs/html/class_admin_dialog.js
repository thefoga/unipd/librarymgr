var class_admin_dialog =
[
    [ "AdminDialog", "class_admin_dialog.html#a45c7f0c8881a0ea2a04d6bc3b282ccdc", null ],
    [ "sendAddBookToLibraryRequest", "class_admin_dialog.html#a6d1942d76464b00b709539edbe17e10d", null ],
    [ "sendAddBookToLibraryRequestResult", "class_admin_dialog.html#aaa29836eddb9f3d0be605f862b8ce211", null ],
    [ "sendAddUserToLibraryRequest", "class_admin_dialog.html#a06c386ab5b7db457acdacf6c299d8206", null ],
    [ "sendAddUserToLibraryRequestResult", "class_admin_dialog.html#ac22ab7ba0236ae9cab7d6e597fa7a7e1", null ],
    [ "sendEditBookInLibraryRequest", "class_admin_dialog.html#ab84c85cd044cfee3558b899544c66e0b", null ],
    [ "sendEditBookInLibraryRequestResult", "class_admin_dialog.html#a953a7c34e5461a83d5d58cd62853f9c3", null ],
    [ "sendEditUserInLibraryRequest", "class_admin_dialog.html#a203d47d014f60e8dcff40d874ff3afb7", null ],
    [ "sendEditUserInLibraryRequestResult", "class_admin_dialog.html#a465dbd18879b4fb120cf82fee64dd403", null ],
    [ "sendRemoveBookFromLibraryRequest", "class_admin_dialog.html#ac524faf342b61d5aa002c013d4086d48", null ],
    [ "sendRemoveBookFromLibraryRequestResult", "class_admin_dialog.html#a019568d28985ba97e38b12899642986d", null ],
    [ "sendRemoveUserFromLibraryRequest", "class_admin_dialog.html#a4a9254bb1a24da17c68c49126a4e5c41", null ],
    [ "sendRemoveUserFromLibraryRequestResult", "class_admin_dialog.html#ac47dfcd00870a5ed4e29c05a1eed84a7", null ],
    [ "sendSearchUserRequest", "class_admin_dialog.html#a68c381c09fae66cfeda9419dc95f9cbc", null ],
    [ "sendSearchUserRequestResult", "class_admin_dialog.html#a2efc7720df8499ce5cb5bacf234150ef", null ],
    [ "sendUserRequest", "class_admin_dialog.html#a543c733c8ed1ce351516ab8d205bad7f", null ]
];