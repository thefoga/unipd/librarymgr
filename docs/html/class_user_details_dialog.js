var class_user_details_dialog =
[
    [ "UserDetailsDialog", "class_user_details_dialog.html#a75103e708a793f9d061134d0b698ec3b", null ],
    [ "getUser", "class_user_details_dialog.html#a8c7b51e1d31fe8cba8211732563f7112", null ],
    [ "populate", "class_user_details_dialog.html#a4f05d113fed6ae53bf530b4b213f56c1", null ],
    [ "setup", "class_user_details_dialog.html#a26d57836ff8ace4f2ab9e72669873e13", null ],
    [ "birthDate", "class_user_details_dialog.html#a6660c4e9c01c412fb177cc3c0f0242b3", null ],
    [ "emailLabel", "class_user_details_dialog.html#a05ee39e3cefb1177b7adfa92a6be9e88", null ],
    [ "nameLabel", "class_user_details_dialog.html#ae3979ca6997afb33503814af270ce94d", null ],
    [ "surnameLabel", "class_user_details_dialog.html#ab4e3f5dc2f481961e053110f2f85255d", null ]
];