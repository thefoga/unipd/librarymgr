var searchData=
[
  ['importwishlistfromfile',['importWishlistFromFile',['../class_user_preferences_widget.html#a9803e6f3072828c2a7a153f4cffccaa5',1,'UserPreferencesWidget']]],
  ['inlinelabel',['InlineLabel',['../class_inline_label.html#adf41b79c4a8ffaf9432acf997bbc9dbb',1,'InlineLabel']]],
  ['iostream',['IOStream',['../class_i_o_stream.html#aa59f4d583a53d0084d42665c80373acd',1,'IOStream']]],
  ['isadmin',['isAdmin',['../class_user.html#ace19848c26b4b131c6f7b48d7b146ffc',1,'User']]],
  ['isavailabletousers',['isAvailableToUsers',['../class_book.html#a4cdc9d69b29066a22c3dbfdd902b4d81',1,'Book']]],
  ['isbookedbyuser',['isBookedByUser',['../class_book.html#a3ae9e7c3540adf5d0bb3f2b38f3484fb',1,'Book']]],
  ['isbookindatabase',['isBookInDatabase',['../class_library.html#a9b67293035c9287e553fdf375c2c07d4',1,'Library']]],
  ['isbookinwishlist',['isBookInWishlist',['../class_user.html#adb9a76bda3d20fb82db91855be66d620',1,'User']]],
  ['isbookreservedbyuser',['isBookReservedByUser',['../class_user.html#aa01f3ad7431073992a545da9660fa3ad',1,'User']]],
  ['isempty',['isEmpty',['../class_database.html#ab4a4b719e7cb7bdae488f56aab57c2c2',1,'Database']]],
  ['isindatabase',['isInDatabase',['../class_database.html#a9e868f7b561ee7bec6dad42c632ca16d',1,'Database']]],
  ['isuserindatabase',['isUserInDatabase',['../class_library.html#a578ce2622f10276289f4818f745ab67e',1,'Library']]],
  ['item',['Item',['../class_item.html#a144db4dbae11aa8f2e231cde8646b028',1,'Item::Item(const std::string IDCode=0, std::string name=&quot;&quot;, std::string description=&quot;&quot;)'],['../class_item.html#ade7e491f0f1fb579f98c28f7079ace05',1,'Item::Item(const Item &amp;i)']]]
];
