var searchData=
[
  ['add',['add',['../class_database.html#a904f8a7375009e5266c9ca3087506420',1,'Database']]],
  ['addbook',['addBook',['../class_library.html#ae18b352bb538f8f7bbef5eba963feec6',1,'Library::addBook()'],['../class_admin.html#a4a64c486c9db82942a3cf2b9591d43bc',1,'Admin::addBook()']]],
  ['addbooktowishlist',['addBookToWishList',['../class_user.html#a9647677311d12cea36d96591f31237c6',1,'User']]],
  ['addid',['addId',['../class_o_auth.html#a44a68aa042340863ba1ab54eb02a3aca',1,'OAuth']]],
  ['additem',['addItem',['../class_admin_books_library_manager.html#af7c3f0506e8879bd59cfdcd40b77b201',1,'AdminBooksLibraryManager::addItem()'],['../class_admin_library_manager.html#a963a092b054b3b7415e54d2eca4f8d40',1,'AdminLibraryManager::addItem()'],['../class_admin_users_library_manager.html#a840999765e03c776542d26810fb9dce4',1,'AdminUsersLibraryManager::addItem()']]],
  ['addnewbooktolibrary',['addNewBookToLibrary',['../class_book_adder_dialog.html#a6bf1cd8438202fc202db47356ad97f81',1,'BookAdderDialog']]],
  ['addnewuser',['addNewUser',['../class_user_adder_dialog.html#abed48a584fce170c45d7dead59c76323',1,'UserAdderDialog']]],
  ['addref',['addRef',['../class_reference_counter.html#aeb051a137eaf2caa97d3a22cb37f3ce1',1,'ReferenceCounter']]],
  ['addtoorremovefromwishlistbook',['addToOrRemoveFromWishListBook',['../class_book_manager_dialog.html#af004bfc3364e9178a2a50b1089046dd3',1,'BookManagerDialog']]],
  ['adduser',['addUser',['../class_library.html#ad6479360c5ecb4592ab7df4cedc1d331',1,'Library::addUser()'],['../class_admin.html#abdb263f503e23f5ef2bbf140092b305e',1,'Admin::addUser()']]],
  ['admin',['Admin',['../class_admin.html#add5f3d47d70f73f496ec38e236e18f12',1,'Admin::Admin(std::string name, std::string surname, QDate date, std::string email, Database&lt; SmartPointer&lt; Book &gt;&gt; *books, Database&lt; SmartPointer&lt; User &gt;&gt; *users)'],['../class_admin.html#a7538bc53c81d7a23fcec717fe67d7368',1,'Admin::Admin(const Admin &amp;a)'],['../class_admin.html#a82d05c516ca5777ef81c97bf605d92cc',1,'Admin::Admin(SmartPointer&lt; User &gt; u)'],['../class_admin.html#a815ad9d97b9f25b733b409faa38ea9dd',1,'Admin::Admin(SmartPointer&lt; User &gt; u, Database&lt; SmartPointer&lt; Book &gt;&gt; *books, Database&lt; SmartPointer&lt; User &gt;&gt; *users)']]],
  ['adminbookslibrarymanager',['AdminBooksLibraryManager',['../class_admin_books_library_manager.html#a462bf404e007cdae1fad2fd94f6c90f3',1,'AdminBooksLibraryManager']]],
  ['admindialog',['AdminDialog',['../class_admin_dialog.html#a45c7f0c8881a0ea2a04d6bc3b282ccdc',1,'AdminDialog']]],
  ['adminlibrarymanager',['AdminLibraryManager',['../class_admin_library_manager.html#a51b64c42d4ce0fc1a39134e7dd388ef3',1,'AdminLibraryManager']]],
  ['adminuserslibrarymanager',['AdminUsersLibraryManager',['../class_admin_users_library_manager.html#ae46c10ae7a89c79a4121db0e17573033',1,'AdminUsersLibraryManager']]],
  ['app',['App',['../class_app.html#acb8cbf3e285b91d0170ffe87df5989c5',1,'App']]],
  ['applysettings',['applySettings',['../class_user_editor_dialog.html#a0135cdcf9cd33c3a76cda672f7f6f20d',1,'UserEditorDialog']]],
  ['authenticate',['authenticate',['../class_o_auth.html#a370cfa3a6c5e099ea831aa47cd0bec2e',1,'OAuth']]]
];
