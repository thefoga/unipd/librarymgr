var searchData=
[
  ['book',['Book',['../class_book.html#abef10101f474f12e17fc49a6eeb5b307',1,'Book::Book(const std::string isbnCode, std::string title, std::string description, bool isAvailable=true, bool isBooked=false)'],['../class_book.html#a23ddc31c79e18d2349e4ff93b2ea0b8b',1,'Book::Book(const std::string isbnCode)'],['../class_book.html#a6f0835cfd79b4ed1e17d9de33973406e',1,'Book::Book(const Book &amp;b)']]],
  ['bookadderdialog',['BookAdderDialog',['../class_book_adder_dialog.html#a35bf59a7e3ba3ea906a9630235316a27',1,'BookAdderDialog']]],
  ['bookdetailsdialog',['BookDetailsDialog',['../class_book_details_dialog.html#a97b435e949ba67130e831d5e114feb0f',1,'BookDetailsDialog']]],
  ['bookmanagerdialog',['BookManagerDialog',['../class_book_manager_dialog.html#a1d43f0bc8e592985b76ca3a4705c674e',1,'BookManagerDialog']]]
];
