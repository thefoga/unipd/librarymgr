var searchData=
[
  ['user',['User',['../class_user.html',1,'User'],['../class_user.html#ae78803d1d577c2450b50cff54bf9ec07',1,'User::User(std::string name, std::string surname, QDate date, std::string email)'],['../class_user.html#a99cadf8dd70a9dcc752b7f1df0af809d',1,'User::User(std::string email)'],['../class_user.html#a018a343a483ee5d702fc11c3d6383ac7',1,'User::User(const User &amp;u)']]],
  ['useradderdialog',['UserAdderDialog',['../class_user_adder_dialog.html',1,'UserAdderDialog'],['../class_user_adder_dialog.html#a8230aa5dc07c3e07832f4a3034f789e9',1,'UserAdderDialog::UserAdderDialog()']]],
  ['userbookswidget',['UserBooksWidget',['../class_user_books_widget.html',1,'UserBooksWidget'],['../class_user_books_widget.html#ad560dbd0a8943e6a691c400259e05da1',1,'UserBooksWidget::UserBooksWidget()']]],
  ['userdetailsdialog',['UserDetailsDialog',['../class_user_details_dialog.html',1,'UserDetailsDialog'],['../class_user_details_dialog.html#a75103e708a793f9d061134d0b698ec3b',1,'UserDetailsDialog::UserDetailsDialog()']]],
  ['userdialog',['UserDialog',['../class_user_dialog.html',1,'UserDialog'],['../class_user_dialog.html#ab399986324d115af1ee3aa9de52fcd14',1,'UserDialog::UserDialog()']]],
  ['usereditordialog',['UserEditorDialog',['../class_user_editor_dialog.html',1,'UserEditorDialog'],['../class_user_editor_dialog.html#a145c3b300bda58a6726baee4ae671289',1,'UserEditorDialog::UserEditorDialog()']]],
  ['userpreferenceswidget',['UserPreferencesWidget',['../class_user_preferences_widget.html',1,'UserPreferencesWidget'],['../class_user_preferences_widget.html#a51d34167fca9fd32332160c109179ed9',1,'UserPreferencesWidget::UserPreferencesWidget()']]],
  ['utils',['Utils',['../class_utils.html',1,'']]]
];
