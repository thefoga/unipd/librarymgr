var searchData=
[
  ['person',['Person',['../class_person.html',1,'Person'],['../class_person.html#a27a7b19cdace6de9a04ce0d06fec7957',1,'Person::Person(std::string name, std::string surname, QDate date)'],['../class_person.html#a847845e2e214a89e790c584678f1ea97',1,'Person::Person(const Person &amp;p)']]],
  ['populate',['populate',['../class_book_details_dialog.html#a316d30d906e4b5706309ac7d777e68b1',1,'BookDetailsDialog::populate()'],['../class_user_details_dialog.html#a4f05d113fed6ae53bf530b4b213f56c1',1,'UserDetailsDialog::populate()'],['../class_user_editor_dialog.html#a32489d865980b4eb7fd7b476aace78b2',1,'UserEditorDialog::populate()']]]
];
