var class_sign_in_dialog =
[
    [ "SignInDialog", "class_sign_in_dialog.html#aaf0256ac655e075b5cbe6b206b00500c", null ],
    [ "getSignInRequestResult", "class_sign_in_dialog.html#a63a8d41fe5c19b8c684f14bb5a27d9b7", null ],
    [ "sendSignInProcessIsComplete", "class_sign_in_dialog.html#aa2f71cae10fc7177d709e04be51c8e17", null ],
    [ "sendSignInRequest", "class_sign_in_dialog.html#a58bb0a5765243955fef02d5ae40f0e80", null ],
    [ "show", "class_sign_in_dialog.html#a8bda7f67a50360ffa94b38b0fda0f322", null ],
    [ "signIn", "class_sign_in_dialog.html#ad9c9146cbedecab272b306e4a36c28b4", null ]
];