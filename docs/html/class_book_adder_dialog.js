var class_book_adder_dialog =
[
    [ "BookAdderDialog", "class_book_adder_dialog.html#a35bf59a7e3ba3ea906a9630235316a27", null ],
    [ "addNewBookToLibrary", "class_book_adder_dialog.html#a6bf1cd8438202fc202db47356ad97f81", null ],
    [ "getAddBookToLibraryRequest", "class_book_adder_dialog.html#a4c2c660a8ab2119353fc5ef4eb98ed7f", null ],
    [ "getBook", "class_book_adder_dialog.html#a0bac0a00b992ed55d481a7e5b879df5e", null ],
    [ "sendAddBookToLibraryRequest", "class_book_adder_dialog.html#a81006d1d02155534227b834f6e071985", null ]
];