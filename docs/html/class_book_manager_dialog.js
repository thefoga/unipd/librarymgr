var class_book_manager_dialog =
[
    [ "BookManagerDialog", "class_book_manager_dialog.html#a1d43f0bc8e592985b76ca3a4705c674e", null ],
    [ "addToOrRemoveFromWishListBook", "class_book_manager_dialog.html#af004bfc3364e9178a2a50b1089046dd3", null ],
    [ "getAddBookToWishlistRequestResult", "class_book_manager_dialog.html#a0ffc7b999e1adac330a2e5ed08cc16c7", null ],
    [ "getDetailsAboutBookRequest", "class_book_manager_dialog.html#a5d4421c3218b0a2ee0a63e64fd60ff32", null ],
    [ "getRemoveBookFromWishlistRequestResult", "class_book_manager_dialog.html#ae64cd9a8fc873e0f929ca812d15fa71d", null ],
    [ "getReserveBookRequestResult", "class_book_manager_dialog.html#aa07dfecbda63174fae341d299cceb05e", null ],
    [ "getReturnBookRequestResult", "class_book_manager_dialog.html#ab3e2db384f11595a333f191d7acca2bf", null ],
    [ "reserveOrReturnBook", "class_book_manager_dialog.html#ade82c98835a5881475afc0eb14e5014a", null ],
    [ "sendAddBookToWishlistRequest", "class_book_manager_dialog.html#a6d054a819bea81819023b1e7e27303d0", null ],
    [ "sendRemoveBookFromWishlistRequest", "class_book_manager_dialog.html#ad23290f75f7bf7de95439aad65987921", null ],
    [ "sendReserveBookRequest", "class_book_manager_dialog.html#a02d1645a9f2b130f0dff9983456bda97", null ],
    [ "sendReturnBookRequest", "class_book_manager_dialog.html#a88a2dead446e650424c62caac67a2360", null ]
];