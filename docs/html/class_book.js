var class_book =
[
    [ "Book", "class_book.html#abef10101f474f12e17fc49a6eeb5b307", null ],
    [ "Book", "class_book.html#a23ddc31c79e18d2349e4ff93b2ea0b8b", null ],
    [ "Book", "class_book.html#a6f0835cfd79b4ed1e17d9de33973406e", null ],
    [ "isAvailableToUsers", "class_book.html#a4cdc9d69b29066a22c3dbfdd902b4d81", null ],
    [ "isBookedByUser", "class_book.html#a3ae9e7c3540adf5d0bb3f2b38f3484fb", null ],
    [ "makeAvailableToUsers", "class_book.html#ac15d2bf0a6b21e9d6996540b1a6242e0", null ],
    [ "makeUnAvailableToUsers", "class_book.html#a7fcc6a13174d31ae9d6789beffbdae60", null ],
    [ "operator!=", "class_book.html#a77903c7af788afd234d6cf3f64a02541", null ],
    [ "operator=", "class_book.html#a26a6c2ae89938f76269e2f93e7ee676f", null ],
    [ "operator==", "class_book.html#ab08c02fc82b5c2f9b9a9bef03063598f", null ],
    [ "reserve", "class_book.html#a05a872fe6ae841704f27e370e31d1901", null ],
    [ "returnToLibrary", "class_book.html#a8b19e4877f1e57bc513b5025f983fbe5", null ],
    [ "operator<<", "class_book.html#ae8c42bdbe7ea5ce5e0c4234fc3041f59", null ]
];