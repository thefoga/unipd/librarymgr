var class_login_dialog =
[
    [ "LoginDialog", "class_login_dialog.html#abea7ed017f7a6375da3c796ca4a50923", null ],
    [ "exit", "class_login_dialog.html#ac0271d9dcf79af2fffa48796fdef264d", null ],
    [ "getSignInProcessIsComplete", "class_login_dialog.html#a24d387fe91d0356ec558e8236af490dd", null ],
    [ "sendSignInProcessIsComplete", "class_login_dialog.html#a6b5d490c51015e3a40dc8d8552c2e265", null ],
    [ "sendSignInRequest", "class_login_dialog.html#ab9bda39689200bb4b507ae14b181d7bb", null ],
    [ "sendSignInRequestResult", "class_login_dialog.html#a7618164c0e6f9b0b4d10c9a49e2e16cd", null ],
    [ "sendSignUpRequest", "class_login_dialog.html#a175d86057013c4e04d330dd56b2fc494", null ],
    [ "sendSignUpRequestResult", "class_login_dialog.html#a79cce1a536e74f438753276bee08e891", null ],
    [ "signIn", "class_login_dialog.html#a61f85a712804a0f90556a049d2374956", null ],
    [ "signUp", "class_login_dialog.html#a563979f996717d8c6dcb670ba56185b8", null ]
];