var class_item =
[
    [ "Item", "class_item.html#a144db4dbae11aa8f2e231cde8646b028", null ],
    [ "Item", "class_item.html#ade7e491f0f1fb579f98c28f7079ace05", null ],
    [ "getDescription", "class_item.html#acd1f64b3368b3f7e8cfa808d5d685c08", null ],
    [ "getID", "class_item.html#a823b431f468a18e58ee2c808e5642a53", null ],
    [ "getName", "class_item.html#a116308353701daee0b4fc3e1d0cac104", null ],
    [ "operator==", "class_item.html#a934a60e51bd80b2e1d3e0c3952a2fd2e", null ],
    [ "setDescription", "class_item.html#ab5b987640aafeee80a284861ebedb959", null ],
    [ "setID", "class_item.html#a2bad6384eb6479ea0e1a131054fa1cc7", null ],
    [ "setName", "class_item.html#ad1df8af319de8f613b85d99118a1e203", null ],
    [ "operator<<", "class_item.html#abe20e9773b0b87540b6b96cced38f38c", null ],
    [ "operator<<", "class_item.html#a18d2e5e1928be35229ef03aeec3dae4b", null ]
];