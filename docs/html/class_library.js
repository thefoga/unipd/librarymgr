var class_library =
[
    [ "Library", "class_library.html#a82338219d8bf51962ff5f60a0db21b19", null ],
    [ "Library", "class_library.html#af4712d4a4733c5e2848129756f3396a4", null ],
    [ "~Library", "class_library.html#a62120f28a9b50cc5b151d868e42ab936", null ],
    [ "addBook", "class_library.html#ae18b352bb538f8f7bbef5eba963feec6", null ],
    [ "addUser", "class_library.html#ad6479360c5ecb4592ab7df4cedc1d331", null ],
    [ "editBook", "class_library.html#a983f569333bcca8e312e28faf4c31299", null ],
    [ "editUser", "class_library.html#ad102e94fb61ef4db352769970c470ffe", null ],
    [ "findBook", "class_library.html#a1e4254bc287282e010057c3e5badb43b", null ],
    [ "findUser", "class_library.html#a525d14bedae90669616814e7bc8b64d2", null ],
    [ "getAdmins", "class_library.html#a4111051c1f0968c5663bb7fd0fef6f44", null ],
    [ "getBooks", "class_library.html#a77968b77aab5078fad2e3a8149f29c74", null ],
    [ "getUsers", "class_library.html#a973b2ab462d6c953575f0b2da93e4d96", null ],
    [ "isBookInDatabase", "class_library.html#a9b67293035c9287e553fdf375c2c07d4", null ],
    [ "isUserInDatabase", "class_library.html#a578ce2622f10276289f4818f745ab67e", null ],
    [ "removeBook", "class_library.html#a553976ddb2812ca85f16d98e747e7e69", null ],
    [ "removeUser", "class_library.html#a237a223c72cc9c46efbc70b05bfc4067", null ],
    [ "operator<<", "class_library.html#a1422cce38ba96fdfc0f552bf9f557420", null ],
    [ "operator<<", "class_library.html#aecea69f7dd7e339fe8fee2361611ee68", null ]
];