var class_user_preferences_widget =
[
    [ "UserPreferencesWidget", "class_user_preferences_widget.html#a51d34167fca9fd32332160c109179ed9", null ],
    [ "exportWishlistFromFile", "class_user_preferences_widget.html#ac01711f58e530c153589d9c3c14cc438", null ],
    [ "getExportWishlistFromFileRequestResult", "class_user_preferences_widget.html#a827e00f603a221714cff721cef48e06f", null ],
    [ "getImportWishlistFromFileRequestResult", "class_user_preferences_widget.html#ac96babf908d60b8713b1b48c2a4a8f36", null ],
    [ "getUserRequestResult", "class_user_preferences_widget.html#a6ffbebef5bedf908c95a448d6430d7fb", null ],
    [ "importWishlistFromFile", "class_user_preferences_widget.html#a9803e6f3072828c2a7a153f4cffccaa5", null ],
    [ "openUserEditorDialog", "class_user_preferences_widget.html#a71734ad30671811645c2932717bd476c", null ],
    [ "sendCurrentUserRequest", "class_user_preferences_widget.html#a55fbe8d94f0526208f63ddb6be94025d", null ],
    [ "sendEditUserRequest", "class_user_preferences_widget.html#a123a0724762a12e30c40ed4ec1223ce5", null ],
    [ "sendEditUserRequestResult", "class_user_preferences_widget.html#ad816b9242da802e2c280d8f9f71b09a6", null ],
    [ "sendExportWishlistFromFileRequest", "class_user_preferences_widget.html#ab86930fdcf5c019ae47150af6dc7cef4", null ],
    [ "sendImportWishlistFromFileRequest", "class_user_preferences_widget.html#a7456117d7fef744b57fe213fccd03d9f", null ]
];