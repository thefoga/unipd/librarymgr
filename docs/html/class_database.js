var class_database =
[
    [ "Database", "class_database.html#aaafc4cda318124e4d41c994ef9939fa0", null ],
    [ "Database", "class_database.html#a1f1458358b96c2e661d321f3bba963ff", null ],
    [ "~Database", "class_database.html#a4aa986777fd39f9c2cf20ccd0e981e7b", null ],
    [ "add", "class_database.html#a904f8a7375009e5266c9ca3087506420", null ],
    [ "getItems", "class_database.html#acb198e1ecf6f67b1e1cc3820dc05195f", null ],
    [ "isEmpty", "class_database.html#ab4a4b719e7cb7bdae488f56aab57c2c2", null ],
    [ "isInDatabase", "class_database.html#a9e868f7b561ee7bec6dad42c632ca16d", null ],
    [ "operator[]", "class_database.html#ab71463e7202e9e13e6db713616d536c3", null ],
    [ "remove", "class_database.html#a6d8e1621305132ecca4e1b21e918d021", null ],
    [ "replace", "class_database.html#a6e4e2967614f44e36de1e7fb80bb46de", null ],
    [ "search", "class_database.html#ad2d166028b81d82ac3696b1055759792", null ],
    [ "size", "class_database.html#aeefe0c4942913d067612694ce92b7c90", null ],
    [ "operator<<", "class_database.html#af4ddfaac7a165a20482cd5e78e437eaf", null ]
];