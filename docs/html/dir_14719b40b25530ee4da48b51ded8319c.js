var dir_14719b40b25530ee4da48b51ded8319c =
[
    [ "AdminDialog", "dir_79c8159beee59ad050d8b78344e075d7.html", "dir_79c8159beee59ad050d8b78344e075d7" ],
    [ "BookAdderDialog", "dir_4d36a66b9383b026078384920cebcce7.html", "dir_4d36a66b9383b026078384920cebcce7" ],
    [ "BookDetailsDialog", "dir_63022fb9c89780fda465f1962f3dc8b0.html", "dir_63022fb9c89780fda465f1962f3dc8b0" ],
    [ "BookManagerDialog", "dir_a0cd57829ca1e04be8784d2a9bec0105.html", "dir_a0cd57829ca1e04be8784d2a9bec0105" ],
    [ "LoginDialog", "dir_b29cb6d97a3da5ba604c2ad34cf7802e.html", "dir_b29cb6d97a3da5ba604c2ad34cf7802e" ],
    [ "SignInDialog", "dir_5281c5bdb61acb986f3fd325b63a6709.html", "dir_5281c5bdb61acb986f3fd325b63a6709" ],
    [ "SignUpDialog", "dir_9b7f3bef2dba50d9082512294a29138a.html", "dir_9b7f3bef2dba50d9082512294a29138a" ],
    [ "UserAdderDialog", "dir_7e7290d88763b1825907181a5a62ffcc.html", "dir_7e7290d88763b1825907181a5a62ffcc" ],
    [ "UserDetailsDialog", "dir_1cedd224851249b3f82cd2616ec60f85.html", "dir_1cedd224851249b3f82cd2616ec60f85" ],
    [ "UserDialog", "dir_a983d9c597fd818a818a1f2184da3b70.html", "dir_a983d9c597fd818a818a1f2184da3b70" ],
    [ "UserEditorDialog", "dir_a78899d006fde375f4226ec931e59b31.html", "dir_a78899d006fde375f4226ec931e59b31" ]
];