var class_i_o_stream =
[
    [ "DATA_FILES", "class_i_o_stream.html#af387fc9782b78d559e9e123f8d4b0378", [
      [ "USERS", "class_i_o_stream.html#af387fc9782b78d559e9e123f8d4b0378a1fb4aaeb2fad9b16ae538b509a2757af", null ],
      [ "BOOKS", "class_i_o_stream.html#af387fc9782b78d559e9e123f8d4b0378a43779fc45726d276476d83273d2b3991", null ],
      [ "AUTH", "class_i_o_stream.html#af387fc9782b78d559e9e123f8d4b0378a2158b8fac5863380ab3ee0ad025239d2", null ]
    ] ],
    [ "IOStream", "class_i_o_stream.html#aa59f4d583a53d0084d42665c80373acd", null ],
    [ "getAppDirPath", "class_i_o_stream.html#ac3e47a49464ceb37fcc7d8813883ba27", null ],
    [ "getAuthDataFilePath", "class_i_o_stream.html#a864edcac5f83b8eda0d707354dd72b84", null ],
    [ "getBooksDataFilePath", "class_i_o_stream.html#ad0e283e64eadd042e415ee8a0a8bd793", null ],
    [ "getUsersDataFilePath", "class_i_o_stream.html#acd040bf507604b891aa1cae4d621aad7", null ],
    [ "readBooks", "class_i_o_stream.html#a726b753d942c4053772f1f4753481076", null ],
    [ "readFileToJson", "class_i_o_stream.html#a1298f34f7ce16078d10823e9a6784d53", null ],
    [ "readOAuth", "class_i_o_stream.html#a99506021a117fd34df0f622bd33545f2", null ],
    [ "readUsers", "class_i_o_stream.html#a419b2221abeaa4756de0be3e117b5fd8", null ],
    [ "writeBooks", "class_i_o_stream.html#acce186afe3fd4a4a11a1499d593aa8d1", null ],
    [ "writeFileAsJson", "class_i_o_stream.html#aae24a6ac3c331a95e8438e7616eefa3b", null ],
    [ "writeOAuth", "class_i_o_stream.html#a40e2bd3e025d41de39c18e72d853b013", null ],
    [ "writeUsers", "class_i_o_stream.html#a4ca580f07f8515b4a2e2763dcf7adc01", null ]
];