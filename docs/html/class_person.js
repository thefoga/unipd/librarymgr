var class_person =
[
    [ "Person", "class_person.html#a27a7b19cdace6de9a04ce0d06fec7957", null ],
    [ "Person", "class_person.html#a847845e2e214a89e790c584678f1ea97", null ],
    [ "~Person", "class_person.html#a700ffd693321c5fe6880262acf43d4da", null ],
    [ "getDateOfBirth", "class_person.html#a6958ae5c891dee615a0802bbefd7bc24", null ],
    [ "getName", "class_person.html#ac0c7e40e02bfa616e17a0c332dd248eb", null ],
    [ "getSurname", "class_person.html#a7f24e2654e52db9c1f1c368a13135ae1", null ],
    [ "operator!=", "class_person.html#ae4f47029402c26c3643d23ef04695bb1", null ],
    [ "operator=", "class_person.html#ae046bc90711195d83799f81e096ad503", null ],
    [ "operator==", "class_person.html#a7c6a2bfb43c790bc8260933bd847a582", null ],
    [ "setDateOfBirth", "class_person.html#a172e5a16b36e2aa9dfec68b289e1747b", null ],
    [ "setName", "class_person.html#ad6e438f456d3ae6f5b477931c0a6aeba", null ],
    [ "setSurname", "class_person.html#af6a9ce78af39382fbab39f64319c7cde", null ],
    [ "operator<<", "class_person.html#ac1e7cfa19a5c01f3f4dcc41c5e7ddfc7", null ],
    [ "operator<<", "class_person.html#af39990b4e5fa4f6b4294b345a4846fa1", null ]
];