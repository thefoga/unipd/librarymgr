var class_admin_books_library_manager =
[
    [ "AdminBooksLibraryManager", "class_admin_books_library_manager.html#a462bf404e007cdae1fad2fd94f6c90f3", null ],
    [ "addItem", "class_admin_books_library_manager.html#af7c3f0506e8879bd59cfdcd40b77b201", null ],
    [ "editItem", "class_admin_books_library_manager.html#a340ffaaee775b3ed0d354a0237d0cbee", null ],
    [ "getRemoveBookFromLibraryRequestResult", "class_admin_books_library_manager.html#aea9487f053934ffb0b02d0255d99e2c4", null ],
    [ "getSearchBookRequestResult", "class_admin_books_library_manager.html#ae137407472af6e304d231a3fc8f14f0a", null ],
    [ "removeItem", "class_admin_books_library_manager.html#af435465c273ee9774d7251b9381e94e0", null ],
    [ "search", "class_admin_books_library_manager.html#a9d26ced5d3dbffc9f63518fb2015be41", null ],
    [ "sendAddBookToLibraryRequest", "class_admin_books_library_manager.html#a254e4333038d138ca840679c5d4a4976", null ],
    [ "sendAddBookToLibraryRequestResult", "class_admin_books_library_manager.html#a7e46d56857efbfe5666c142cb9062c41", null ],
    [ "sendEditBookInLibraryRequest", "class_admin_books_library_manager.html#a12e7f5bc0346305a011758d29e7ff2eb", null ],
    [ "sendEditBookInLibraryRequestResult", "class_admin_books_library_manager.html#afb107ef90d236a9a8a4183542bed4a71", null ],
    [ "sendRemoveBookFromLibraryRequest", "class_admin_books_library_manager.html#a7ebb75f64c0bd2447843a7fa47c0c1d2", null ],
    [ "sendSearchBookRequest", "class_admin_books_library_manager.html#a4d1d8c4d3925fd271016cf7546db9802", null ]
];