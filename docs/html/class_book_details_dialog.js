var class_book_details_dialog =
[
    [ "BookDetailsDialog", "class_book_details_dialog.html#a97b435e949ba67130e831d5e114feb0f", null ],
    [ "getBook", "class_book_details_dialog.html#a1709550d11aeb5194c5448221a9acc72", null ],
    [ "getDetailsAboutBookRequest", "class_book_details_dialog.html#aca2c15a4195ba11ef8933843f4da4597", null ],
    [ "populate", "class_book_details_dialog.html#a316d30d906e4b5706309ac7d777e68b1", null ],
    [ "sendDetailsAboutBookRequest", "class_book_details_dialog.html#ad349bcf5e6d958d07e29b355546b00db", null ],
    [ "setBookId", "class_book_details_dialog.html#a583234f5606f7be26ec2dbde8767ba75", null ],
    [ "setup", "class_book_details_dialog.html#a55803efb73392edd2f5eb0928bc47848", null ],
    [ "bookIsbn", "class_book_details_dialog.html#a0b4cfc2def903fd2dd0a7af1d9b3cfca", null ],
    [ "descriptionLabel", "class_book_details_dialog.html#a92087f561a05f21cc70781a428369d19", null ],
    [ "isbnLabel", "class_book_details_dialog.html#ac8ffc8da92e7cdd940d42281711837bc", null ],
    [ "titleLabel", "class_book_details_dialog.html#af8ef4c4fa853270384476024d6555b76", null ]
];