var class_admin_users_library_manager =
[
    [ "AdminUsersLibraryManager", "class_admin_users_library_manager.html#ae46c10ae7a89c79a4121db0e17573033", null ],
    [ "addItem", "class_admin_users_library_manager.html#a840999765e03c776542d26810fb9dce4", null ],
    [ "editItem", "class_admin_users_library_manager.html#ac241d538666eb9518efdc574ceb98d82", null ],
    [ "getRemoveUserFromLibraryRequestResult", "class_admin_users_library_manager.html#a43347bf727ba1230ed52a96e3f0bddad", null ],
    [ "getSearchUserRequestResult", "class_admin_users_library_manager.html#a3dd572fcdf6f3fc779b408b536f4c94c", null ],
    [ "getUserRequestResult", "class_admin_users_library_manager.html#af35dcd502d2f2bc79201c8441a601c15", null ],
    [ "removeItem", "class_admin_users_library_manager.html#aecdc75b2c88c518d322859875159f7ff", null ],
    [ "search", "class_admin_users_library_manager.html#a6e84e1cce8f7e66bd610adfefba9737e", null ],
    [ "sendAddUserToLibraryRequest", "class_admin_users_library_manager.html#a5cf7536297c50dc0462aad73b2959527", null ],
    [ "sendAddUserToLibraryRequestResult", "class_admin_users_library_manager.html#aa37e06de4238dea20dfd2632726b8fdd", null ],
    [ "sendEditUserInLibraryRequest", "class_admin_users_library_manager.html#a86e86ab1471f38a1907dd69149c8706a", null ],
    [ "sendEditUserInLibraryRequestResult", "class_admin_users_library_manager.html#aa84128ee88d2362df4df89d02a4b1311", null ],
    [ "sendEditUserRequest", "class_admin_users_library_manager.html#a2a7c5516b21ce114aab80e368fc9846d", null ],
    [ "sendRemoveUserFromLibraryRequest", "class_admin_users_library_manager.html#a7cfdef06fd1f3f129645c2df0f4aedec", null ],
    [ "sendSearchUserRequest", "class_admin_users_library_manager.html#a95bf77f1f271264f4d9889200d91cd96", null ],
    [ "sendUserRequest", "class_admin_users_library_manager.html#acdda0722f5574876dd1b95594589ca29", null ]
];