var hierarchy =
[
    [ "Database< T >", "class_database.html", null ],
    [ "Database< Book >", "class_database.html", null ],
    [ "Database< OAuthId >", "class_database.html", null ],
    [ "Database< SmartPointer< Book > >", "class_database.html", null ],
    [ "Database< SmartPointer< User > >", "class_database.html", null ],
    [ "IOStream", "class_i_o_stream.html", null ],
    [ "Item", "class_item.html", [
      [ "Book", "class_book.html", null ]
    ] ],
    [ "Library", "class_library.html", null ],
    [ "OAuth", "class_o_auth.html", null ],
    [ "OAuthId", "class_o_auth_id.html", null ],
    [ "Person", "class_person.html", [
      [ "User", "class_user.html", [
        [ "Admin", "class_admin.html", null ]
      ] ]
    ] ],
    [ "QDialog", null, [
      [ "BookDetailsDialog", "class_book_details_dialog.html", [
        [ "BookAdderDialog", "class_book_adder_dialog.html", null ],
        [ "BookManagerDialog", "class_book_manager_dialog.html", null ]
      ] ],
      [ "LoginDialog", "class_login_dialog.html", null ],
      [ "SignInDialog", "class_sign_in_dialog.html", null ],
      [ "SignUpDialog", "class_sign_up_dialog.html", null ],
      [ "UserDetailsDialog", "class_user_details_dialog.html", [
        [ "UserAdderDialog", "class_user_adder_dialog.html", null ],
        [ "UserEditorDialog", "class_user_editor_dialog.html", null ]
      ] ],
      [ "UserDialog", "class_user_dialog.html", [
        [ "AdminDialog", "class_admin_dialog.html", null ]
      ] ]
    ] ],
    [ "QObject", null, [
      [ "App", "class_app.html", null ],
      [ "Gui", "class_gui.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "AdminLibraryManager", "class_admin_library_manager.html", [
        [ "AdminBooksLibraryManager", "class_admin_books_library_manager.html", null ],
        [ "AdminUsersLibraryManager", "class_admin_users_library_manager.html", null ]
      ] ],
      [ "InlineLabel", "class_inline_label.html", null ],
      [ "UserBooksWidget", "class_user_books_widget.html", null ],
      [ "UserPreferencesWidget", "class_user_preferences_widget.html", null ]
    ] ],
    [ "ReferenceCounter", "class_reference_counter.html", null ],
    [ "Settings", "class_settings.html", null ],
    [ "SmartPointer< T >", "class_smart_pointer.html", null ],
    [ "Utils", "class_utils.html", null ]
];