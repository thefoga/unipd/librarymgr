var class_admin =
[
    [ "Admin", "class_admin.html#add5f3d47d70f73f496ec38e236e18f12", null ],
    [ "Admin", "class_admin.html#a7538bc53c81d7a23fcec717fe67d7368", null ],
    [ "Admin", "class_admin.html#a82d05c516ca5777ef81c97bf605d92cc", null ],
    [ "Admin", "class_admin.html#a815ad9d97b9f25b733b409faa38ea9dd", null ],
    [ "~Admin", "class_admin.html#a8fb5fe72fb4e05d50980c13cdf3b779c", null ],
    [ "addBook", "class_admin.html#a4a64c486c9db82942a3cf2b9591d43bc", null ],
    [ "addUser", "class_admin.html#abdb263f503e23f5ef2bbf140092b305e", null ],
    [ "makeAdmin", "class_admin.html#a800d579a5b22fdfc4e05ba595f46667f", null ],
    [ "operator=", "class_admin.html#a4e451f158e0c497042b9d8391e23c829", null ],
    [ "removeBook", "class_admin.html#ab5abc30d4feddbe7c6ae34743378c995", null ],
    [ "removeUser", "class_admin.html#a2cd28597301d7c6e3acc5923055b0956", null ],
    [ "replaceBook", "class_admin.html#a19ccdf33fc763617f77c6ec7ccea46c3", null ],
    [ "operator<<", "class_admin.html#ab50d4206f7748ecb4dad7c3734322c97", null ],
    [ "operator<<", "class_admin.html#aec88e857e4a3328db47ab3f48998bb34", null ]
];