# Change Log
All notable changes to this project will be documented in this file.

## 0.6.2 - 2017-01-04
### Changed
- report structure: now using article class with sections
### Added
- report: added details

## 0.6.1 - 2017-01-04
### Added
- partial report

## 0.6 - 2017-01-04
### Added
- report structure

## 0.5.9.9 - 2017-01-04
### Changed
- Qt project file so that compiler uses c++11
- import/export wishlist: now accept only file path with length > 1

## 0.5.9.8 - 2017-01-04
### Added
- help/about dialog
### Tested
- gui and backend (now before starting app we run tests)

## 0.5.9.7 - 2017-01-04
### Added
- import/export wishlist
### Removed
- io streams useless enum

## 0.5.9.6 - 2017-01-04
### Fixed
- SEG FAULT when deleting user

## 0.5.9.5 - 2017-01-04
### Added
- (partly) help/about dialog
- export wishlist to .json
- import wishlist from .json
### Changed
- now window has fixed size

## 0.5.9.4 - 2017-01-03
### Fixed
- memory leak in smart pointer when deleting book

## 0.5.9.3 - 2017-01-03
### Added
- edit user dialog (with "make admin" option)
- now admins can signup other admins
### Fixed
- bug when converting admin to user through GUI

## 0.5.9.3 - 2017-01-03
### Added
- admin user manager: edit user dialog

## 0.5.9.2 - 2017-01-03
### Fixed
- bug when adding new user there was no oauth predefined
- duplicate signal when adding new user
- add new book correctly (missing title and description)
- SEG FAULT in removing user
- duplicate dialog when adding/finding books
- repositioning user adder dialog in center screen
### Added
- edit dialog for users
- add user details edit dialog subclass

## 0.5.9.1 - 2017-01-02
### Added
- user details subclass to add new user

## 0.5.9 - 2017-01-02
### Removed
- edit dialogs to edit books and users
### Changed
- BookDetailsDialog now is a QDialog, not a QWidget
### Refactored
- BookManagerDialog now is a subclass of BookDetailsDialog
### Fixed
- bug in BooksDetailsDialog that would make 2 dialogs appear when reserving book
### Added
- book details subclass to add new book

## 0.5.8.9 - 2017-01-02
### Added
- now admins can safely remove users (except themselves)

## 0.5.8.8 - 2017-01-02
### Happy new year!
### Added
- admin users manager
- backend: user management

## 0.5.8.7 - 2016-12-30
### Added
- admin dialog books manager: setup and signal/slots
- now admin can remove books from library

## 0.5.8.6 - 2016-12-30
### Added
- users can change passwords through GUI now

## 0.5.8.5 - 2016-12-30
### Added
- edit user fields and credentials
- edit user fields (signal/slots)
### Fixed
- bug in assigning user

## 0.5.8.4 - 2016-12-30
### Added
- loans table: signals/slots

## 0.5.8.3 - 2016-12-30
### Added
- read/write user loans

## 0.5.8.2 - 2016-12-29
### Added
- user preferences dialog

## 0.5.8.1 - 2016-12-29
### Added
- parsers/converters/generic writers in IoStream
### Refactored
- removed compiler warnings

## 0.5.8 - 2016-12-29
### Fixed
- bug in reading user wishlist books
- bug in GUI-returning-reserving books
### Added
- full book details dialog with options to manage wishlist and return/reserve book

## 0.5.7.9 - 2016-12-29
### Tested
- reserve/return book management

## 0.5.7.8 - 2016-12-29
### Added
- signal/slots to reserve/return book
### Changed
- changelog with actual dates and versions

## 0.5.7.7 - 2016-12-29
### Added
- signal/slots to manage book details dialog

## 0.5.7.6 - 2016-12-28
### Tested
- search results in user/admin dialog

## 0.5.7.5 - 2016-12-28
### Removed
- double login panel user/admin: now only login panel, one for all
### Added
- signal/slots to populate user dialog

## 0.5.7.4.1 - 2016-12-28
### Added
- save/load database of user books wishlist

## 0.5.7.4 - 2016-12-27
### Added
- open user dialog when sign-in successful

## 0.5.7.3 - 2016-12-27
### Fixed
- bug in loading/saving app data

## 0.5.7.2 - 2016-12-26
### Tested
- signUp/signIn process

## 0.5.7.1.1 - 2016-12-26
### Changed
- signUp slots now come with bool result

## 0.5.7.1 - 2016-12-26
### Added
- (raw) signUp signals/slots

## 0.5.7 - 2016-12-26
### Added (and tested)
- IOStream read&write

## 0.5.6 - 2016-12-26
### Added
- IOStream basics

## 0.5.5 - 2016-12-25
### Added
- test unit

## 0.5.4 - 2016-12-25
### Added
- OAuth services

## 0.5.3 - 2016-12-25
### Tested
- library, authentication and partly GUI

## 0.5.2.2 - 2016-12-22
### Added
- User details dialog

## 0.5.2.1 - 2016-12-22
### Added
- Inline labels widget
- Book details dialog

## 0.5.2 - 2016-12-22
### Tested
- Library backend

## 0.5.1 - 2016-12-22
### Tested
- SmartPointer (+ReferenceCounting)
### Added
- user wish list getter

## 0.5 - 2016-12-22
### Changed
- refactored classes
### Added
- SmartPointer (+ReferenceCounting)

## 0.4.2 - 2016-12-21
### Refactored
- created specific packages for widgets and dialogs

## 0.4.1 - 2016-12-20
### Added
- Sign-in dialog

## 0.4 - 2016-12-20
### Refactored
- classes structure
### Fixed
- tested and properly de-bugged backend structure

## 0.3.1 - 2016-12-20
### Added
- Library class

## 0.3 - 2016-12-19
### Fixed
- Person and User "operator!="
### Refactored
- now using a real app structure

## 0.2 - 2016-12-16
### Added
- admin and database

## 0.1 - 2016-12-16
### Added
- models of items and users
