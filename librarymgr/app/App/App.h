/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_APP_H
#define LIBRARYMGR_APP_H


#include <QObject>

#include "../../streams/IOStream/IOStream.h"
#include "../../auth/OAuth/OAuth.h"
#include "../Settings/Settings.h"
#include "../../gui/Gui/Gui.h"
#include "../Library/Library.h"

/**
 * @brief MVC controller class
 * Has backend and frontend parts: controls and make them interact
 */
class App : public QObject {
Q_OBJECT

public:
    /**
     * New app loading default database of users and books
     * @return new app
     */
    App();

    /**
     * Destroy app
     */
    ~App();

    /**
     * @brief start open app
     */
    void start();

    /**
     * Sign-in user
     * @param username name or user to login
     * @param password password or user to login
     * @return true if everything went well
     */
    bool signIn(QString username, QString password);

    /**
     * Sign-up new user
     * @param user new user to sign up
     * @param password password of user to authenticate
     * @return true if everything went well
     */
    bool signUp(User user, QString password);

    /**
     * Exits app properly (saving changes)
     */
    void exit();

public slots:
    // login

    /**
     * Mark when a sign in request is made by the dialog
     * @param username username to sign in with
     * @param password password of user to authenticate
     */
    void getSignInRequest(QString username, QString password);

    /**
     * Mark when a sign up request is made by the dialog
     * @param user new user to sign up
     */
    void getSignUpRequest(User user, QString password);

    // user dialog

    /**
     * Asks backend for details about book with isbn
     * @param isbn isbn of book to get details about
     */
    void getDetailsAboutBookRequest(QString isbn);

    /**
     * Gets search results
     * @param id isbn code to search book
     * @param title title of book to search
     */
    void getSearchBookRequest(QString id, QString title);

    /**
     * Gets user wishlist
     */
    void getWishlistRequest();

    /**
     * Sends request to backend to get books loans of current user
     */
    void getLoansRequest();

    /**
     * Checks if adding requested book to user wishlist operation went well
     * @param book book to add to wishlist
     */
    void getAddBookToWishlistRequest(Book book);

    /**
     * Checks if removing requested book from user wishlist operation went well
     * @param book book to remove from wishlist
     */
    void getRemoveBookFromWishlistRequest(Book book);

    /**
     * Sends request to backend to reserve book
     * @param book book to reserve
     */
    void getReserveBookRequest(Book book);

    /**
     * Sends request to backend to return book
     * @param book book to return
     */
    void getReturnBookRequest(Book book);

    /**
     * Asks backend to send info about current user
     */
    void getCurrentUserRequest();

    /**
     * Asks backend to send info about current user
     * @param username username of user to get info about
     */
    void getUserRequest(QString username);

    /**
     * Asks backend to change current user to this new one
     * @param user new user
     * @param isAdmin true iff user to display is an admin
     * @param password new password
     */
    void getEditUserRequest(User user, bool isAdmin, QString password);

    /**
     * Sends backend request to import books to user wishlist from given file
     * @param pathToFileToImport file to import books to user wishlist
     */
    void getImportWishlistFromFileRequest(QString pathToFileToImport);

    /**
     * Sends backend request to export books to user wishlist from given file
     * @param pathToFileToExport file to export books to user wishlist
     */
    void getExportWishlistFromFileRequest(QString pathToFileToExport);

    // admin dialog
    // books

    /**
     * Sends request to backend to add new book to library
     * @param book book to add
     */
    void getAddBookToLibraryRequest(Book book);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param oldBookIsbn isbn of book to edit
     * @param newBook new book
     */
    void getEditBookInLibraryRequest(QString oldBookIsbn, Book newBook);

    /**
     * Sends request to backend to remove book from library
     * @param book book to remove
     */
    void getRemoveBookFromLibraryRequest(Book book);

    // users

    /**
     * Sends request to backend to search database for given user
     * @param id email of user to search
     * @param name name of user to search
     */
    void getSearchUserRequest(QString id, QString name);

    /**
     * Sends request to backend to add new user to library
     * @param user new user to add
     * @param makeAdmin true iff new user should be an admin
     */
    void getAddUserToLibraryRequest(User user, bool makeAdmin);

    /**
     * Sends request to backend to edit user
     * @param oldId email of user to edit
     * @param newUser new user
     * @param newPassword password of new user
     */
    void getEditUserInLibraryRequest(QString oldId, User newUser, bool isAdmin, QString newPassword);

    /**
     * Sends request to backend to remove user from library
     * @param user user to remove
     */
    void getRemoveUserFromLibraryRequest(User user);

signals:

    /**
     * Sends information regardin admin status of the current user of the app
     * @param isCurrentUserAnAdmin true if current user of app is an admin
     */
    void sendCurrentUserAdminStatus(bool isCurrentUserAnAdmin);

    // login

    /**
     * Sends sign in result
     * @param signInOk whether sign authentication went well
     */
    void sendSignInRequestResult(bool signInOk);

    /**
     * Sends sign up result
     * @param signUpOk whether sign up process went well
     */
    void sendSignUpRequestResult(bool signUpOk);

    // user dialog

    /**
 * Gets backend response on book details
 * @param book full book details
 * @param isReservedByUser true iff current user has reserved this book
 * @param isInUserWishlist true iff book is in current user wishlist
 */
    void sendDetailsAboutBookRequestResult(Book book, bool isReservedByUser, bool isInUserWishlist);

    /**
     * Gets user wishlist
     * @param book user books wishlist
     */
    void sendSearchBookRequestResult(std::vector<Book> books);

    /**
     * Sends request to backend to get wishlist of current user
     */
    void sendWishlistRequestResult(std::vector<Book> books);

    /**
     * Sends request to backend to get books loans of current user
     * @param books user books loans
     */
    void sendLoansRequestResult(std::vector<Book> books);

    /**
     * Checks if adding requested book to user wishlist operation went well
     * @param ok true iff operation went well
     */
    void sendAddBookToWishlistRequestResult(bool ok);

    /**
     * Checks if removing requested book from user wishlist operation went well
     * @param ok true iff operation went well
     */
    void sendRemoveBookFromWishlistRequestResult(bool ok);

    /**
     * Sends request to backend to reserve book
     * @param ok true iff operation went well
     */
    void sendReserveBookRequestResult(bool ok);

    /**
     * Sends request to backend to return book
     * @param ok true iff operation went well
     */
    void sendReturnBookRequestResult(bool ok);

    /**
     * Gets backend response to send info about current user
     * @param user current user
     * @param isAdmin true iff user to display is an admin
     */
    void sendUserRequestResult(User user, bool isAdmin);

    /**
     * Gets backend response on change current user settings
     * @param ok true iff operation went well
     */
    void sendEditUserRequestResult(bool ok);

    /**
     * Sends backend request to import books to user wishlist from given file
     * @param ok true iff operation went well
     */
    void sendImportWishlistFromFileRequestResult(bool ok);

    /**
     * Sends backend request to export books to user wishlist from given file
     * @param ok true iff operation went well
     */
    void sendExportWishlistFromFileRequestResult(bool ok);

    // admin dialog
    // books

    /**
     * Sends request to backend to add new book to library
     * @param ok true iff operation went well
     */
    void sendAddBookToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param ok true iff operation went well
     */
    void sendEditBookInLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to remove book from library
     * @param ok true iff operation went well
     */
    void sendRemoveBookFromLibraryRequestResult(bool ok);

    // users
    // results

    /**
     * Gets search results
     * @param users list of users matching query
     */
    void sendSearchUserRequestResult(std::vector<User> users);

    /**
     * Sends request to backend to add new user to library
     * @param ok true iff operation went well
     */
    void sendAddUserToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to edit user
     * @param ok true iff operation went well
     */
    void sendEditUserInLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to remove user from library
     * @param ok true iff operation went well
     */
    void sendRemoveUserFromLibraryRequestResult(bool ok);

private:
    User *user;  // current user logged in
    OAuth oAuth;  // authentication manager
    Gui *gui;  // user interface
    Library library;  // "backend": model of library with users and books
    IOStream io;  // file I/O stream manager

    // app

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();

    /**
     * Opens database files and loads in memory app data
     */
    void loadAppData();

    /**
     * Saves app data to database
     */
    void saveAppData();

    // login

    /**
     * Adds a default admin login to list of users and credentials; username: admin, password: admin.
     */
    void addDefaultAdminLogin();

    /**
     * Adds sample books
     */
    void addSampleBooks();

    /**
     * Adds some users for demo purposes
     */
    void addSampleUsers();

    // user

    /**
     * Checks if current user of app is an admin
     * @return true if current user of app is an admin
     */
    bool isCurrentUserAnAdmin();

    /**
     * Edits current user fields
     * @param user user to edit
     * @param newUser new user fields
     * @return true iff everything went well
     */
    bool editUser(User *user, User newUser);

    /**
     * Edits current user fields
     * @param oldId id of old user to edit
     * @param newUser new user fields
     * @param makeAdmin true iff new user should be and admin or not
     * @param newPassword new password of new user
     * @return true iff everything went well
     */
    bool editUserAndChangeAdminStatus(QString oldId, User newUser, bool makeAdmin, QString newPassword);

    /**
     * Edits current username
     * @param user user to edit
     * @param newUserName new username of user
     * @return true iff everything went well
     */
    bool editUserUsername(User *user, std::string newUserName);

    /**
     * Edits current user password
     * @param user user to edit
     * @param newPassword new password of user
     * @return true iff everything went well
     */
    bool editUserPassword(User *user, std::string newPassword);

    /**
     * Removes user from library
     * @param user user to remove
     * @return true iff operation went well
     */
    bool removeUser(User user);

    /**
     * Fetches user wishlist
     * @return user wishlist books
     */
    std::vector<Book> getCurrentUserWishlist();
};


#endif //LIBRARYMGR_APP_H
