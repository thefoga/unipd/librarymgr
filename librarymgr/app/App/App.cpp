/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QSplashScreen>

#include "App.h"

App::App() {
    loadAppData();
    gui = new Gui();
    connectSignalsSlots();
}

App::~App() {
    exit();
}

void App::start() {
    gui->open();  // show GUI
}

bool App::signIn(QString username, QString password) {
    bool isAuthenticated = oAuth.authenticate(
            OAuthId(
                    username,
                    password
            )
    );

    if (isAuthenticated) {
        user = library.findUser(
                new User(
                        "name",
                        "surname",
                        QDateTime::currentDateTime().date(),
                        username.toStdString().c_str()
                )
        ).getData();  // set current user

        emit sendCurrentUserAdminStatus(isCurrentUserAnAdmin());
    }

    return isAuthenticated;
}

bool App::signUp(User user, QString password) {
    if (library.isUserInDatabase(user)) {  // user is already in database
        return false;
    }

    if (oAuth.addId(OAuthId(QString::fromUtf8(user.getEmail().c_str()), password))) {  // register new user credentials
        library.addUser(new User(user));  // add new user
        return true;
    } else {
        return false;
    }
}

void App::exit() {
    delete gui;  // delete gui objects

    saveAppData();
}

void App::loadAppData() {
    oAuth = io.readOAuth(io.getAuthDataFilePath());  // load credentials

    std::vector<Book> books = io.readBooks(io.getBooksDataFilePath());  // load books database
    for (unsigned int i = 0; i < books.size(); i++) {
        library.addBook(SmartPointer<Book>(new Book(books[i])));  // add to library database books
    }

    std::vector<User *> users = io.readUsers(io.getUsersDataFilePath());  // load users database
    for (unsigned int i = 0; i < users.size(); i++) {
        library.addUser(SmartPointer<User>(users[i]));  // add to library database books
    }

    // TODO: demo purposes only
    if (library.getAdmins()->isEmpty()) {
        addDefaultAdminLogin();
    }

    if (library.getUsers()->isEmpty()) {
        addSampleUsers();
    }

    if (library.getBooks()->isEmpty()) {
        addSampleBooks();
    }
}

void App::saveAppData() {
    io.writeOAuth(oAuth, io.getAuthDataFilePath());  // save users credentials

    std::vector<SmartPointer<User>> usersPointers = library.getUsers()->getItems();  // get users in library
    std::vector<User *> users;
    for (unsigned int i = 0; i < usersPointers.size(); i++) {
        users.push_back(usersPointers[i].getData());
    }
    io.writeUsers(users, io.getUsersDataFilePath());  // save users databases

    std::vector<SmartPointer<Book>> booksPointers = library.getBooks()->getItems();  // get users in library
    std::vector<Book> books;
    for (unsigned int i = 0; i < booksPointers.size(); i++) {
        books.push_back(*booksPointers[i].getData());
    }
    io.writeBooks(books, io.getBooksDataFilePath());  // save books databases
}

void App::getSignInRequest(QString username, QString password) {
    emit sendSignInRequestResult(signIn(username, password));
}

void App::getSignUpRequest(User user, QString password) {
    emit sendSignUpRequestResult(signUp(user, password));
}

bool App::isCurrentUserAnAdmin() {
    Admin *adminCandidate = dynamic_cast<Admin *> (user);
    return adminCandidate && adminCandidate->isAdmin();
}

void App::addDefaultAdminLogin() {
    Admin *defaultAdmin = new Admin(
            new User(
                    "Name",
                    "Surname",
                    QDate(0, 0, 0),
                    "admin"
            )
    );

    if (!library.isUserInDatabase(*defaultAdmin)) {  // if not already in user list
        library.addUser(defaultAdmin);  // add to users list
        oAuth.addId(OAuthId(QString::fromUtf8(defaultAdmin->getEmail().c_str()), "admin"));  // add credentials
    }
}

void App::connectSignalsSlots() {
    // admin user
    connect(this, SIGNAL(sendCurrentUserAdminStatus(bool)), gui, SLOT(getCurrentUserAdminStatus(bool)));

    // sign in
    connect(gui, SIGNAL(sendSignInRequest(QString, QString)), this, SLOT(getSignInRequest(QString, QString)));
    connect(this, SIGNAL(sendSignInRequestResult(bool)), gui, SIGNAL(sendSignInRequestResult(bool)));

    // sign up
    connect(gui, SIGNAL(sendSignUpRequest(User, QString)), this, SLOT(getSignUpRequest(User, QString)));
    connect(this, SIGNAL(sendSignUpRequestResult(bool)), gui, SIGNAL(sendSignUpRequestResult(bool)));

    // user dialog
    // send requests
    connect(gui, SIGNAL(sendSearchBookRequest(QString, QString)), this, SLOT(getSearchBookRequest(QString, QString)));
    connect(gui, SIGNAL(sendWishlistRequest()), this, SLOT(getWishlistRequest()));
    connect(gui, SIGNAL(sendLoansRequest()), this, SLOT(getLoansRequest()));
    connect(gui, SIGNAL(sendAddBookToWishlistRequest(Book)), this, SLOT(getAddBookToWishlistRequest(Book)));
    connect(gui, SIGNAL(sendRemoveBookFromWishlistRequest(Book)), this, SLOT(getRemoveBookFromWishlistRequest(Book)));
    connect(gui, SIGNAL(sendReserveBookRequest(Book)), this, SLOT(getReserveBookRequest(Book)));
    connect(gui, SIGNAL(sendReturnBookRequest(Book)), this, SLOT(getReturnBookRequest(Book)));
    connect(gui, SIGNAL(sendDetailsAboutBookRequest(QString)), this, SLOT(getDetailsAboutBookRequest(QString)));
    connect(gui, SIGNAL(sendEditUserRequest(User, bool, QString)), this, SLOT(getEditUserRequest(User, bool, QString)));
    connect(gui, SIGNAL(sendCurrentUserRequest()), this, SLOT(getCurrentUserRequest()));
    connect(gui, SIGNAL(sendUserRequest(QString)), this, SLOT(getUserRequest(QString)));
    connect(gui, SIGNAL(sendImportWishlistFromFileRequest(QString)), this,
            SLOT(getImportWishlistFromFileRequest(QString)));
    connect(gui, SIGNAL(sendExportWishlistFromFileRequest(QString)), this,
            SLOT(getExportWishlistFromFileRequest(QString)));

    // forward results
    connect(this, SIGNAL(sendSearchBookRequestResult(std::vector<Book>)), gui,
            SIGNAL(sendSearchBookRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendWishlistRequestResult(std::vector<Book>)), gui,
            SIGNAL(sendWishlistRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendLoansRequestResult(std::vector<Book>)), gui,
            SIGNAL(sendLoansRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendAddBookToWishlistRequestResult(bool)), gui,
            SIGNAL(sendAddBookToWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveBookFromWishlistRequestResult(bool)), gui,
            SIGNAL(sendRemoveBookFromWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendReserveBookRequestResult(bool)), gui,
            SIGNAL(sendReserveBookRequestResult(bool)));
    connect(this, SIGNAL(sendReturnBookRequestResult(bool)), gui,
            SIGNAL(sendReturnBookRequestResult(bool)));
    connect(this, SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)), gui,
            SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)));
    connect(this, SIGNAL(sendEditUserRequestResult(bool)), gui,
            SIGNAL(sendEditUserRequestResult(bool)));
    connect(this, SIGNAL(sendUserRequestResult(User, bool)), gui,
            SIGNAL(sendUserRequestResult(User, bool)));
    connect(this, SIGNAL(sendImportWishlistFromFileRequestResult(bool)), gui,
            SIGNAL(sendImportWishlistFromFileRequestResult(bool)));
    connect(this, SIGNAL(sendExportWishlistFromFileRequestResult(bool)), gui,
            SIGNAL(sendExportWishlistFromFileRequestResult(bool)));

    // admin dialog

    // books
    // send requests
    connect(gui, SIGNAL(sendAddBookToLibraryRequest(Book)), this, SLOT(getAddBookToLibraryRequest(Book)));
    connect(gui, SIGNAL(sendEditBookInLibraryRequest(QString, Book)), this,
            SLOT(getEditBookInLibraryRequest(QString, Book)));
    connect(gui, SIGNAL(sendRemoveBookFromLibraryRequest(Book)), this, SLOT(getRemoveBookFromLibraryRequest(Book)));

    // forward results
    connect(this, SIGNAL(sendAddBookToLibraryRequestResult(bool)), gui,
            SIGNAL(sendAddBookToLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendEditBookInLibraryRequestResult(bool)), gui,
            SIGNAL(sendEditBookInLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveBookFromLibraryRequestResult(bool)), gui,
            SIGNAL(sendRemoveBookFromLibraryRequestResult(bool)));

    // users
    // send requests
    connect(gui, SIGNAL(sendSearchUserRequest(QString, QString)), this, SLOT(getSearchUserRequest(QString, QString)));
    connect(gui, SIGNAL(sendAddUserToLibraryRequest(User, bool)), this, SLOT(getAddUserToLibraryRequest(User, bool)));
    connect(gui, SIGNAL(sendEditUserInLibraryRequest(QString, User, bool, QString)), this,
            SLOT(getEditUserInLibraryRequest(QString, User, bool, QString)));
    connect(gui, SIGNAL(sendRemoveUserFromLibraryRequest(User)), this, SLOT(getRemoveUserFromLibraryRequest(User)));

    // forward results
    connect(this, SIGNAL(sendSearchUserRequestResult(std::vector<User>)), gui,
            SIGNAL(sendSearchUserRequestResult(std::vector<User>)));
    connect(this, SIGNAL(sendAddUserToLibraryRequestResult(bool)), gui,
            SIGNAL(sendAddUserToLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendEditUserInLibraryRequestResult(bool)), gui,
            SIGNAL(sendEditUserInLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveUserFromLibraryRequestResult(bool)), gui,
            SIGNAL(sendRemoveUserFromLibraryRequestResult(bool)));
}

void App::getSearchBookRequest(QString id, QString title) {
    std::vector<SmartPointer<Book>> libraryBooks = library.getBooks()->getItems();
    std::vector<Book> searchResult;

    for (unsigned int i = 0; i < libraryBooks.size(); i++) {
        QString bookId = QString::fromUtf8(libraryBooks[i].getData()->getID().c_str());
        QString bookTitle = QString::fromUtf8(libraryBooks[i].getData()->getName().c_str());

        if (bookId.contains(id) || bookTitle.contains(title)) {
            searchResult.push_back(*libraryBooks[i].getData());
        }
    }

    emit sendSearchBookRequestResult(searchResult);
}

void App::getWishlistRequest() {
    emit sendWishlistRequestResult(getCurrentUserWishlist());
}

void App::getAddBookToWishlistRequest(Book book) {
    try {
        user->addBookToWishList(book);
        emit sendAddBookToWishlistRequestResult(true);
    } catch (...) {
        emit sendAddBookToWishlistRequestResult(false);
    }
}

void App::getRemoveBookFromWishlistRequest(Book book) {
    try {
        user->removeBookFromWishList(book);
        emit sendAddBookToWishlistRequestResult(true);
    } catch (...) {
        emit sendAddBookToWishlistRequestResult(false);
    }
}

void App::addSampleBooks() {
    library.addBook(
            new Book(
                    "3271897391",
                    "42: a life full of surprises",
                    "Boy! You MUST read this book!",
                    true,
                    false
            )
    );

    library.addBook(
            new Book(
                    "6545241210",
                    "The Bible",
                    "Same old story",
                    true,
                    false
            )
    );
}

void App::addSampleUsers() {
    signUp(
            User(
                    "Bepi",
                    "Sugaman",
                    QDate(1901, 11, 11),
                    "bepisugaman@gmail.com"
            ), "bepi"
    );

    signUp(
            User(
                    "Jesus",
                    "Geppetto",
                    QDate(0, 12, 25),
                    "jesus@gmail.com"
            ), "god"
    );
}

void App::getReserveBookRequest(Book book) {
    try {
        Book *bookToReserve = library.findBook(new Book(book)).getData();  // find book and reserve
        if (user->reserveBook(bookToReserve)) {
            bookToReserve->reserve();
            emit sendReserveBookRequestResult(true);
        } else {
            emit sendReserveBookRequestResult(false);
        }
    } catch (...) {
        emit sendReserveBookRequestResult(false);
    }
}

void App::getReturnBookRequest(Book book) {
    try {
        Book *bookToReturn = library.findBook(new Book(book)).getData();  // find book and return
        emit sendReturnBookRequestResult(user->returnBook(bookToReturn));
    } catch (...) {
        emit sendReturnBookRequestResult(false);
    }
}

void App::getDetailsAboutBookRequest(QString isbn) {
    Book book = *library.findBook(new Book(isbn.toStdString())).getData();  // find complete details about book
    bool isReservedByUser = user->isBookReservedByUser(book);  // find if book is reserved by the user
    bool isInUserWishlist = user->isBookInWishlist(book);  // find if book is in user wishlist

    emit sendDetailsAboutBookRequestResult(book, isReservedByUser, isInUserWishlist);  // send data back
}

void App::getLoansRequest() {
    emit sendLoansRequestResult(user->getLoans());
}

bool App::editUser(User *user, User newUser) {
    if (user && library.isUserInDatabase(*user)) {
        user->setName(newUser.getName());
        user->setSurname(newUser.getSurname());
        user->setDateOfBirth(newUser.getDateOfBirth());

        return !(newUser.getEmail() != user->getEmail()) || editUserUsername(this->user, user->getEmail());

    }

    return false;
}

bool App::editUserPassword(User *user, std::string newPassword) {
    return oAuth.editPassword(user->getEmail(), newPassword);
}

bool App::editUserUsername(User *user, std::string newUserName) {
    std::string oldUsername = user->getEmail();
    user->setEmail(newUserName);  // edit username
    return oAuth.editUsername(oldUsername, newUserName);  // edit credentials
}

void App::getEditUserRequest(User user, bool isAdmin, QString password) {
    emit sendEditUserRequestResult(
            editUserAndChangeAdminStatus(QString::fromUtf8(user.getEmail().c_str()), user, isAdmin, password));
}

void App::getCurrentUserRequest() {
    getUserRequest(user->getEmail().c_str());
}

void App::getAddBookToLibraryRequest(Book book) {
    if (library.isBookInDatabase(book)) {  // cannot add already in book
        emit sendAddBookToLibraryRequestResult(false);
    } else {
        library.addBook(new Book(book));
        emit sendAddBookToLibraryRequestResult(true);
    }
}

void App::getEditBookInLibraryRequest(QString oldBookIsbn, Book newBook) {
    Book *bookToEdit = library.findBook(new Book(oldBookIsbn.toStdString())).getData();
    try {
        library.editBook(bookToEdit, new Book(newBook));
        emit sendEditBookInLibraryRequestResult(true);
    } catch (...) {
        emit sendEditBookInLibraryRequestResult(false);
    }
}

void App::getRemoveBookFromLibraryRequest(Book book) {
    SmartPointer<Book> bookToRemove = library.findBook(new Book(book));

    bool canRemoveBook = bookToRemove.getData() && library.isBookInDatabase(book) && !bookToRemove->isBookedByUser();
    sendRemoveBookFromLibraryRequestResult(canRemoveBook && library.removeBook(bookToRemove));
}

void App::getSearchUserRequest(QString id, QString name) {
    std::vector<SmartPointer<User>> libraryUsers = library.getUsers()->getItems();
    std::vector<User> searchResult;

    for (unsigned int i = 0; i < libraryUsers.size(); i++) {
        QString userId = QString::fromUtf8(libraryUsers[i].getData()->getEmail().c_str());
        QString userName = QString::fromUtf8(libraryUsers[i].getData()->getName().c_str());
        QString userSurname = QString::fromUtf8(libraryUsers[i].getData()->getSurname().c_str());

        if (userId.contains(id) || userName.contains(name) || userSurname.contains(name)) {
            searchResult.push_back(*libraryUsers[i].getData());
        }
    }

    emit sendSearchUserRequestResult(searchResult);
}

void App::getAddUserToLibraryRequest(User user, bool makeAdmin) {
    if (!library.isUserInDatabase(user)) {  // cannot add to library already in username
        if (makeAdmin) {
            library.addUser(new Admin(new User(user)));
        } else {
            library.addUser(new User(user));
        }

        oAuth.addId(OAuthId(QString::fromUtf8(user.getEmail().c_str()), QString::fromUtf8(user.getEmail().c_str())));
        emit sendAddUserToLibraryRequestResult(true);
    } else {
        emit sendAddUserToLibraryRequestResult(false);
    }
}

void App::getRemoveUserFromLibraryRequest(User user) {
    try {
        emit sendRemoveUserFromLibraryRequestResult(removeUser(user));
    } catch (...) {
        emit sendRemoveUserFromLibraryRequestResult(false);
    }
}

bool App::removeUser(User user) {
    SmartPointer<User> userToRemove = library.findUser(new User(user));

    bool canRemoveUser =
            userToRemove.getData() && library.isUserInDatabase(*userToRemove) && userToRemove.getData() != this->user;
    if (canRemoveUser) {  // current user cannot remove current user
        std::vector<Book> booksLoanedToUser = userToRemove->getLoans();  // make available books that were loaned to the user

        if (library.removeUser(userToRemove)) {  // if library has successfully removed user
            for (unsigned int i = 0; i < booksLoanedToUser.size(); i++) {
                Book *bookToReturn = library.findBook(new Book(booksLoanedToUser[i])).getData();
                bookToReturn->returnToLibrary();  // return book
            }

            return true;
        } else {
            return false;
        }
    }
    return false;
}

void App::getEditUserInLibraryRequest(QString oldId, User newUser, bool isAdmin, QString newPassword) {
    emit sendEditUserInLibraryRequestResult(editUserAndChangeAdminStatus(oldId, newUser, isAdmin, newPassword));
}

void App::getUserRequest(QString username) {
    User *userToSendInfoAbout = library.findUser(new User(username.toStdString())).getData();
    if (userToSendInfoAbout) {
        Admin *candidateAdmin = dynamic_cast<Admin *>(userToSendInfoAbout);
        bool isAdmin = candidateAdmin && candidateAdmin->isAdmin();

        emit sendUserRequestResult(*userToSendInfoAbout, isAdmin);
    }
}

bool App::editUserAndChangeAdminStatus(QString oldId, User newUser, bool makeAdmin, QString newPassword) {
    try {
        User *userToEdit = library.findUser(new User(oldId.toStdString())).getData();
        bool userEdited = editUser(userToEdit, newUser);  // edit user

        if (makeAdmin) {
            if (isCurrentUserAnAdmin()) {
                Admin *currentUser = dynamic_cast<Admin *> (user);
                SmartPointer<User> normalUser = library.findUser(new User(newUser.getEmail()));
                SmartPointer<User> newAdmin = currentUser->makeAdmin(normalUser.getData());
                library.editUser(normalUser, newAdmin);  // make admin
            }
        } else {
            SmartPointer<User> adminToChange = library.findUser(new User(newUser.getEmail()));
            library.editUser(adminToChange, new User(newUser));  // make admin
        }

        if (userEdited) {
            bool passwordEdited = true;  // edit password
            if (newPassword != NULL && newPassword.length() > 0) {
                passwordEdited = editUserPassword(library.findUser(new User(newUser)).getData(),
                                                  newPassword.toStdString());  // edit password
            }

            return userEdited && passwordEdited;
        } else {
            return false;
        }
    } catch (...) {
        return false;
    }
}

void App::getImportWishlistFromFileRequest(QString pathToFileToImport) {
    try {
        if (pathToFileToImport.length() > 1) {
            std::vector<Book> userBooks = io.readBooks(pathToFileToImport);
            for (unsigned int i = 0; i < user->getWishList().size(); i++) {  // remove all user wishlist
                user->removeBookFromWishList(user->getWishList()[i]);
            }

            for (unsigned int i = 0; i < userBooks.size(); i++) {  // replace wishlist books
                user->addBookToWishList(userBooks[i]);
            }
            emit sendImportWishlistFromFileRequestResult(true);
            emit sendWishlistRequestResult(getCurrentUserWishlist());  // send signal to update wishlist
        }
    } catch (...) {
        emit sendImportWishlistFromFileRequestResult(false);
    }
}

void App::getExportWishlistFromFileRequest(QString pathToFileToExport) {
    try {
        if (pathToFileToExport.length() > 1) {
            io.writeBooks(user->getWishList(), pathToFileToExport);
            emit sendExportWishlistFromFileRequestResult(true);
        }
    } catch (...) {
        emit sendExportWishlistFromFileRequestResult(false);
    }
}

std::vector<Book> App::getCurrentUserWishlist() {
    std::vector<Book> userWishlist = user->getWishList();
    std::vector<Book> userWishlistBookDetailsComplete;
    for (unsigned int i = 0; i < userWishlist.size(); i++) {
        userWishlistBookDetailsComplete.push_back(*library.findBook(new Book(userWishlist[i])).getData());
    }

    return userWishlistBookDetailsComplete;
}
