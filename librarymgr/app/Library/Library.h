/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_LIBRARY_H
#define LIBRARYMGR_LIBRARY_H

#include <vector>
#include <ostream>

#include "../../models/People/User/User.h"
#include "../../models/People/Admin/Admin.h"
#include "../../utils/SmartPointer/SmartPointer.h"

/**
 * @brief model of library with users and books
 */
class Library {
public:
    // constructors
    Library();

    /**
     * @brief Library new library
     * @param l const library to copy
     */
    Library(const Library &l);

    /**
     * Deconstructor
     */
    ~Library();

    // setters

    /**
     * @brief adds new user to library
     * @param user new user to add
     */
    void addUser(SmartPointer<User> user);

    /**
     * @brief adds new book to library
     * @param book new book to add
     */
    void addBook(SmartPointer<Book> book);

    /**
     * Removes user from library
     * @param user user to remove
     * @return true iff operation goes well
     */
    bool removeUser(SmartPointer<User> user);

    /**
     * @brief removes book from library
     * @param book book to remove
     * @return true iff operation went well
     */
    bool removeBook(SmartPointer<Book> book);

    /**
     * @brief replaces selected user with new one
     * @param oldUser user to replace
     * @param newUser new user
     */
    void editUser(SmartPointer<User> oldUser, SmartPointer<User> newUser);

    /**
     * @brief replaces selected book with new one
     * @param oldBook book to replace
     * @param newBook new book
     */
    void editBook(SmartPointer<Book> oldBook, SmartPointer<Book> newBook);

    // getters

    /**
     * Checks if user is in database
     * @param user user to find
     * @return true iff user is in database
     */
    bool isUserInDatabase(User user) const;

    /**
     * Checks if book is in database
     * @param book book to find
     * @return true iff book is in database
     */
    bool isBookInDatabase(Book book) const;

    /**
     * @brief findUser finds users matching query
     * @param user user to find
     * @return users matching query
     */
    SmartPointer<User> findUser(SmartPointer<User> user) const;

    /**
     * @brief findBook finds books matching query
     * @param book book to find
     * @return books matching query
     */
    SmartPointer<Book> findBook(SmartPointer<Book> book) const;

    /**
     * @brief getUsers gets users (also admins) of library
     * @return list of users in library
     */
    Database<SmartPointer<User>> *getUsers() const;

    /**
     * @brief getUsers gets admins of library
     * @return list of admins in library
     */
    Database<SmartPointer<Admin>> *getAdmins() const;

    /**
     * @brief getBooks gets books in library
     * @return list of books in library
     */
    Database<SmartPointer<Book>> *getBooks() const;

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Library &library);

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Library *library);

private:
    Database<SmartPointer<Book>> *books;  // list of books in library
    Database<SmartPointer<User>> *users;  // list of users in library

    /**
     * Checks if user is the only admin left
     * @param user user to check if only admin left
     * @return true iff user is the only admin left
     */
    bool isTheOnlyAdminLeft(SmartPointer<User> user);
};

#endif // LIBRARYMGR_LIBRARY_H
