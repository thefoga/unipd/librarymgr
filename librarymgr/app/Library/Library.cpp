/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <typeinfo>
#include "Library.h"

Library::Library() {
    books = new Database<SmartPointer<Book>>();
    users = new Database<SmartPointer<User>>();
}

Library::Library(const Library &l) {
    books = l.books;
    users = l.users;
}

Library::~Library() {}

void Library::addUser(SmartPointer<User> user) {
    users->add(user);
}

void Library::addBook(SmartPointer<Book> book) {
    books->add(book);
}

bool Library::removeUser(SmartPointer<User> user) {
    try {
        if (!isTheOnlyAdminLeft(user)) {
            users->remove(user);
            return true;
        } else {
            return false;
        }
    } catch (...) {
        return false;
    }
}

bool Library::removeBook(SmartPointer<Book> book) {
    try {
        books->remove(book);

        std::vector<SmartPointer<User>> libraryUsers = getUsers()->getItems();  // remove book also from users wishlist
        for (unsigned int i = 0; i < libraryUsers.size(); i++) {
            libraryUsers[i].getData()->removeBookFromWishList(*book.getData());
        }

        return true;
    } catch (...) {
        return false;
    }
}

void Library::editUser(SmartPointer<User> oldUser, SmartPointer<User> newUser) {
    users->replace(oldUser, newUser);
}

void Library::editBook(SmartPointer<Book> oldBook, SmartPointer<Book> newBook) {
    books->replace(oldBook, newBook);
}

SmartPointer<User> Library::findUser(SmartPointer<User> user) const {
    return users->search(user);
}

SmartPointer<Book> Library::findBook(SmartPointer<Book> book) const {
    return books->search(book);
}

Database<SmartPointer<User>> *Library::getUsers() const {
    return users;
}

Database<SmartPointer<Admin>> *Library::getAdmins() const {
    Database<SmartPointer<Admin>> *admins = new Database<SmartPointer<Admin>>();  // list of admins
    for (unsigned int i = 0; i < users->size(); i++) {
        try {
            std::vector<SmartPointer<User>> smartUserWhoWantsToBeAnAdmin = users->getItems();
            Admin *adminCandidate = dynamic_cast<Admin *> (smartUserWhoWantsToBeAnAdmin[i].getData());
            if (adminCandidate && adminCandidate->isAdmin()) {
                Admin *adminToAdd = new Admin(getUsers()->getItems()[i], books, users);
                admins->add(SmartPointer<Admin>(adminToAdd));
            }
        } catch (...) {
            // evidently it was NOT an admin...
        }

    }

    return admins;
}

Database<SmartPointer<Book>> *Library::getBooks() const {
    return books;
}

std::ostream &operator<<(std::ostream &out, const Library &library) {
    return out << "Library {" << std::endl
               << "books: " << *library.books << std::endl
               << "users: " << *library.users << std::endl
               << "}";
}

std::ostream &operator<<(std::ostream &out, const Library *library) {
    return out << *library;
}

bool Library::isUserInDatabase(User user) const {
    return users->isInDatabase(new User(user));
}

bool Library::isBookInDatabase(Book book) const {
    return books->isInDatabase(new Book(book));
}

bool Library::isTheOnlyAdminLeft(SmartPointer<User> user) {
    try {
        Admin *candidateAdmin = dynamic_cast<Admin *>(user.getData());
        bool isAdmin = candidateAdmin && candidateAdmin->isAdmin();
        return isAdmin && getAdmins()->size() <= 1;
    } catch (...) {
        return false;
    }
}
