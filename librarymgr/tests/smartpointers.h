/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_SMARTPOINTERS_H
#define LIBRARYMGR_SMARTPOINTERS_H

#include <assert.h>

#include "../app/Library/Library.h"

/**
 * Testing area on smart pointers
 * @return always 0 (if anything goes wrong it stops the execution)
 */
int testSmartPointers() {
    Book *book0 = new Book("vheruoho");
    SmartPointer<Book> *sbook0 = new SmartPointer<Book>(book0);
    delete sbook0;

    return 0;
}

#endif //LIBRARYMGR_SMARTPOINTERS_H
