/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_TESTS_OAUTH_H
#define LIBRARYMGR_TESTS_OAUTH_H

#include <assert.h>
#include <string>
#include <QString>

#include "../auth/OAuth/OAuth.h"

/**
 * Test mechanism for user authentication
 * @return always 0 (if anything goes wrong it stops the execution)
 */
int testOAuth() {
    OAuth auth;  // empty database

    OAuthId user(QString::fromStdString("bepi@sugaman.com"), QString::fromStdString("1"));  // create user

    auth.addId(user);  // add user to database

    assert(!auth.wrongUser(user));
    assert(!auth.wrongPassword(user));
    assert(auth.authenticate(user));

    OAuthId nonAuthenticatedUser(QString::fromStdString("bepi"), QString::fromStdString("1"));

    assert(auth.wrongUser(nonAuthenticatedUser));
    assert(!auth.wrongPassword(nonAuthenticatedUser));
    assert(!auth.authenticate(nonAuthenticatedUser));

    auth.removeId(user);  // remove user

    assert(auth.wrongUser(user));
    assert(!auth.wrongPassword(user));
    assert(!auth.authenticate(user));

    return 0;
}

#endif  //LIBRARYMGR_TESTS_OAUTH_H