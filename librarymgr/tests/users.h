/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USERS_H
#define LIBRARYMGR_USERS_H

#include <assert.h>

#include "../app/Library/Library.h"

/**
 * Testing area of users and admins in library
 * @return always 0 (if anything goes wrong it stops the execution)
 */
int testUsers() {
    Library library;

    Admin *a0 = new Admin("admin0", "admin01", QDate(1955, 12, 19), "a0@g.com", library.getBooks(), library.getUsers());
    Admin *a1 = new Admin("admin1", "admin11", QDate(1977, 12, 19), "a1@g.com", library.getBooks(), library.getUsers());
    User *u0 = new User("u0", "u01", QDate(1952, 12, 19), "u0@g.com");
    library.addUser(a0);
    library.addUser(u0);
    library.addUser(a1);

    assert(library.getUsers()->size() == 3);
    assert(library.getAdmins()->size() == 2);

    SmartPointer<User> userToMakeAdmin = library.findUser(new User("u0", "u01", QDate(1952, 12, 19), "u0@g.com"));
    SmartPointer<User> userBecameAdmin = a0->makeAdmin(userToMakeAdmin.getData());
    library.editUser(userToMakeAdmin, userBecameAdmin);

    assert(userBecameAdmin.getData()->isAdmin());
    assert(library.getAdmins()->size() == 3);

    a0->addUser(new User("u0", "u01", QDate(1952, 12, 19), "u0@g.com"));
    library.getAdmins()->getItems()[2]->addUser(new User("u5", "u01", QDate(1952, 12, 19), "u5@g.com"));

    assert(library.getUsers()->size() == 4);
    assert(library.getAdmins()->size() == 3);

    return 0;
}

#endif //LIBRARYMGR_USERS_H
