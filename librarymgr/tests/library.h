/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_TESTS_LIBRARY_H
#define LIBRARYMGR_TESTS_LIBRARY_H

#include <assert.h>

#include "../app/Library/Library.h"

/**
 * Testing area on library classes
 * @return always 0 (if anything goes wrong it stops the execution)
 */
int testLibrary() {
    Library library;

    library.addBook(new Book("0", "title 0", "description", true, true));
    library.addBook(new Book("0", "title 0", "description", true, true));
    library.addBook(new Book("1", "title 1", "description", true, false));
    library.addBook(new Book("2", "title 2", "description", true, false));
    library.addBook(new Book("3", "title 3", "description", true, true));

    Admin *a0 = new Admin("admin0", "admin01", QDate(1955, 12, 19), "a0@g.com", library.getBooks(), library.getUsers());
    Admin *a1 = new Admin("admin1", "admin11", QDate(1977, 12, 19), "a1@g.com", library.getBooks(), library.getUsers());
    User *u0 = new User("u0", "u01", QDate(1952, 12, 19), "u0@g.com");
    library.addUser(a0);
    library.addUser(u0);
    library.addUser(a1);

    library.removeBook(library.getBooks()->getItems()[0]);
    a0->removeBook(library.getBooks()->getItems()[0]);

    SmartPointer<User> userToMakeAdmin = library.findUser(new User("u0@g.com"));
    SmartPointer<User> userBecameAdmin = a0->makeAdmin(userToMakeAdmin.getData());
    library.editUser(userToMakeAdmin, userBecameAdmin);

    a0->addUser(new User("u0", "u01", QDate(1952, 12, 19), "u0@g.com"));
    a0->addBook(new Book("0", "title 0", "description", true, true));
    library.getAdmins()->getItems()[0]->addBook(new Book("4", "title 4", "description", true, false));
    library.getAdmins()->getItems()[2]->addUser(new User("u5", "u01", QDate(1952, 12, 19), "u5@g.com"));

    assert(a0->reserveBook(library.findBook(new Book("4")).getData()));  // admin can reserve book 4
    assert(library.findBook(new Book("4")).getData()->isBookedByUser());  // book 4 is reserved
    assert(!a0->reserveBook(library.findBook(new Book("0")).getData()));  // admin cannot reserve book 0
    assert(a0->reserveBook(library.findBook(new Book("2")).getData()));  // admin can reserve book 2

    assert(!a0->isBookReservedByUser(*library.findBook(new Book("0")).getData()));  // book 0 is not booked by admin
    assert(a0->isBookReservedByUser(*library.findBook(new Book("4")).getData()));  // book 4 is booked by admin
    assert(a0->isBookReservedByUser(*library.findBook(new Book("2")).getData()));  // book 2 is booked by admin

    assert(a0->returnBook(library.findBook(new Book("2")).getData()));  // admin can return book 2
    assert(!library.findBook(new Book("2")).getData()->isBookedByUser());  // book 2 is not booked by anyone

    assert(!a1->reserveBook(library.findBook(new Book("4")).getData()));  // other admin cannot reserve book 4
    assert(library.findUser(new User("u5", "u01", QDate(1952, 12, 19), "u5@g.com"))->reserveBook(
            library.findBook(new Book("2")).getData()));  // other user can reserve book 2


    std::vector<SmartPointer<Book>> books = library.getBooks()->getItems();  // get books in library
    for (unsigned int i = 0; i < books.size(); i++) {  // remove all books from library
        library.removeBook(books[i]);
    }

    assert(library.getBooks()->isEmpty());

    std::vector<SmartPointer<User>> users = library.getUsers()->getItems();  // get users in library
    for (unsigned int i = 0; i < users.size(); i++) {  // remove all users from library
        library.removeUser(users[i]);
    }

    assert(library.getUsers()->size() == 1);  // there is only one admin left
    assert(library.getAdmins()->size() == 1);

    return 0;
}

#endif  //LIBRARYMGR_TESTS_LIBRARY_H
