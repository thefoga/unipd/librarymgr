/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_SMARTPOINTER_H
#define LIBRARYMGR_SMARTPOINTER_H

#include "ReferenceCounter.h"

/**
 * Wraps a 'raw' (or 'bare') C++ pointer, to manage the lifetime of the object being pointed to
 */
template<typename T>
class SmartPointer {
public:
    /**
     * Default smart pointer with null values
     * @return
     */
    SmartPointer() : SmartPointer(0) {
        referenceCounter = new ReferenceCounter();
        referenceCounter->addRef();
    }

    /**
     * Creates smart pointer with pointer value
     * @param pValue pointer value
     * @return new smart pointer
     */
    SmartPointer(T *pValue) : pointerData(pValue), referenceCounter(0) {
        referenceCounter = new ReferenceCounter();
        referenceCounter->addRef();
    }

    /**
     * Copy constructor: copies the data and reference pointer and increment the reference count
     * @param smartPointer smart pointer to copy
     * @return new smart pointer copied
     */
    SmartPointer(const SmartPointer<T> &smartPointer) : pointerData(smartPointer.pointerData),
                                                        referenceCounter(smartPointer.referenceCounter) {
        referenceCounter->addRef();
    }

    /**
     * If there are no more references it deletes the smart pointer
     */
    ~SmartPointer() {
        if (referenceCounter->release() < 1) {  // there are no more references
            delete pointerData;
            delete referenceCounter;
        }
    }

    /**
     * Gets inner pointer data
     * @return inner pointer data
     */
    T *getData() const {
        return pointerData;
    }

    // operators

    /**
     * Assignment operator
     * @param smartPointer other smart pointer
     * @return assigner smart pointer
     */
    SmartPointer<T> &operator=(const SmartPointer<T> &smartPointer) {
        if (this != &smartPointer) {  // avoid self assignment
            // decrement the old reference count
            if (referenceCounter->release() == 0) {
                delete pointerData;  // if reference becomes zero delete the old data
                delete referenceCounter;
            }

            // copy the data and reference pointer and increment the reference count
            pointerData = smartPointer.pointerData;
            referenceCounter = smartPointer.referenceCounter;
            referenceCounter->addRef();
        }

        return *this;
    }

    /**
     * Gets reference to the data
     * @return reference to the pointer data
     */
    T &operator*() const {
        return *pointerData;
    }

    /**
     * Gets reference to the data
     * @return reference to the pointer data
     */
    T *operator->() const {
        return pointerData;
    }

    /**
     * Equals other smart pointer based on data
     * @param other other smart pointer to compare with
     * @return true iff data are the same
     */
    bool operator==(const SmartPointer &other) const {
        return *getData() == *other.getData();
    }

    /**
     *
     * @param other
     * @return
     */
    bool operator!=(const SmartPointer &other) const {
        return !(*this == other);
    }

    /**
     * @brief operator << printer
     * @param out stream
     * @param item object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const SmartPointer &smartPointer) {
        return out << *smartPointer.getData();
    }

    /**
     * @brief operator << printer
     * @param out stream
     * @param item object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const SmartPointer *smartPointer) {
        return out << *smartPointer;
    }

private:
    T *pointerData; // generic pointer to be stored
    ReferenceCounter *referenceCounter;  // reference count
};


#endif //LIBRARYMGR_SMARTPOINTER_H
