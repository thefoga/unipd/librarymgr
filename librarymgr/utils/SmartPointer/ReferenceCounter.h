/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_REFERENCECOUNTER_H
#define LIBRARYMGR_REFERENCECOUNTER_H


/**
 * Maintain an integer value which represents the reference count for smart pointers.
 */
class ReferenceCounter {
public:
    /**
     * Increment the reference count
     */
    void addRef();

    /**
     * Decrement the reference count and
     * @return the reference count.
     */
    int release();

private:
    int count; // reference count
};


#endif //LIBRARYMGR_REFERENCECOUNTER_H
