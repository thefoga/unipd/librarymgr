QT       += core gui

QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LibraryMgr
TEMPLATE = app


SOURCES += main.cpp \
    app/App/App.cpp \
    app/Library/Library.cpp \
    app/Settings/Settings.cpp \
    gui/Gui/Gui.cpp \
    models/Article/Book/Book.cpp \
    models/People/Admin/Admin.cpp \
    models/People/Person/Person.cpp \
    models/People/User/User.cpp \
    gui/dialogs/AdminDialog/AdminDialog.cpp \
    gui/dialogs/LoginDialog/LoginDialog.cpp \
    gui/dialogs/SignInDialog/SignInDialog.cpp \
    gui/dialogs/SignUpDialog/SignUpDialog.cpp \
    gui/dialogs/UserDetailsDialog/UserDetailsDialog.cpp \
    gui/dialogs/UserDialog/UserDialog.cpp \
    gui/widgets/UserBooksWidget/UserBooksWidget.cpp \
    gui/widgets/UserPreferencesWidget/UserPreferencesWidget.cpp \
    gui/widgets/AdminLibraryManager/AdminLibraryManager.cpp \
    models/Article/Item/Item.cpp \
    utils/SmartPointer/ReferenceCounter.cpp \
    gui/widgets/InlineLabel/InlineLabel.cpp \
    auth/OAuth/OAuth.cpp \
    auth/OAuthId/OAuthId.cpp \
    tests/library.h \
    tests/oauth.h \
    streams/IOStream/IOStream.cpp \
    utils/utils/Utils.cpp \
    gui/widgets/AdminBooksLibraryManager/AdminBooksLibraryManager.cpp \
    gui/widgets/AdminUsersLibraryManager/AdminUsersLibraryManager.cpp \
    gui/dialogs/BookManagerDialog/BookManagerDialog.cpp \
    gui/dialogs/BookDetailsDialog/BookDetailsDialog.cpp \
    gui/dialogs/BookAdderDialog/BookAdderDialog.cpp \
    gui/dialogs/UserAdderDialog/UserAdderDialog.cpp \
    gui/dialogs/UserEditorDialog/UserEditorDialog.cpp

HEADERS += \
    app/App/App.h \
    app/Library/Library.h \
    app/Settings/Settings.h \
    models/Article/Book/Book.h \
    models/Database/Database.h \
    models/People/Admin/Admin.h \
    models/People/Person/Person.h \
    models/People/User/User.h \
    gui/Gui/Gui.h \
    gui/dialogs/AdminDialog/AdminDialog.h \
    gui/dialogs/LoginDialog/LoginDialog.h \
    gui/dialogs/SignInDialog/SignInDialog.h \
    gui/dialogs/SignUpDialog/SignUpDialog.h \
    gui/dialogs/UserDetailsDialog/UserDetailsDialog.h \
    gui/dialogs/UserDialog/UserDialog.h \
    gui/widgets/UserBooksWidget/UserBooksWidget.h \
    gui/widgets/UserPreferencesWidget/UserPreferencesWidget.h \
    utils/Demangle.h \
    gui/widgets/AdminLibraryManager/AdminLibraryManager.h \
    models/Article/Item/Item.h \
    utils/SmartPointer/ReferenceCounter.h \
    utils/SmartPointer/SmartPointer.h \
    gui/widgets/InlineLabel/InlineLabel.h \
    auth/OAuth/OAuth.h \
    auth/OAuthId/OAuthId.h \
    streams/IOStream/IOStream.h \
    utils/utils/Utils.h \
    tests/streams.h \
    tests/books.h \
    tests/users.h \
    gui/widgets/AdminBooksLibraryManager/AdminBooksLibraryManager.h \
    gui/widgets/AdminUsersLibraryManager/AdminUsersLibraryManager.h \
    gui/dialogs/BookManagerDialog/BookManagerDialog.h \
    gui/dialogs/BookDetailsDialog/BookDetailsDialog.h \
    gui/dialogs/BookAdderDialog/BookAdderDialog.h \
    gui/dialogs/UserAdderDialog/UserAdderDialog.h \
    gui/dialogs/UserEditorDialog/UserEditorDialog.h

RESOURCES += \
    res/res.qrc

DISTFILES += \
    CMakeLists.txt \
    streams/IOStream/sample_auth.json \
    streams/IOStream/sample_books.json \
    streams/IOStream/sample_users.json
