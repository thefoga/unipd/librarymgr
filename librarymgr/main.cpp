/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QApplication>
#include <QDir>
#include <QtWidgets/QSplashScreen>

#include "app/App/App.h"
#include "tests/library.h"
#include "tests/oauth.h"
#include "tests/users.h"
#include "tests/books.h"
#include "tests/smartpointers.h"

int runTests() {
    testSmartPointers();
    testUsers();
    testBooks();
    testOAuth();
    testLibrary();

    return 0;
}

int runApp(int argc, char *argv[]) {
    QApplication a(argc, argv);  // app wrapper
    App app;  // create new app
    app.start();  // start app

    return a.exec();  // classic return statement in Qt
}

int main(int argc, char *argv[]) {
    QCoreApplication::setOrganizationName(QString::fromUtf8("LibraryMgr"));  // set app settings
    QCoreApplication::setApplicationName(QString::fromUtf8("LibraryMgr"));

    runTests();  // run tests before launching app
    return runApp(argc, argv);
}
