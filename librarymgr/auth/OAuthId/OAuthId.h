/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_OAUTHID_H
#define LIBRARYMGR_OAUTHID_H

#include <QtCore/QByteArray>
#include <QString>


/**
 * Provides OAuth credential for a user of an app
 */
class OAuthId {
public:
    /**
     * New credentials for user with given password
     * @param username username to login in app
     * @param password password to login in app
     * @return Credentials to login
     */
    OAuthId(QString username, QString password);

    /**
     * Credentials with given hashes; useful when checking credentials from database.
     * @param userHash hash of username
     * @param passwordHash hash of password
     * @return Credentials to login
     */
    OAuthId(QByteArray userHash, QByteArray passwordHash);

    // getters

    /**
     * Get username hash
     * @return username hash (utf8 encoded)
     */
    const QString getUserHash() const;

    /**
     * Get password hash
     * @return password hash (utf8 encoded)
     */
    const QString getPasswordHash() const;

    // operators

    /**
     * Assignment operator
     * @param id other id
     * @return assigner id
     */
    OAuthId &operator=(const OAuthId &id);

    /**
    * @brief checks password hash of other credentials
    * @param other other object to compare with
    * @return true iff 2 items are the same
    */
    bool operator==(const OAuthId &other) const;

    /**
     * @brief checks password hash of other credentials
     * @param other other object to compare with
     * @return true iff 2 items are the same
     */
    bool operator!=(const OAuthId &other) const;

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const OAuthId &oauthId);

    /**
     * Computes hash of given stuff
     * @param toBeHashed stuff to be hashed
     * @return hash of stuff
     */
    static QByteArray getHash(std::string toBeHashed);

private:
    QByteArray userHash;
    QByteArray passwordHash;
};


#endif //LIBRARYMGR_OAUTHID_H
