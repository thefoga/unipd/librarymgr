/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtCore/QCryptographicHash>
#include <iostream>

#include "OAuthId.h"

OAuthId::OAuthId(QByteArray userHash, QByteArray passwordHash) : userHash(userHash), passwordHash(passwordHash) {}

OAuthId::OAuthId(QString username, QString password)
        : userHash(getHash(username.toStdString())),
          passwordHash(getHash(password.toStdString())) {}

const QString OAuthId::getUserHash() const {
    return QString(userHash).toUtf8();
}

const QString OAuthId::getPasswordHash() const {
    return QString(passwordHash).toUtf8();
}

bool OAuthId::operator==(const OAuthId &other) const {
    return getUserHash() == other.getUserHash() && getPasswordHash() == other.getPasswordHash();
}

bool OAuthId::operator!=(const OAuthId &other) const {
    return !(*this == other);
}

OAuthId &OAuthId::operator=(const OAuthId &id) {
    if (this != &id) {  // avoid self assignment
        userHash = QString(id.getUserHash()).toUtf8();
        passwordHash = QString(id.getPasswordHash()).toUtf8();
    }

    return *this;
}

std::ostream &operator<<(std::ostream &out, const OAuthId &oauthId) {
    return out << "OAuthId {" << std::endl
               << "username hash: " << oauthId.getUserHash().toStdString() << std::endl
               << "password hash: " << oauthId.getPasswordHash().toStdString() << std::endl
               << "}";
}

QByteArray OAuthId::getHash(std::string toBeHashed) {
    return QCryptographicHash::hash(toBeHashed.c_str(), QCryptographicHash::Sha3_512);
}
