/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "OAuth.h"

OAuth::OAuth() {}

OAuth::OAuth(Database<OAuthId> database) : database(database) {}

bool OAuth::authenticate(OAuthId id) {
    return database.isInDatabase(id);
}

bool OAuth::wrongUser(OAuthId id) {
    for (unsigned int i = 0; i < database.size(); i++) {
        if (database[i].getUserHash() == id.getUserHash()) {  // there IS a user with same name
            return false;
        }
    }

    return true;  // there is no user with same name
}

bool OAuth::wrongPassword(OAuthId id) {
    return !wrongUser(id) && !authenticate(id);
}

bool OAuth::addId(OAuthId id) {
    try {
        database.add(id);  // allow no duplicates
        return true;
    } catch (...) {
        return false;
    }
}

bool OAuth::removeId(OAuthId id) {
    try {
        database.remove(id);  // allow no duplicates
        return true;
    } catch (...) {
        return false;
    }
}

bool OAuth::editId(OAuthId oldId, OAuthId newId) {
    try {
        database.replace(oldId, newId);  // allow no duplicates
        return true;
    } catch (...) {
        return false;
    }
}

std::ostream &operator<<(std::ostream &out, const OAuth &oauth) {
    return out << oauth.database;
}

const Database<OAuthId> &OAuth::getDatabase() const {
    return database;
}

OAuthId OAuth::getId(std::string username) {
    OAuthId idToFind = OAuthId(QString::fromUtf8(username.c_str()), "");  // sample id to get user hash
    std::vector<OAuthId> iDs = database.getItems();

    for (unsigned int i = 0; i < iDs.size(); i++) {
        if (iDs[i].getUserHash() == idToFind.getUserHash()) {
            return iDs[i];
        }
    }

    return OAuthId(QByteArray(NULL), QByteArray(NULL));
}

bool OAuth::editUsername(std::string username, std::string newUsername) {
    OAuthId oldAuth = getId(username);  // finds old credentials
    if (oldAuth.getPasswordHash() != NULL) {
        QByteArray oldPassword = oldAuth.getPasswordHash().toUtf8();
        OAuthId newAuth = OAuthId(OAuthId::getHash(newUsername), oldPassword); // generates new credentials

        return editId(oldAuth, newAuth);  // replace credentials
    }

    return false;
}

bool OAuth::editPassword(std::string username, std::string newPassword) {
    OAuthId oldAuth = getId(username);  // finds old credentials
    if (oldAuth.getPasswordHash() != NULL) {
        OAuthId newAuth = OAuthId(OAuthId::getHash(username),
                                  OAuthId::getHash(newPassword)); // generates new credentials

        return editId(oldAuth, newAuth);  // replace credentials
    }

    return false;
}
