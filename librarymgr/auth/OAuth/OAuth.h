/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Manages authentication of app
 */
#ifndef LIBRARYMGR_OAUTH_H
#define LIBRARYMGR_OAUTH_H

#include "../OAuthId/OAuthId.h"
#include "../../models/Database/Database.h"

/**
 * Credentials pool to manage multi-user app
 */
class OAuth {

public:
    /**
     * New OAuth manager with empty database
     * @return New OAuth manager
     */
    OAuth();

    /**
     * New OAuth manager with credentials database
     * @param database credentials database
     * @return New OAuth manager
     */
    OAuth(Database<OAuthId> database);

    /**
     * Gets inner database of credentials
     * @return database of credentials
     */
    const Database<OAuthId> &getDatabase() const;

    // getters

    /**
     * Fetches credentials of selected user
     * @param username username to fetch
     * @return credentials of selected user
     */
    OAuthId getId(std::string username);

    // check

    /**
     * Tries to authenticate user woth username and password
     * @param id user to authenticate
     * @return true if user is authenticated
     */
    bool authenticate(OAuthId id);

    /**
     * Checks if user is not authenticated because of a wrong username
     * @param id user to authenticate
     * @return true if user is not authenticated because of a wrong username
     */
    bool wrongUser(OAuthId id);

    /**
     * Checks if user is not authenticated because of a wrong password
     * @param id user to authenticate
     * @return true if user is not authenticated because of a wrong password
     */
    bool wrongPassword(OAuthId id);

    // management

    /**
     * Adds new ID to database
     * @param id ID to add
     * @return true if everything went well
     */
    bool addId(OAuthId id);

    /**
     * Removes ID from database
     * @param id ID to remove
     * @return true if everything went well
     */
    bool removeId(OAuthId id);

    /**
     * Edits username of given user
     * @param username username to edit
     * @param newUsername new username
     * @return true iff everything went well
     */
    bool editUsername(std::string username, std::string newUsername);

    /**
     * Edits password of given user
     * @param username username to edit
     * @param newPassword new password
     * @return true iff everything went well
     */
    bool editPassword(std::string username, std::string newPassword);

    /**
     * Edits new ID to database
     * @param oldId ID to edit
     * @param newId ID to replace with
     * @return true iff everything went well
     */
    bool editId(OAuthId oldId, OAuthId newId);

    // operators

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const OAuth &oauth);

private:
    Database<OAuthId> database;  // database of credentials
};


#endif //LIBRARYMGR_OAUTH_H
