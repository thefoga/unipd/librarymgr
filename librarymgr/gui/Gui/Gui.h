/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_GUI_H
#define LIBRARYMGR_GUI_H


#include <QtWidgets/QMainWindow>

#include "../dialogs/AdminDialog/AdminDialog.h"
#include "../dialogs/UserDialog/UserDialog.h"
#include "../dialogs/LoginDialog/LoginDialog.h"

/**
 * @brief Manages UI for the app
 */
class Gui : public QObject {
Q_OBJECT

public:
    /**
     * New GUI manager for the app
     * @return
     */
    explicit Gui();

    /**
     * Remove gui from screen and delete all references
     */
    ~Gui();

    /**
     * @brief starts GUI
     */
    void open();

public slots:
    // user

    /**
     * Gets information regardin admin status of the current user of the app
     * @param isCurrentUserAnAdmin true if current user of app is an admin
     */
    void getCurrentUserAdminStatus(bool isCurrentUserAnAdmin);

    // login

    /**
     * Sign in process is complete!
     */
    void getSignInProcessIsComplete();

signals:
    // login

    /**
     * Mark when a sign in request is made by the user via the GUI
     * @param username username to sign in with
     * @param password password of user to authenticate
     */
    void sendSignInRequest(QString username, QString password);

    /**
     * Sends sign in result
     * @param signInOk whether sign authentication went well
     */
    void sendSignInRequestResult(bool signInOk);

    /**
     * Mark when a sign in request is made by the user via the GUI
     * @param user new user to sign up
     * @param password password of user to authenticate
     */
    void sendSignUpRequest(User user, QString password);

    /**
     * Sends sign up result
     * @param signUpOk whether sign up process went well
     */
    void sendSignUpRequestResult(bool signUpOk);

    // user dialog
    // requests

    /**
     * Asks backend for details about book with isbn
     * @param isbn isbn of book to get details about
     */
    void sendDetailsAboutBookRequest(QString isbn);

    /**
     * Sends request to backend to search database for given book
     * @param id isbn code to search book
     * @param title title of book to search
     */
    void sendSearchBookRequest(QString id, QString title);

    /**
     * Sends request to backend to get wishlist of current user
     */
    void sendWishlistRequest();

    /**
     * Sends request to backend to get books loans of current user
     */
    void sendLoansRequest();

    /**
     * Sends request to backend to add new book to user wishlist
     * @param book
     */
    void sendAddBookToWishlistRequest(Book book);

    /**
     * Sends request to backend to remove book from user wishlist
     * @param book
     */
    void sendRemoveBookFromWishlistRequest(Book book);

    /**
     * Sends request to backend to reserve book
     * @param book book to reserve
     */
    void sendReserveBookRequest(Book book);

    /**
     * Sends request to backend to return book
     * @param book book to return
     */
    void sendReturnBookRequest(Book book);

    /**
     * Asks backend to send info about current user
     */
    void sendCurrentUserRequest();

    /**
     * Asks backend to send info about current user
     * @param username username of user to get info about
     */
    void sendUserRequest(QString username);

    /**
     * Asks backend to change current user to this new one
     * @param user new user
     * @param isAdmin true iff user to display is an admin
     * @param password new password
     */
    void sendEditUserRequest(User user, bool isAdmin, QString password);

    /**
     * Sends backend request to import books to user wishlist from given file
     * @param pathToFileToImport file to import books to user wishlist
     */
    void sendImportWishlistFromFileRequest(QString pathToFileToImport);

    /**
     * Sends backend request to export books to user wishlist from given file
     * @param pathToFileToExport file to export books to user wishlist
     */
    void sendExportWishlistFromFileRequest(QString pathToFileToExport);

    // results

    /**
 * Gets backend response on book details
 * @param book full book details
 * @param isReservedByUser true iff current user has reserved this book
 * @param isInUserWishlist true iff book is in current user wishlist
 */
    void sendDetailsAboutBookRequestResult(Book book, bool isReservedByUser, bool isInUserWishlist);

    /**
     * Gets user wishlist
     * @param book user books wishlist
     */
    void sendSearchBookRequestResult(std::vector<Book> books);

    /**
     * Sends request to backend to get wishlist of current user
     */
    void sendWishlistRequestResult(std::vector<Book> books);

    /**
     * Sends request to backend to get books loans of current user
     * @param books user books loans
     */
    void sendLoansRequestResult(std::vector<Book> books);

    /**
     * Checks if adding requested book to user wishlist operation went well
     * @param ok true iff operation went well
     */
    void sendAddBookToWishlistRequestResult(bool ok);

    /**
     * Checks if removing requested book from user wishlist operation went well
     * @param ok true iff operation went well
     */
    void sendRemoveBookFromWishlistRequestResult(bool ok);

    /**
     * Sends request to backend to reserve book
     * @param ok true iff operation went well
     */
    void sendReserveBookRequestResult(bool ok);

    /**
     * Sends request to backend to return book
     * @param ok true iff operation went well
     */
    void sendReturnBookRequestResult(bool ok);

    /**
     * Gets backend response to send info about current user
     * @param user current user
     * @param isAdmin true iff user to display is an admin
     */
    void sendUserRequestResult(User user, bool isAdmin);

    /**
     * Gets backend response on change current user settings
     * @param ok true iff operation went well
     */
    void sendEditUserRequestResult(bool ok);

    /**
     * Sends backend request to import books to user wishlist from given file
     * @param ok true iff operation went well
     */
    void sendImportWishlistFromFileRequestResult(bool ok);

    /**
     * Sends backend request to export books to user wishlist from given file
     * @param ok true iff operation went well
     */
    void sendExportWishlistFromFileRequestResult(bool ok);

    // admin dialog
    // books
    // requests

    /**
     * Sends request to backend to add new book to library
     * @param book book to add
     */
    void sendAddBookToLibraryRequest(Book book);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param oldBookIsbn isbn of book to edit
     * @param newBook new book
     */
    void sendEditBookInLibraryRequest(QString oldBookIsbn, Book newBook);

    /**
     * Sends request to backend to remove book from library
     * @param book book to remove
     */
    void sendRemoveBookFromLibraryRequest(Book book);

    // results

    /**
     * Sends request to backend to add new book to library
     * @param ok true iff operation went well
     */
    void sendAddBookToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param ok true iff operation went well
     */
    void sendEditBookInLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to remove book from library
     * @param ok true iff operation went well
     */
    void sendRemoveBookFromLibraryRequestResult(bool ok);

    // users
    // requests

    /**
     * Sends request to backend to search database for given user
     * @param id email of user to search
     * @param name name of user to search
     */
    void sendSearchUserRequest(QString id, QString name);

    /**
     * Sends request to backend to add new user to library
     * @param user new user to add
     * @param makeAdmin true iff new user should be an admin
     */
    void sendAddUserToLibraryRequest(User user, bool makeAdmin);

    /**
     * Sends request to backend to edit user
     * @param oldId email of user to edit
     * @param newUser new user
     * @param newPassword password of new user
     */
    void sendEditUserInLibraryRequest(QString oldId, User newUser, bool isAdmin, QString newPassword);

    /**
     * Sends request to backend to remove user from library
     * @param user user to remove
     */
    void sendRemoveUserFromLibraryRequest(User user);

    // results

    /**
     * Gets search results
     * @param users list of users matching query
     */
    void sendSearchUserRequestResult(std::vector<User> users);

    /**
     * Sends request to backend to add new user to library
     * @param ok true iff operation went well
     */
    void sendAddUserToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to edit user
     * @param ok true iff operation went well
     */
    void sendEditUserInLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to remove user from library
     * @param ok true iff operation went well
     */
    void sendRemoveUserFromLibraryRequestResult(bool ok);

private:
    bool isCurrentUserAnAdmin;  // true if current user of app is an admin
    LoginDialog *loginDialog;  // login dialog
    UserDialog *userDialog;  // dialog to let users manage library

    /**
     * Connect signal and slots of object managed by the gui
     */
    void connectSignalsSlots();

    /**
     * Opens login dialog
     */
    void openLoginDialog();

    /**
     * Opens dialog to let users manage library
     */
    void openUserDialog();
};

#endif // LIBRARYMGR_GUI_H
