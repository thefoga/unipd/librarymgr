/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "Gui.h"

Gui::Gui() {
    loginDialog = new LoginDialog();
    userDialog = new UserDialog();

    connectSignalsSlots();
}

Gui::~Gui() {}

void Gui::open() {
    openLoginDialog();
}

void Gui::openLoginDialog() {
    loginDialog->show();
}

void Gui::getCurrentUserAdminStatus(bool isCurrentUserAnAdmin) {
    this->isCurrentUserAnAdmin = isCurrentUserAnAdmin;
}

void Gui::getSignInProcessIsComplete() {
    openUserDialog();
}

void Gui::openUserDialog() {
    if (isCurrentUserAnAdmin) {
        userDialog = new AdminDialog();  // display admin dialog
        connectSignalsSlots();  // re-connect signal/slots for admin dialog
    }

    userDialog->setup();
    emit sendWishlistRequest();  // request update wishlist
    emit sendLoansRequest();  // request to update user loans table
    emit sendCurrentUserRequest();  // request to display user info

    userDialog->show();
}

void Gui::connectSignalsSlots() {
    // sign in
    connect(loginDialog, SIGNAL(sendSignInRequest(QString, QString)), this,
            SIGNAL(sendSignInRequest(QString, QString)));
    connect(loginDialog, SIGNAL(sendSignInProcessIsComplete()), this, SLOT(getSignInProcessIsComplete()));
    connect(this, SIGNAL(sendSignInRequestResult(bool)), loginDialog, SIGNAL(sendSignInRequestResult(bool)));

    // sing up
    connect(loginDialog, SIGNAL(sendSignUpRequest(User, QString)), this, SIGNAL(sendSignUpRequest(User, QString)));
    connect(this, SIGNAL(sendSignUpRequestResult(bool)), loginDialog, SIGNAL(sendSignUpRequestResult(bool)));

    // user dialog
    // send requests
    connect(userDialog, SIGNAL(sendSearchBookRequest(QString, QString)), this,
            SIGNAL(sendSearchBookRequest(QString, QString)));
    connect(userDialog, SIGNAL(sendWishlistRequest()), this, SIGNAL(sendWishlistRequest()));
    connect(userDialog, SIGNAL(sendLoansRequest()), this, SIGNAL(sendLoansRequest()));
    connect(userDialog, SIGNAL(sendAddBookToWishlistRequest(Book)), this, SIGNAL(sendAddBookToWishlistRequest(Book)));
    connect(userDialog, SIGNAL(sendRemoveBookFromWishlistRequest(Book)), this,
            SIGNAL(sendRemoveBookFromWishlistRequest(Book)));
    connect(userDialog, SIGNAL(sendReserveBookRequest(Book)), this, SIGNAL(sendReserveBookRequest(Book)));
    connect(userDialog, SIGNAL(sendReturnBookRequest(Book)), this,
            SIGNAL(sendReturnBookRequest(Book)));
    connect(userDialog, SIGNAL(sendDetailsAboutBookRequest(QString)), this,
            SIGNAL(sendDetailsAboutBookRequest(QString)));
    connect(userDialog, SIGNAL(sendEditUserRequest(User, bool, QString)), this,
            SIGNAL(sendEditUserRequest(User, bool, QString)));
    connect(userDialog, SIGNAL(sendCurrentUserRequest()), this,
            SIGNAL(sendCurrentUserRequest()));
    connect(userDialog, SIGNAL(sendImportWishlistFromFileRequest(QString)), this,
            SIGNAL(sendImportWishlistFromFileRequest(QString)));
    connect(userDialog, SIGNAL(sendExportWishlistFromFileRequest(QString)), this,
            SIGNAL(sendExportWishlistFromFileRequest(QString)));

    // forward results
    connect(this, SIGNAL(sendSearchBookRequestResult(std::vector<Book>)), userDialog,
            SIGNAL(sendSearchBookRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendWishlistRequestResult(std::vector<Book>)), userDialog,
            SIGNAL(sendWishlistRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendLoansRequestResult(std::vector<Book>)), userDialog,
            SIGNAL(sendLoansRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendAddBookToWishlistRequestResult(bool)), userDialog,
            SIGNAL(sendAddBookToWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveBookFromWishlistRequestResult(bool)), userDialog,
            SIGNAL(sendRemoveBookFromWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendReserveBookRequestResult(bool)), userDialog,
            SIGNAL(sendReserveBookRequestResult(bool)));
    connect(this, SIGNAL(sendReturnBookRequestResult(bool)), userDialog,
            SIGNAL(sendReturnBookRequestResult(bool)));
    connect(this, SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)), userDialog,
            SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)));
    connect(this, SIGNAL(sendEditUserRequestResult(bool)), userDialog,
            SIGNAL(sendEditUserRequestResult(bool)));
    connect(this, SIGNAL(sendUserRequestResult(User, bool)), userDialog,
            SIGNAL(sendUserRequestResult(User, bool)));
    connect(this, SIGNAL(sendImportWishlistFromFileRequestResult(bool)), userDialog,
            SIGNAL(sendImportWishlistFromFileRequestResult(bool)));
    connect(this, SIGNAL(sendExportWishlistFromFileRequestResult(bool)), userDialog,
            SIGNAL(sendExportWishlistFromFileRequestResult(bool)));

    if (isCurrentUserAnAdmin && dynamic_cast<AdminDialog *> (userDialog)) {
        // books
        // send requests
        connect(userDialog, SIGNAL(sendAddBookToLibraryRequest(Book)), this,
                SIGNAL(sendAddBookToLibraryRequest(Book)));
        connect(userDialog, SIGNAL(sendEditBookInLibraryRequest(QString, Book)), this,
                SIGNAL(sendEditBookInLibraryRequest(QString, Book)));
        connect(userDialog, SIGNAL(sendRemoveBookFromLibraryRequest(Book)), this,
                SIGNAL(sendRemoveBookFromLibraryRequest(Book)));

        // forward results
        connect(this, SIGNAL(sendAddBookToLibraryRequestResult(bool)), userDialog,
                SIGNAL(sendAddBookToLibraryRequestResult(bool)));
        connect(this, SIGNAL(sendEditBookInLibraryRequestResult(bool)), userDialog,
                SIGNAL(sendEditBookInLibraryRequestResult(bool)));
        connect(this, SIGNAL(sendRemoveBookFromLibraryRequestResult(bool)), userDialog,
                SIGNAL(sendRemoveBookFromLibraryRequestResult(bool)));

        // users
        // send requests
        connect(userDialog, SIGNAL(sendUserRequest(QString)), this,
                SIGNAL(sendUserRequest(QString)));
        connect(userDialog, SIGNAL(sendSearchUserRequest(QString, QString)), this,
                SIGNAL(sendSearchUserRequest(QString, QString)));
        connect(userDialog, SIGNAL(sendAddUserToLibraryRequest(User, bool)), this,
                SIGNAL(sendAddUserToLibraryRequest(User, bool)));
        connect(userDialog, SIGNAL(sendEditUserInLibraryRequest(QString, User, bool, QString)), this,
                SIGNAL(sendEditUserInLibraryRequest(QString, User, bool, QString)));
        connect(userDialog, SIGNAL(sendRemoveUserFromLibraryRequest(User)), this,
                SIGNAL(sendRemoveUserFromLibraryRequest(User)));

        // forward results
        connect(this, SIGNAL(sendSearchUserRequestResult(std::vector<User>)), userDialog,
                SIGNAL(sendSearchUserRequestResult(std::vector<User>)));
        connect(this, SIGNAL(sendAddUserToLibraryRequestResult(bool)), userDialog,
                SIGNAL(sendAddUserToLibraryRequestResult(bool)));
        connect(this, SIGNAL(sendEditUserInLibraryRequestResult(bool)), userDialog,
                SIGNAL(sendEditUserInLibraryRequestResult(bool)));
        connect(this, SIGNAL(sendRemoveUserFromLibraryRequestResult(bool)), userDialog,
                SIGNAL(sendRemoveUserFromLibraryRequestResult(bool)));
    }
}
