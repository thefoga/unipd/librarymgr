/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_ADMINUSERSLIBRARYMANAGER_H
#define LIBRARYMGR_ADMINUSERSLIBRARYMANAGER_H

#include "../../../gui/dialogs/UserAdderDialog/UserAdderDialog.h"
#include "../../../gui/dialogs/UserEditorDialog/UserEditorDialog.h"
#include "../AdminLibraryManager/AdminLibraryManager.h"
#include "../../../models/People/User/User.h"

/**
 * Lets admin manage users of the library
 */
class AdminUsersLibraryManager : public AdminLibraryManager {
Q_OBJECT

public:
    /**
     * New widget
     * @param parent widget to show widget in
     * @return new widget
     */
    AdminUsersLibraryManager(QWidget *parent = 0);

public slots:

    /**
     * Search library for user
     */
    void search();

    /**
     * Opens dialog to add new item in database
     */
    void addItem();

    /**
     * Opens dialog to edit selected item
     */
    void editItem();

    /**
     * Opens dialog to remove selected item from database
     */
    void removeItem();

    /**
     * Asks backend to change current user to this new one
     * @param user new user
     * @param isAdmin true iff user to display is an admin
     * @param password new password
     */
    void sendEditUserRequest(User user, bool isAdmin, QString password);

    // results

    /**
     * Gets backend response to send info about current user
     * @param user current user
     * @param isAdmin true iff user to display is an admin
     */
    void getUserRequestResult(User user, bool isAdmin);

    /**
     * Gets search results
     * @param users list of users matching query
     */
    void getSearchUserRequestResult(std::vector<User> users);

    /**
     * Sends request to backend to remove user from library
     * @param ok true iff operation went well
     */
    void getRemoveUserFromLibraryRequestResult(bool ok);

signals:
    // requests

    /**
     * Asks backend to send info about current user
     * @param username username of user to get info about
     */
    void sendUserRequest(QString username);

    /**
     * Sends request to backend to search database for given user
     * @param id email of user to search
     * @param name name of user to search
     */
    void sendSearchUserRequest(QString id, QString name);

    /**
     * Sends request to backend to add new user to library
     * @param user new user to add
     * @param makeAdmin true iff new user should be an admin
     */
    void sendAddUserToLibraryRequest(User user, bool makeAdmin);

    /**
     * Sends request to backend to edit user
     * @param oldId email of user to edit
     * @param newUser new user
     * @param isAdmin true iff user to display is an admin
     * @param newPassword password of new user
     */
    void sendEditUserInLibraryRequest(QString oldId, User newUser, bool isAdmin, QString newPassword);

    /**
     * Sends request to backend to remove user from library
     * @param user user to remove
     */
    void sendRemoveUserFromLibraryRequest(User user);

    // results to dialogs
    /**
     * Sends request to backend to add new user to library
     * @param ok true iff operation went well
     */
    void sendAddUserToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to edit user
     * @param ok true iff operation went well
     */
    void sendEditUserInLibraryRequestResult(bool ok);

private:
    UserAdderDialog *addUserDialog;  // dialog to add new user to library
    UserEditorDialog *editUserDialog;  // dialog to edit user

    /**
     * Setups gui
     */
    void setup();

    /**
     * Setups add/edit/remove buttons
     */
    void setupButtons();

    /**
     * Adds list of found users to search box
     * @param users list of users matching query
     */
    void populateSearchBox(std::vector<User> users);

    /**
     * Parses currently selected list item and builds user object
     * @return currently selected user
     */
    User *getSelectedUser();

    /**
     * Opens dialog to add new user to library
     */
    void openAddUserDialog();

    /**
     * Opens dialog to edit user
     */
    void openEditUserDialog();

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();
};


#endif //LIBRARYMGR_ADMINUSERSLIBRARYMANAGER_H
