/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtWidgets/QWidget>
#include <QtWidgets/QMessageBox>

#include "AdminUsersLibraryManager.h"


AdminUsersLibraryManager::AdminUsersLibraryManager(QWidget *parent) : AdminLibraryManager(parent) {
    addUserDialog = new UserAdderDialog();
    editUserDialog = new UserEditorDialog(true);  // can edit admin status

    setup();

    connectSignalsSlots();
}

void AdminUsersLibraryManager::setup() {
    QGridLayout *layout = new QGridLayout();

    setupSearchBox("Name:", "Username (email):");
    setupButtons();
    setupSearchResultBox();

    layout->addWidget(searchBoxArea, 0, 0, 1, 1);  // add widgets to layout
    layout->addWidget(buttonsArea, 1, 0, 1, 1);
    layout->addWidget(searchResultsArea, 0, 1, 2, 1);

    setLayout(layout);
}

void AdminUsersLibraryManager::populateSearchBox(std::vector<User> users) {
    searchResultsList->clear();

    for (unsigned int i = 0; i < users.size(); i++) {
        QString username = QString::fromUtf8(users[i].getEmail().c_str());
        QString name = QString::fromUtf8(users[i].getName().c_str());
        QString surname = QString::fromUtf8(users[i].getSurname().c_str());
        QString userSummary = name + " " + surname + " (" + username + ")";
        searchResultsList->addItem(userSummary);
    }
}

User *AdminUsersLibraryManager::getSelectedUser() {
    if (selectedItem) {
        QString username = selectedItem->text().split(" (")[1];
        username = username.split(")")[0];
        return new User(username.toStdString());
    }

    return 0;
}

void AdminUsersLibraryManager::openAddUserDialog() {
    addUserDialog->populate(User("User name", "User surname", QDateTime::currentDateTime().date(), "User email"));
    addUserDialog->show();
}

void AdminUsersLibraryManager::openEditUserDialog() {
    if (getSelectedUser()) {
        emit sendUserRequest(
                QString::fromUtf8(getSelectedUser()->getEmail().c_str()));  // ask backend info about user to show
        editUserDialog->show();
    }
}

void AdminUsersLibraryManager::search() {
    emit sendSearchUserRequest(findByIdInput->text(), findByNameInput->text());
}

void AdminUsersLibraryManager::addItem() {
    openAddUserDialog();
}

void AdminUsersLibraryManager::editItem() {
    openEditUserDialog();
}

void AdminUsersLibraryManager::removeItem() {
    if (getSelectedUser()) {
        QMessageBox::StandardButton reply = QMessageBox::question(
                this,
                "Are you sure?",
                "You are about to remove the user \"" +
                selectedItem->text() +
                "\" from the library.\nAre you sure?",
                QMessageBox::Yes | QMessageBox::No
        );

        if (reply == QMessageBox::Yes) {
            emit sendRemoveUserFromLibraryRequest(*getSelectedUser());
        }
    }
}

void AdminUsersLibraryManager::getSearchUserRequestResult(std::vector<User> users) {
    if (isVisible()) {
        if (users.size() == 0) {
            QMessageBox messageBox;
            messageBox.warning(0, "Search result",
                               "Sorry, no user matching your query has been found in the library!");
        } else {
            QMessageBox messageBox;
            messageBox.information(0, "Search result",
                                   QString::number(users.size()) + " users have been found in the library!");
        }

        populateSearchBox(users);
    }
}

void AdminUsersLibraryManager::getRemoveUserFromLibraryRequestResult(bool ok) {
    if (ok) {
        QMessageBox messageBox;
        messageBox.information(0, "Success",
                               "The user has been removed successfully from the library!");

    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error",
                           "The user could not be removed from the library!");
    }
}

void AdminUsersLibraryManager::connectSignalsSlots() {
    AdminLibraryManager::connectSignalsSlots();

    // send requests
    connect(addUserDialog, SIGNAL(sendAddUserToLibraryRequest(User, bool)), this,
            SIGNAL(sendAddUserToLibraryRequest(User, bool)));
    connect(editUserDialog, SIGNAL(sendEditUserRequest(User, bool, QString)), this,
            SLOT(sendEditUserRequest(User, bool, QString)));

    // forward results
    connect(this, SIGNAL(sendAddUserToLibraryRequestResult(bool)), addUserDialog,
            SLOT(getAddUserToLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendEditUserInLibraryRequestResult(bool)), editUserDialog,
            SLOT(getEditUserRequestResult(bool)));
}

void AdminUsersLibraryManager::setupButtons() {
    buttonsArea = new QGroupBox("Actions");  // setup layout
    QVBoxLayout *layout = new QVBoxLayout();

    addButton = new QPushButton("Add new");  // create widgets
    editButton = new QPushButton("Edit");
    removeButton = new QPushButton("Remove");

    layout->addWidget(addButton);  // place widgets
    layout->addWidget(editButton);
    layout->addWidget(removeButton);

    buttonsArea->setLayout(layout);
}

void AdminUsersLibraryManager::sendEditUserRequest(User user, bool isAdmin, QString password) {
    if (getSelectedUser()) {
        QString oldUserId = QString::fromUtf8(getSelectedUser()->getEmail().c_str());
        emit sendEditUserInLibraryRequest(oldUserId, user, isAdmin, password);
    }
}

void AdminUsersLibraryManager::getUserRequestResult(User user, bool isAdmin) {
    editUserDialog->populate(user, isAdmin);
}
