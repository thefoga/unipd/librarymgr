/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "AdminLibraryManager.h"

AdminLibraryManager::AdminLibraryManager(QWidget *parent) : QWidget(parent) {}

void AdminLibraryManager::setup() {
    QGridLayout *layout = new QGridLayout();

    setupSearchBox();
    setupButtons();
    setupSearchResultBox();

    layout->addWidget(searchBoxArea, 0, 0, 1, 1);  // add widgets to layout
    layout->addWidget(buttonsArea, 1, 0, 1, 1);
    layout->addWidget(searchResultsArea, 0, 1, 2, 1);

    setLayout(layout);
}

void AdminLibraryManager::setupSearchBox(QString nameLabel, QString idLabel) {
    searchBoxArea = new QGroupBox("Search");  // setup layout
    QGridLayout *layout = new QGridLayout();

    findByNameInput = new QLineEdit();  // create widgets
    findByIdInput = new QLineEdit();
    searchButton = new QPushButton("Find");

    layout->addWidget(new QLabel(nameLabel), 0, 0, 1, 1);  // place widgets
    layout->addWidget(findByIdInput, 0, 1, 1, 1);
    layout->addWidget(new QLabel(idLabel), 1, 0, 1, 1);
    layout->addWidget(findByNameInput, 1, 1, 1, 1);
    layout->addWidget(searchButton, 2, 0, 1, 2);

    searchBoxArea->setLayout(layout);
}

void AdminLibraryManager::setupButtons() {
    buttonsArea = new QGroupBox("Actions");  // setup layout
    QVBoxLayout *layout = new QVBoxLayout();

    addButton = new QPushButton("Add new");  // create widgets
    editButton = new QPushButton("Edit");
    removeButton = new QPushButton("Remove");

    layout->addWidget(addButton);  // place widgets
    // layout->addWidget(editButton);
    layout->addWidget(removeButton);

    buttonsArea->setLayout(layout);
}

void AdminLibraryManager::setupSearchResultBox() {
    searchResultsArea = new QGroupBox("Search results");  // setup layout
    QGridLayout *layout = new QGridLayout();

    searchResultsList = new QListWidget();  // create widget
    layout->addWidget(searchResultsList, 0, 0);  // place widget

    searchResultsArea->setLayout(layout);
}

void AdminLibraryManager::setCurrentSelectedItem(QListWidgetItem *item) {
    selectedItem = item;
}

void AdminLibraryManager::connectSignalsSlots() {
    connect(searchButton, SIGNAL(clicked()), this, SLOT(search()));
    connect(addButton, SIGNAL(clicked()), this, SLOT(addItem()));
    connect(editButton, SIGNAL(clicked()), this, SLOT(editItem()));
    connect(removeButton, SIGNAL(clicked()), this, SLOT(removeItem()));
    connect(searchResultsList, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(setCurrentSelectedItem(QListWidgetItem * )));
}

void AdminLibraryManager::search() {}

void AdminLibraryManager::addItem() {}

void AdminLibraryManager::editItem() {}

void AdminLibraryManager::removeItem() {}
