/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_ADMINLIBRARYMANAGER_H
#define LIBRARYMGR_ADMINLIBRARYMANAGER_H


#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTableWidget>

/**
 * Lets admin manage library stuff (e.g books, users ..)
 * Features:
 * - add new item
 * - remove item
 * - edit item box
 * - list box with search results
 */
class AdminLibraryManager : public QWidget {
Q_OBJECT

public:
    /**
     * New widget
     * @param parent widget to show widget in
     * @return new widget
     */
    AdminLibraryManager(QWidget *parent = 0);

public slots:

    /**
     * Search library for book
     */
    virtual void search();

    /**
     * Opens dialog to add new item in database
     */
    virtual void addItem();

    /**
     * Opens dialog to edit selected item
     */
    virtual void editItem();

    /**
     * Opens dialog to remove selected item from database
     */
    virtual void removeItem();

    /**
     * Sets current selected item
     * @param item item that is currently selected by user
     */
    void setCurrentSelectedItem(QListWidgetItem *item);

protected:
    QListWidgetItem *selectedItem;

    // search box
    QGroupBox *searchBoxArea;  // layout
    QLineEdit *findByNameInput;
    QLineEdit *findByIdInput;
    QPushButton *searchButton;

    // add/edit/remove buttons
    QGroupBox *buttonsArea;
    QPushButton *addButton;
    QPushButton *editButton;
    QPushButton *removeButton;

    // list with found items
    QGroupBox *searchResultsArea;
    QListWidget *searchResultsList;

    /**
     * Setups gui
     */
    virtual void setup();

    /**
     * Connect signal and slots of object managed by backend
     */
    virtual void connectSignalsSlots();

    /**
     * Setups search box
     * @param nameLabel label text of name field
     * @param idLabel label text if id field
     */
    void setupSearchBox(QString nameLabel = "Name:", QString idLabel = "Id:");

    /**
     * Setups add/edit/remove buttons
     */
    void setupButtons();

    /**
     * Setups box with search results
     */
    void setupSearchResultBox();
};


#endif //LIBRARYMGR_ADMINLIBRARYMANAGER_H
