/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>

#include "InlineLabel.h"

InlineLabel::InlineLabel(bool isReadOnly, QString title, QString text, QWidget *parent)
        : QWidget(parent) {
    input = new QLineEdit(text);  // setup label
    setReadOnly(isReadOnly);

    setup(title);
}

QString InlineLabel::getInput() {
    return input->text();
}

void InlineLabel::setup(QString title) {
    QHBoxLayout *layout = new QHBoxLayout(this);

    layout->addWidget(new QLabel(title));
    layout->addWidget(input);

    setLayout(layout);
}

void InlineLabel::setText(QString text) {
    input->setText(text);
}

void InlineLabel::setReadOnly(bool isReadOnly) {
    input->setReadOnly(isReadOnly);
}
