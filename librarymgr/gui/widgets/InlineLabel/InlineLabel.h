/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_INLINELABEL_H
#define LIBRARYMGR_INLINELABEL_H


#include <QtWidgets/QWidget>
#include <QtWidgets/QLineEdit>

/**
 * GUI widget with 2 labels: one of them can be an input
 */
class InlineLabel : public QWidget {

Q_OBJECT

public:
    /**
     * New widget in parent and edit mode
     * @param parent parent widget to show in
     * @param title title of label
     * @param text text of input label
     * @param isReadOnly whether input is editable or read only
     * @return new widget
     */
    InlineLabel(bool isReadOnly, QString title, QString text, QWidget *parent = 0);

    /**
     * Sets labels status
     * @param isReadOnly true iff labels should be read only
     */
    void setReadOnly(bool isReadOnly);

    /**
     * Sets text on input
     * @param text input text
     */
    void setText(QString text);

public slots:

    /**
     * Gets input of label
     * @return input of label
     */
    QString getInput();

private:
    QLineEdit *input;

    /**
     * Setups gui
     * @param title title of label
     */
    void setup(QString title);
};


#endif //LIBRARYMGR_INLINELABEL_H
