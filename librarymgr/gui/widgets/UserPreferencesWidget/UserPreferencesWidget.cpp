/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QWidget>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>

#include "UserPreferencesWidget.h"

UserPreferencesWidget::UserPreferencesWidget(QWidget *parent) : QWidget(parent) {
    userEditorDialog = new UserEditorDialog();
    importWishlistFromFileButton = new QPushButton("Import wishlist from file");
    exportWishlistFromFileButton = new QPushButton("Export wishlist to file");
    openUserEditorDialogButton = new QPushButton("Edit my details");

    setup();
    connectSignalsSlots();
    emit sendCurrentUserRequest();  // ask backend info about current user
}

void UserPreferencesWidget::setup() {
    QVBoxLayout *layout = new QVBoxLayout();  // layout
    layout->addWidget(importWishlistFromFileButton);  // add widgets
    layout->addWidget(exportWishlistFromFileButton);
    layout->addWidget(openUserEditorDialogButton);
    setLayout(layout);
}

void UserPreferencesWidget::connectSignalsSlots() {
    connect(importWishlistFromFileButton, SIGNAL(clicked()), this, SLOT(importWishlistFromFile()));
    connect(exportWishlistFromFileButton, SIGNAL(clicked()), this, SLOT(exportWishlistFromFile()));
    connect(openUserEditorDialogButton, SIGNAL(clicked()), this, SLOT(openUserEditorDialog()));

    // requests
    connect(userEditorDialog, SIGNAL(sendEditUserRequest(User, bool, QString)), this,
            SIGNAL(sendEditUserRequest(User, bool, QString)));

    // results
    connect(this, SIGNAL(sendEditUserRequestResult(bool)), userEditorDialog, SLOT(getEditUserRequestResult(bool)));
}

void UserPreferencesWidget::getUserRequestResult(User user, bool isAdmin) {
    userEditorDialog->populate(user, isAdmin);
}

void UserPreferencesWidget::openUserEditorDialog() {
    emit sendCurrentUserRequest();  // ask backend info about current user
    userEditorDialog->show();
}

void UserPreferencesWidget::getImportWishlistFromFileRequestResult(bool ok) {
    if (ok) {
        QMessageBox messageBox;
        messageBox.information(0, "Success",
                               "Wishlist imported successfully!");
    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error",
                           "Error parsing file!");
    }
}

void UserPreferencesWidget::getExportWishlistFromFileRequestResult(bool ok) {
    if (ok) {
        QMessageBox messageBox;
        messageBox.information(0, "Success",
                               "Wishlist exported successfully!");
    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error",
                           "Error parsing file!");
    }
}

QString UserPreferencesWidget::chooseFileDialog(QString title) {
    return QFileDialog::getOpenFileName(this, title, ".json", "*.json");
}

void UserPreferencesWidget::importWishlistFromFile() {
    QString fileChosen = chooseFileDialog("Choose import file");
    emit sendImportWishlistFromFileRequest(fileChosen);
}

void UserPreferencesWidget::exportWishlistFromFile() {
    QString fileChosen = chooseFileDialog("Choose export file");
    emit sendExportWishlistFromFileRequest(fileChosen);
}
