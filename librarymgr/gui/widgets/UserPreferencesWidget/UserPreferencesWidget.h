/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USERPREFERENCESWIDGET_H
#define LIBRARYMGR_USERPREFERENCESWIDGET_H


#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTableWidget>

#include "../../dialogs/UserEditorDialog/UserEditorDialog.h"
#include "../../../models/People/User/User.h"

/**
 * GUI widget to manage user preferences
 */
class UserPreferencesWidget : public QWidget {
Q_OBJECT

public:
    /**
     * New widget
     * @param parent parent widget to show in
     * @return widget
     */
    UserPreferencesWidget(QWidget *parent = 0);

public slots:

    /**
     * Backend response on user details
     * @param user current user of app
     * @param isAdmin true iff user to display is an admin
     */
    void getUserRequestResult(User user, bool isAdmin);

    /**
     * Shows user editor dialog
     */
    void openUserEditorDialog();

    /**
     * Sends backend request to import books to user wishlist from given file
     */
    void importWishlistFromFile();

    /**
     * Sends backend request to export books to user wishlist from given file
     */
    void exportWishlistFromFile();

    /**
     * Sends backend request to import books to user wishlist from given file
     * @param ok true iff operation went well
     */
    void getImportWishlistFromFileRequestResult(bool ok);

    /**
     * Sends backend request to export books to user wishlist from given file
     * @param ok true iff operation went well
     */
    void getExportWishlistFromFileRequestResult(bool ok);

signals:
    // send requests

    /**
     * Asks backend info about current user
     */
    void sendCurrentUserRequest();

    /**
     * Asks backend to change current user to this new one
     * @param user new user
     * @param isAdmin true iff user to display is an admin
     * @param password new password
     */
    void sendEditUserRequest(User user, bool isAdmin, QString password);

    /**
     * Sends backend request to import books to user wishlist from given file
     * @param pathToFileToImport file to import books to user wishlist
     */
    void sendImportWishlistFromFileRequest(QString pathToFileToImport);

    /**
     * Sends backend request to export books to user wishlist from given file
     * @param pathToFileToExport file to export books to user wishlist
     */
    void sendExportWishlistFromFileRequest(QString pathToFileToExport);

    // forward results

    /**
     * Gets confirmation of operation success
     * @param ok true iff operation went well
     */
    void sendEditUserRequestResult(bool ok);

private:
    UserEditorDialog *userEditorDialog;  // dialog to edit user fields
    QPushButton *importWishlistFromFileButton;  // buttons to import/export user wishlist
    QPushButton *exportWishlistFromFileButton;
    QPushButton *openUserEditorDialogButton;  // button to open user editor

    /**
     * Setups gui
     */
    void setup();

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();

    /**
     * Opens new file chooser dialog with title and gets file chosen
     * @param title title of dialog
     * @return file chosen
     */
    QString chooseFileDialog(QString title);
};


#endif //LIBRARYMGR_USERPREFERENCESWIDGET_H
