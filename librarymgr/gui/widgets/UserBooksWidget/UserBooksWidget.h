/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USERBOOKSWIDGET_H
#define LIBRARYMGR_USERBOOKSWIDGET_H


#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTableWidget>

#include "../../../gui/dialogs/BookManagerDialog/BookManagerDialog.h"
#include "../../../models/Article/Book/Book.h"

/**
 * GUI widget to manage user books
 * Features
 * - search box with search result area
 * - whishlist books area
 * - table with past loans
 */
class UserBooksWidget : public QWidget {
Q_OBJECT

public:
    /**
     * New widget
     * @param parent parent widget to show in
     * @return widget
     */
    UserBooksWidget(QWidget *parent = 0);

public slots:

    /**
     * Shows dialog with details about selected book
     * @param i item in list double clicked
     */
    void openBookDetailsDialog(QListWidgetItem *i);

    /**
     * Search book with currently input isbn and title
     */
    void searchBook();

    /**
     * Gets search results
     * @param books list of books matching query
     */
    void getSearchBookRequestResult(std::vector<Book> books);

    /**
     * Gets user wishlist
     * @param book user books wishlist
     */
    void getWishlistRequestResult(std::vector<Book> books);

    /**
     * Gets user loans
     * @param book user books loans
     */
    void getLoansRequestResult(std::vector<Book> books);

    /**
     * Checks if adding requested book to user wishlist operation went well
     * @param ok true iff operation went well
     */
    void getAddBookToWishlistRequestResult(bool ok);

    /**
     * Checks if removing requested book from user wishlist operation went well
     * @param ok true iff operation went well
     */
    void getRemoveBookFromWishlistRequestResult(bool ok);

    /**
     * Checks if request to backend to reserve book went well
     * @param ok true iff operation went well
     */
    void getReserveBookRequestResult(bool ok);

    /**
     * Checks if request to backend to return book went well
     * @param ok true iff operation went well
     */
    void getReturnBookRequestResult(bool ok);

signals:
    // requests

    /**
     * Asks backend for details about book with isbn
     * @param isbn isbn of book to get details about
     */
    void sendDetailsAboutBookRequest(QString isbn);

    /**
     * Sends request to backend to search database for given book
     * @param id isbn code to search book
     * @param title title of book to search
     */
    void sendSearchBookRequest(QString id, QString title);

    /**
     * Sends request to backend to get wishlist of current user
     */
    void sendWishlistRequest();

    /**
     * Sends request to backend to get books loans of current user
     */
    void sendLoansRequest();

    /**
     * Sends request to backend to add new book to user wishlist
     * @param book
     */
    void sendAddBookToWishlistRequest(Book book);

    /**
     * Sends request to backend to remove book from user wishlist
     * @param book
     */
    void sendRemoveBookFromWishlistRequest(Book book);

    /**
     * Sends request to backend to reserve book
     * @param book book to reserve
     */
    void sendReserveBookRequest(Book book);

    /**
     * Sends request to backend to return book
     * @param book book to return
     */
    void sendReturnBookRequest(Book book);

    // results

    /**
     * Gets backend response on book details
     * @param book full book details
     * @param isReservedByUser true iff current user has reserved this book
     * @param isInUserWishlist true iff book is in current user wishlist
     */
    void sendDetailsAboutBookRequestResult(Book book, bool isReservedByUser, bool isInUserWishlist);

    /**
     * Sends request to backend to reserve book
     * @param ok true iff operation went well
     */
    void sendReserveBookRequestResult(bool ok);

    /**
     * Sends request to backend to return book
     * @param ok true iff operation went well
     */
    void sendReturnBookRequestResult(bool ok);

    /**
     * Checks if adding requested book to user wishlist operation went well
     * @param ok true iff operation went well
     */
    void sendAddBookToWishlistRequestResult(bool ok);

    /**
     * Checks if removing requested book from user wishlist operation went well
     * @param ok true iff operation went well
     */
    void sendRemoveBookFromWishlistRequestResult(bool ok);

private:
    // search box
    QGroupBox *searchBoxArea;  // layout
    QLineEdit *findByIdInput;
    QLineEdit *findByNameInput;
    QPushButton *searchButton;
    QListWidget *searchListWidget;  // list with found items

    // wishList box
    QGroupBox *wishListBoxArea;  // layout
    QListWidget *wishListWidget;  // list with favorite books

    // loans table
    QGroupBox *loansBoxArea;  // layout
    QListWidget *loansTable;  // table that contains previous books loans

    BookManagerDialog *bookDetailsDialog;

    /**
     * Setups gui
     */
    void setup();

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();

    /**
     * Setups search box
     */
    void setupSearchBox();

    /**
     * Setups whishlist box
     */
    void setupWishListBox();

    /**
     * Setups past books loans table
     */
    void setupLoansTable();

    /**
     * Adds list of found books to search box
     * @param books list of books matching query
     */
    void populateSearchBox(std::vector<Book> books);

    /**
     * Adds books wishlist to wishlist box
     * @param books wishlist of user
     */
    void populateWishlistBox(std::vector<Book> books);

    /**
     * Adds loans books to loans table
     * @param books loans of user
     */
    void populateLoansBox(std::vector<Book> books);
};


#endif //LIBRARYMGR_USERBOOKSWIDGET_H
