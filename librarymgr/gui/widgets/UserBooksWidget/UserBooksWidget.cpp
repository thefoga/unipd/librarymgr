/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMessageBox>
#include <iostream>

#include "UserBooksWidget.h"

UserBooksWidget::UserBooksWidget(QWidget *parent) : QWidget(parent) {
    setup();
    bookDetailsDialog = new BookManagerDialog();  // create widget to show book details

    connectSignalsSlots();
}

void UserBooksWidget::setup() {
    QGridLayout *layout = new QGridLayout();

    setupSearchBox();
    setupWishListBox();
    setupLoansTable();

    layout->addWidget(searchBoxArea, 0, 0, 1, 1);  // add widgets to layout
    layout->addWidget(wishListBoxArea, 0, 1, 1, 1);
    layout->addWidget(loansBoxArea, 1, 0, 1, 2);

    setLayout(layout);
}

void UserBooksWidget::setupSearchBox() {
    searchBoxArea = new QGroupBox("Search library");  // setup layout
    QGridLayout *searchLayout = new QGridLayout();

    findByIdInput = new QLineEdit();  // create wodgets
    findByNameInput = new QLineEdit();
    searchButton = new QPushButton("Find");
    searchListWidget = new QListWidget();  // list with found books

    searchLayout->addWidget(new QLabel("Title:"), 1, 0, 1, 1);  // place widgets
    searchLayout->addWidget(findByNameInput, 1, 1, 1, 1);
    searchLayout->addWidget(new QLabel("Isbn:"), 0, 0, 1, 1);
    searchLayout->addWidget(findByIdInput, 0, 1, 1, 1);
    searchLayout->addWidget(searchButton, 2, 0, 1, 2);
    searchLayout->addWidget(searchListWidget, 0, 3, 2, 1);

    searchBoxArea->setLayout(searchLayout);
}

void UserBooksWidget::setupWishListBox() {
    wishListBoxArea = new QGroupBox("Wish list");
    QGridLayout *wishListLayout = new QGridLayout();

    wishListWidget = new QListWidget();  // create wish list widget

    wishListLayout->addWidget(wishListWidget, 0, 0);  // add wish list widget

    wishListBoxArea->setLayout(wishListLayout);
}

void UserBooksWidget::setupLoansTable() {
    loansBoxArea = new QGroupBox("Current books loaned to you");
    QGridLayout *loansBoxLayout = new QGridLayout();

    loansTable = new QListWidget();
    loansBoxLayout->addWidget(loansTable, 0, 0);

    loansBoxArea->setLayout(loansBoxLayout);
}

void UserBooksWidget::getSearchBookRequestResult(std::vector<Book> books) {
    if (isVisible()) {
        if (books.size() == 0) {
            QMessageBox messageBox;
            messageBox.warning(0, "Search result",
                               "Sorry, no book matching your query has been found in the library!");
        } else {
            QMessageBox messageBox;
            messageBox.information(0, "Search result",
                                   QString::number(books.size()) + " books have been found in the library!");
        }

        populateSearchBox(books);
    }
}

void UserBooksWidget::getWishlistRequestResult(std::vector<Book> books) {
    populateWishlistBox(books);
}

void UserBooksWidget::getAddBookToWishlistRequestResult(bool ok) {
    if (ok) {
        emit sendWishlistRequest();  // request update wishlist
    }

    emit sendAddBookToWishlistRequestResult(ok);
}

void UserBooksWidget::getRemoveBookFromWishlistRequestResult(bool ok) {
    if (ok) {
        emit sendWishlistRequest();  // request update wishlist
    }

    emit sendRemoveBookFromWishlistRequestResult(ok);
}

void UserBooksWidget::populateSearchBox(std::vector<Book> books) {
    searchListWidget->clear();

    for (unsigned int i = 0; i < books.size(); i++) {
        QString bookIsbn = QString::fromUtf8(books[i].getID().c_str());
        QString bookTitle = QString::fromUtf8(books[i].getName().c_str());
        QString bookSummary = bookIsbn + " | " + bookTitle;
        searchListWidget->addItem(bookSummary);
    }
}

void UserBooksWidget::populateWishlistBox(std::vector<Book> books) {
    wishListWidget->clear();

    for (unsigned int i = 0; i < books.size(); i++) {
        QString bookIsbn = QString::fromUtf8(books[i].getID().c_str());
        QString bookTitle = QString::fromUtf8(books[i].getName().c_str());
        QString bookSummary = bookIsbn + " | " + bookTitle;
        wishListWidget->addItem(bookSummary);
    }
}

void UserBooksWidget::searchBook() {
    emit sendSearchBookRequest(findByIdInput->text(), findByNameInput->text());
}

void UserBooksWidget::openBookDetailsDialog(QListWidgetItem *i) {
    QString isbn = i->text().split(" | ")[0];
    bookDetailsDialog->setBookId(isbn);  // populate dialog with this book
    bookDetailsDialog->show();
}

void UserBooksWidget::connectSignalsSlots() {
    // search
    connect(searchButton, SIGNAL(clicked()), this, SLOT(searchBook()));
    connect(searchListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem * )), this,
            SLOT(openBookDetailsDialog(QListWidgetItem * )));
    connect(bookDetailsDialog, SIGNAL(sendDetailsAboutBookRequest(QString)), this,
            SIGNAL(sendDetailsAboutBookRequest(QString)));
    connect(this, SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)), bookDetailsDialog,
            SLOT(getDetailsAboutBookRequest(Book, bool, bool)));

    // wishlist
    connect(wishListWidget, SIGNAL(itemDoubleClicked(QListWidgetItem * )), this,
            SLOT(openBookDetailsDialog(QListWidgetItem * )));
    connect(bookDetailsDialog, SIGNAL(sendAddBookToWishlistRequest(Book)), this,
            SIGNAL(sendAddBookToWishlistRequest(Book)));
    connect(bookDetailsDialog, SIGNAL(sendRemoveBookFromWishlistRequest(Book)), this,
            SIGNAL(sendRemoveBookFromWishlistRequest(Book)));
    connect(this, SIGNAL(sendAddBookToWishlistRequestResult(bool)), bookDetailsDialog,
            SLOT(getAddBookToWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveBookFromWishlistRequestResult(bool)), bookDetailsDialog,
            SLOT(getRemoveBookFromWishlistRequestResult(bool)));

    // loans table
    connect(loansTable, SIGNAL(itemDoubleClicked(QListWidgetItem * )), this,
            SLOT(openBookDetailsDialog(QListWidgetItem * )));

    // return/reserve
    connect(bookDetailsDialog, SIGNAL(sendReserveBookRequest(Book)), this, SIGNAL(sendReserveBookRequest(Book)));
    connect(bookDetailsDialog, SIGNAL(sendReturnBookRequest(Book)), this, SIGNAL(sendReturnBookRequest(Book)));
    connect(this, SIGNAL(sendReserveBookRequestResult(bool)), bookDetailsDialog,
            SLOT(getReserveBookRequestResult(bool)));
    connect(this, SIGNAL(sendReturnBookRequestResult(bool)), bookDetailsDialog,
            SLOT(getReturnBookRequestResult(bool)));
}

void UserBooksWidget::getLoansRequestResult(std::vector<Book> books) {
    populateLoansBox(books);
}

void UserBooksWidget::populateLoansBox(std::vector<Book> books) {
    loansTable->clear();

    for (unsigned int i = 0; i < books.size(); i++) {
        QString bookIsbn = QString::fromUtf8(books[i].getID().c_str());
        QString bookTitle = QString::fromUtf8(books[i].getName().c_str());
        QString bookSummary = bookIsbn + " | " + bookTitle;
        loansTable->addItem(bookSummary);
    }
}

void UserBooksWidget::getReserveBookRequestResult(bool ok) {
    if (ok) {
        emit sendLoansRequest();  // update loans table
    }

    emit sendReserveBookRequestResult(ok);
}

void UserBooksWidget::getReturnBookRequestResult(bool ok) {
    if (ok) {
        emit sendLoansRequest();  // update loans table
    }

    emit sendReturnBookRequestResult(ok);
}
