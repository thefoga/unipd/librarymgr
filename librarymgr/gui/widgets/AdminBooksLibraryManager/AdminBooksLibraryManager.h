/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_ADMINBOOKSLIBRARYMANAGER_H
#define LIBRARYMGR_ADMINBOOKSLIBRARYMANAGER_H


#include "../../../gui/dialogs/BookAdderDialog/BookAdderDialog.h"
#include "../AdminLibraryManager/AdminLibraryManager.h"
#include "../../../models/Article/Book/Book.h"

/**
 * Lets admin manage books of the library
 */
class AdminBooksLibraryManager : public AdminLibraryManager {
Q_OBJECT

public:
    /**
     * New widget
     * @param parent widget to show widget in
     * @return new widget
     */
    AdminBooksLibraryManager(QWidget *parent = 0);

public slots:

    /**
     * Search library for book
     */
    void search();

    /**
     * Opens dialog to add new item in database
     */
    void addItem();

    /**
     * Opens dialog to edit selected item
     */
    void editItem();

    /**
     * Opens dialog to remove selected item from database
     */
    void removeItem();

    // results

    /**
     * Gets search results
     * @param books list of books matching query
     */
    void getSearchBookRequestResult(std::vector<Book> books);

    /**
     * Sends request to backend to remove book from library
     * @param ok true iff operation went well
     */
    void getRemoveBookFromLibraryRequestResult(bool ok);

signals:
    // requests

    /**
     * Sends request to backend to search database for given book
     * @param id isbn code to search book
     * @param title title of book to search
     */
    void sendSearchBookRequest(QString id, QString title);

    /**
     * Sends request to backend to add new book to library
     * @param book book to add
     */
    void sendAddBookToLibraryRequest(Book book);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param oldBookIsbn isbn of book to edit
     * @param newBook new book
     */
    void sendEditBookInLibraryRequest(QString oldBookIsbn, Book newBook);

    /**
     * Sends request to backend to remove book from library
     * @param book book to remove
     */
    void sendRemoveBookFromLibraryRequest(Book book);

    // results to dialogs

    /**
     * Sends request to backend to add new book to library
     * @param ok true iff operation went well
     */
    void sendAddBookToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param ok true iff operation went well
     */
    void sendEditBookInLibraryRequestResult(bool ok);

private:
    BookAdderDialog *addBookDialog;  // dialog to add new book to the library

    /**
     * Setups gui
     */
    void setup();

    /**
     * Adds list of found books to search box
     * @param books list of books matching query
     */
    void populateSearchBox(std::vector<Book> books);

    /**
     * Parses currently selected list item and builds book object
     * @return currently selected book
     */
    Book *getSelectedBook();

    /**
     * Opens dialog to add new book to library
     */
    void openAddBookDialog();

    /**
     * Opens dialog to edit book
     */
    void openEditBookDialog();

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();
};


#endif //LIBRARYMGR_ADMINBOOKSLIBRARYMANAGER_H
