/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtWidgets/QMessageBox>
#include <iostream>

#include "AdminBooksLibraryManager.h"

AdminBooksLibraryManager::AdminBooksLibraryManager(QWidget *parent) : AdminLibraryManager(parent) {
    addBookDialog = new BookAdderDialog();
    setup();

    connectSignalsSlots();
}

void AdminBooksLibraryManager::setup() {
    QGridLayout *layout = new QGridLayout();

    setupSearchBox("Title:", "Isbn:");
    setupButtons();
    setupSearchResultBox();

    layout->addWidget(searchBoxArea, 0, 0, 1, 1);  // add widgets to layout
    layout->addWidget(buttonsArea, 1, 0, 1, 1);
    layout->addWidget(searchResultsArea, 0, 1, 2, 1);

    setLayout(layout);
}

void AdminBooksLibraryManager::search() {
    emit sendSearchBookRequest(findByIdInput->text(), findByNameInput->text());
}

void AdminBooksLibraryManager::addItem() {
    openAddBookDialog();
}

void AdminBooksLibraryManager::editItem() {
    openEditBookDialog();
}

void AdminBooksLibraryManager::removeItem() {
    if (getSelectedBook()) {
        QMessageBox::StandardButton reply = QMessageBox::question(
                this,
                "Are you sure?",
                "You are about to remove the book \"" +
                selectedItem->text() +
                "\" from the library.\nAre you sure?",
                QMessageBox::Yes | QMessageBox::No
        );

        if (reply == QMessageBox::Yes) {
            emit sendRemoveBookFromLibraryRequest(*getSelectedBook());
        }
    }
}

Book *AdminBooksLibraryManager::getSelectedBook() {
    if (selectedItem) {
        QString isbnBook = selectedItem->text().split(" | ")[0];
        return new Book(isbnBook.toStdString());
    }

    return 0;
}

void AdminBooksLibraryManager::getSearchBookRequestResult(std::vector<Book> books) {
    if (isVisible()) {
        if (books.size() == 0) {
            QMessageBox messageBox;
            messageBox.warning(0, "Search result",
                               "Sorry, no book matching your query has been found in the library!");
        } else {
            QMessageBox messageBox;
            messageBox.information(0, "Search result",
                                   QString::number(books.size()) + " books have been found in the library!");
        }

        populateSearchBox(books);
    }
}

void AdminBooksLibraryManager::getRemoveBookFromLibraryRequestResult(bool ok) {
    if (ok) {
        QMessageBox messageBox;
        messageBox.information(0, "Success",
                               "The book has been removed successfully from the library!");

    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error",
                           "The book could not be removed from the library: there is someone reading it!");
    }
}

void AdminBooksLibraryManager::populateSearchBox(std::vector<Book> books) {
    searchResultsList->clear();

    for (unsigned int i = 0; i < books.size(); i++) {
        QString bookIsbn = QString::fromUtf8(books[i].getID().c_str());
        QString bookTitle = QString::fromUtf8(books[i].getName().c_str());
        QString bookSummary = bookIsbn + " | " + bookTitle;
        searchResultsList->addItem(bookSummary);
    }
}

void AdminBooksLibraryManager::openAddBookDialog() {
    addBookDialog->setBookId("");  // editable book fields
    addBookDialog->show();
}

void AdminBooksLibraryManager::openEditBookDialog() {}

void AdminBooksLibraryManager::connectSignalsSlots() {
    AdminLibraryManager::connectSignalsSlots();

    // send requests
    connect(addBookDialog, SIGNAL(sendAddBookToLibraryRequest(Book)), this, SIGNAL(sendAddBookToLibraryRequest(Book)));

    // forward results
    connect(this, SIGNAL(sendAddBookToLibraryRequestResult(bool)), addBookDialog,
            SLOT(getAddBookToLibraryRequest(bool)));
}
