/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USERDETAILSDIALOG_H
#define LIBRARYMGR_USERDETAILSDIALOG_H

#include <QtWidgets/QWidget>
#include <QDialog>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QDateEdit>

#include "../../widgets/InlineLabel/InlineLabel.h"
#include "../../../models/People/User/User.h"

/**
 * GUI dialog to show user details
 */
class UserDetailsDialog : public QDialog {
Q_OBJECT

public:
    /**
     * New dialog
     * @param parent parent widget to show dialog in
     * @param isReadOnly whether user can edit fields
     * @return new dialog
     */
    UserDetailsDialog(bool isReadOnly = true, QWidget *parent = 0);

    /**
     * Populate dialog with book features
     * @param user user to show in dialog
     */
    void populate(User user);

    /**
     * Builds user with fields set in dialog
     * @return user
     */
    User getUser();

    /**
     * Setups gui
     */
    virtual void setup();

protected:
    InlineLabel *emailLabel;  // widgets
    InlineLabel *nameLabel;
    InlineLabel *surnameLabel;
    QDateEdit *birthDate;
};


#endif //LIBRARYMGR_USERDETAILSDIALOG_H
