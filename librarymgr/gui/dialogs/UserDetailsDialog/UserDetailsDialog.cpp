/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>

#include "../../../models/People/User/User.h"
#include "UserDetailsDialog.h"

UserDetailsDialog::UserDetailsDialog(bool isReadOnly, QWidget *parent) : QDialog(parent) {
    nameLabel = new InlineLabel(isReadOnly, "Name:", "User name", this);
    surnameLabel = new InlineLabel(isReadOnly, "Surname:", "User surname", this);
    birthDate = new QDateEdit(QDateTime::currentDateTime().date(), this);
    birthDate->setEnabled(!isReadOnly);
    emailLabel = new InlineLabel(isReadOnly, "Email:", "User email", this);
}

void UserDetailsDialog::populate(User user) {
    nameLabel->setText(user.getName().c_str());
    surnameLabel->setText(user.getSurname().c_str());
    birthDate->setDate(user.getDateOfBirth());
    emailLabel->setText(user.getEmail().c_str());
}

User UserDetailsDialog::getUser() {
    return User(
            nameLabel->getInput().toStdString(),
            surnameLabel->getInput().toStdString(),
            birthDate->date(),
            emailLabel->getInput().toStdString()
    );
}

void UserDetailsDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addWidget(nameLabel);
    layout->addWidget(surnameLabel);

    QWidget *birthDateLabel = new QWidget();
    QHBoxLayout *birthDateLayout = new QHBoxLayout();
    birthDateLayout->addWidget(new QLabel("Birth date:"));
    birthDateLayout->addWidget(birthDate);
    birthDateLabel->setLayout(birthDateLayout);
    layout->addWidget(birthDateLabel);

    layout->addWidget(emailLabel);

    setLayout(layout);
}
