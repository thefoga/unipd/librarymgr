/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_ADMINDIALOG_H
#define LIBRARYMGR_ADMINDIALOG_H

#include "../../widgets/AdminBooksLibraryManager/AdminBooksLibraryManager.h"
#include "../../widgets/AdminUsersLibraryManager/AdminUsersLibraryManager.h"
#include "../UserDialog/UserDialog.h"

/**
 * Dialog to manage admin stuff: edit library (users and books), books whishlit and preferences
 */
class AdminDialog : public UserDialog {
Q_OBJECT

public:
    explicit AdminDialog(QString title = "Admin", QDialog *parent = 0);

signals:

    // books managements
    // requests

    /**
     * Sends request to backend to add new book to library
     * @param book book to add
     */
    void sendAddBookToLibraryRequest(Book book);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param oldBookIsbn isbn of book to edit
     * @param newBook new book
     */
    void sendEditBookInLibraryRequest(QString oldBookIsbn, Book newBook);

    /**
     * Sends request to backend to remove book from library
     * @param book book to remove
     */
    void sendRemoveBookFromLibraryRequest(Book book);

    // results

    /**
     * Sends request to backend to add new book to library
     * @param ok true iff operation went well
     */
    void sendAddBookToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to add new book to user wishlist
     * @param ok true iff operation went well
     */
    void sendEditBookInLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to remove book from library
     * @param ok true iff operation went well
     */
    void sendRemoveBookFromLibraryRequestResult(bool ok);

    // users managements
    // requests

    /**
     * Asks backend to send info about current user
     * @param username username of user to get info about
     */
    void sendUserRequest(QString username);

    /**
     * Sends request to backend to search database for given user
     * @param id email of user to search
     * @param name name of user to search
     */
    void sendSearchUserRequest(QString id, QString name);

    /**
     * Sends request to backend to add new user to library
     * @param user new user to add
     * @param makeAdmin true iff new user should be an admin
     */
    void sendAddUserToLibraryRequest(User user, bool makeAdmin);

    /**
     * Sends request to backend to edit user
     * @param oldId email of user to edit
     * @param newUser new user
     * @param isAdmin true iff user to display is an admin
     * @param newPassword password of new user
     */
    void sendEditUserInLibraryRequest(QString oldId, User newUser, bool isAdmin, QString newPassword);

    /**
     * Sends request to backend to remove user from library
     * @param user user to remove
     */
    void sendRemoveUserFromLibraryRequest(User user);

    // results

    /**
     * Gets search results
     * @param users list of users matching query
     */
    void sendSearchUserRequestResult(std::vector<User> users);

    /**
     * Sends request to backend to add new user to library
     * @param ok true iff operation went well
     */
    void sendAddUserToLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to edit user
     * @param ok true iff operation went well
     */
    void sendEditUserInLibraryRequestResult(bool ok);

    /**
     * Sends request to backend to remove user from library
     * @param ok true iff operation went well
     */
    void sendRemoveUserFromLibraryRequestResult(bool ok);

private:
    AdminBooksLibraryManager *booksManagerTab;  // tab that manages books
    AdminUsersLibraryManager *usersManagerTab;  // tab that manages users

    /**
     * Setups gui
     */
    void setup();

    /**
     * Creates and adds to layout books preferences tab
     */
    void createAndAddBooksManagerTab();

    /**
     * Creates and adds to layout preferences tab
     */
    void createAndAddUsersManagerTab();

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();
};


#endif //LIBRARYMGR_ADMINDIALOG_H
