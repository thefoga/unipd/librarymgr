/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "AdminDialog.h"


AdminDialog::AdminDialog(QString title, QDialog *parent) : UserDialog(title, parent) {}

void AdminDialog::setup() {
    QVBoxLayout *mainLayout = new QVBoxLayout;  // create tabs and layout
    tabWidget = new QTabWidget;

    createAndAddBooksManagerTab();  // add tabs
    createAndAddUsersManagerTab();
    createAndAddMyBooksTab();
    createAndAddPreferencesTab();

    mainLayout->setMenuBar(createMenuBar());
    mainLayout->addWidget(tabWidget);
    setLayout(mainLayout);

    connectSignalsSlots();
}

void AdminDialog::createAndAddBooksManagerTab() {
    booksManagerTab = new AdminBooksLibraryManager();
    tabWidget->addTab(booksManagerTab, "Manage books");
}

void AdminDialog::createAndAddUsersManagerTab() {
    usersManagerTab = new AdminUsersLibraryManager();
    tabWidget->addTab(usersManagerTab, "Manage users");
}

void AdminDialog::connectSignalsSlots() {
    UserDialog::connectSignalsSlots();

    // books
    // send requests
    connect(booksManagerTab, SIGNAL(sendSearchBookRequest(QString, QString)), this,
            SIGNAL(sendSearchBookRequest(QString, QString)));
    connect(booksManagerTab, SIGNAL(sendAddBookToLibraryRequest(Book)), this,
            SIGNAL(sendAddBookToLibraryRequest(Book)));
    connect(booksManagerTab, SIGNAL(sendEditBookInLibraryRequest(QString, Book)), this,
            SIGNAL(sendEditBookInLibraryRequest(QString, Book)));
    connect(booksManagerTab, SIGNAL(sendRemoveBookFromLibraryRequest(Book)), this,
            SIGNAL(sendRemoveBookFromLibraryRequest(Book)));

    // forward results
    connect(this, SIGNAL(sendSearchBookRequestResult(std::vector<Book>)), booksManagerTab,
            SLOT(getSearchBookRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendAddBookToLibraryRequestResult(bool)), booksManagerTab,
            SIGNAL(sendAddBookToLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendEditBookInLibraryRequestResult(bool)), booksManagerTab,
            SIGNAL(sendEditBookInLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveBookFromLibraryRequestResult(bool)), booksManagerTab,
            SLOT(getRemoveBookFromLibraryRequestResult(bool)));

    // users
    // send requests
    connect(usersManagerTab, SIGNAL(sendUserRequest(QString)), this,
            SIGNAL(sendUserRequest(QString)));
    connect(usersManagerTab, SIGNAL(sendSearchUserRequest(QString, QString)), this,
            SIGNAL(sendSearchUserRequest(QString, QString)));
    connect(usersManagerTab, SIGNAL(sendAddUserToLibraryRequest(User, bool)), this,
            SIGNAL(sendAddUserToLibraryRequest(User, bool)));
    connect(usersManagerTab, SIGNAL(sendEditUserInLibraryRequest(QString, User, bool, QString)), this,
            SIGNAL(sendEditUserInLibraryRequest(QString, User, bool, QString)));
    connect(usersManagerTab, SIGNAL(sendRemoveUserFromLibraryRequest(User)), this,
            SIGNAL(sendRemoveUserFromLibraryRequest(User)));

    // forward results
    connect(this, SIGNAL(sendUserRequestResult(User, bool)), usersManagerTab,
            SLOT(getUserRequestResult(User, bool)));
    connect(this, SIGNAL(sendSearchUserRequestResult(std::vector<User>)), usersManagerTab,
            SLOT(getSearchUserRequestResult(std::vector<User>)));
    connect(this, SIGNAL(sendAddUserToLibraryRequestResult(bool)), usersManagerTab,
            SIGNAL(sendAddUserToLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendEditUserInLibraryRequestResult(bool)), usersManagerTab,
            SIGNAL(sendEditUserInLibraryRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveUserFromLibraryRequestResult(bool)), usersManagerTab,
            SLOT(getRemoveUserFromLibraryRequestResult(bool)));
}
