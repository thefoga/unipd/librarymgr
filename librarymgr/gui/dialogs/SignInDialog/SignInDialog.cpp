/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <iostream>
#include <QErrorMessage>


#include "SignInDialog.h"

SignInDialog::SignInDialog(QDialog *parent) : QDialog(parent) {
    setWindowTitle("Sign in");

    setup();  // setups gui

    connect(signInButton, SIGNAL(clicked()), this, SLOT(signIn()));
}

void SignInDialog::signIn() {
    emit sendSignInRequest(emailInputLabel->text(), passwordInputLabel->text());
}

void SignInDialog::setup() {
    QGridLayout *layout = new QGridLayout(this);

    emailInputLabel = new QLineEdit(this);
    passwordInputLabel = new QLineEdit(this);

    signInButton = new QPushButton("Sign in", this);

    layout->addWidget(new QLabel("Email:", this), 1, 0);  // setups positions
    layout->addWidget(emailInputLabel, 1, 1);

    layout->addWidget(new QLabel("Password:", this), 2, 0);
    layout->addWidget(passwordInputLabel, 2, 1);

    layout->addWidget(signInButton, 6, 1);
    signInButton->setDefault(true);

    setLayout(layout);
}

void SignInDialog::getSignInRequestResult(bool signInOk) {
    if (signInOk) {
        close();
        emit sendSignInProcessIsComplete();  // notify parent gui that sign-in is complete!
    } else {
        QMessageBox messageBox;
        messageBox.critical(0, "Wrong username/password",
                            "The username \"" + emailInputLabel->text() + "\" and the password \"" +
                            passwordInputLabel->text() + "\" are invalid!");
        messageBox.setFixedSize(this->size());
    }
}

void SignInDialog::show() {
    emailInputLabel->clear();
    passwordInputLabel->clear();

    QDialog::show();
}
