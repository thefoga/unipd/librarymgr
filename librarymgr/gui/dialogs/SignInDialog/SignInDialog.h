/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_SIGNINDIALOG_H
#define LIBRARYMGR_SIGNINDIALOG_H


#include <QDateTime>
#include <QDateEdit>
#include <QDialog>
#include <QComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

/**
 * Dialog to sign-in new users
 */
class SignInDialog : public QDialog {
Q_OBJECT

public:
    /**
     * New dialog with UI
     * @param parent parent dialog to show dialog in
     * @return New dialog to register user
     */
    explicit SignInDialog(QDialog *parent = 0);

    /**
     * Overrides parent show method to clear inputs before showing
     */
    void show();

public slots:
    /**
    * Sign in
    */
    void signIn();

    /**
     * Gets sign in result
     * @param signInOk whether sign authentication went well
     */
    void getSignInRequestResult(bool signInOk);

signals:
    /**
     * Signal to mark when a sign in request is made by the user via the GUI
     * @param username username to sign in with
     * @param password password of user to authenticate
     */
    void sendSignInRequest(QString username, QString password);

    /**
     * Signal to mark successful sign in process
     */
    void sendSignInProcessIsComplete();

private:
    // gui
    QLineEdit *emailInputLabel;
    QLineEdit *passwordInputLabel;

    QPushButton *signInButton;

    /**
     * Setups gui
     */
    void setup();
};


#endif //LIBRARYMGR_SIGNINDIALOG_H
