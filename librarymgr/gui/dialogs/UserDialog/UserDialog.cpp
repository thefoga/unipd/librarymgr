/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMessageBox>

#include "UserDialog.h"

UserDialog::UserDialog(QString title, QDialog *parent) : QDialog(parent) {
    setWindowTitle(title);  // set window gui
    setFixedSize(QSize(780, 360));
}

void UserDialog::setup() {
    QVBoxLayout *mainLayout = new QVBoxLayout;  // create tabs and layout
    tabWidget = new QTabWidget;

    createAndAddMyBooksTab();  // add tabs
    createAndAddPreferencesTab();

    mainLayout->setMenuBar(createMenuBar());
    mainLayout->addWidget(tabWidget);
    setLayout(mainLayout);

    connectSignalsSlots();
}

void UserDialog::createAndAddMyBooksTab() {
    userBooksTab = new UserBooksWidget();
    tabWidget->addTab(userBooksTab, "My Books");
}

void UserDialog::createAndAddPreferencesTab() {
    userPreferencesTab = new UserPreferencesWidget();
    tabWidget->addTab(userPreferencesTab, "Settings");
}

void UserDialog::connectSignalsSlots() {
    // send requests
    connect(userBooksTab, SIGNAL(sendSearchBookRequest(QString, QString)), this,
            SIGNAL(sendSearchBookRequest(QString, QString)));
    connect(userBooksTab, SIGNAL(sendWishlistRequest()), this, SIGNAL(sendWishlistRequest()));
    connect(userBooksTab, SIGNAL(sendAddBookToWishlistRequest(Book)), this, SIGNAL(sendAddBookToWishlistRequest(Book)));
    connect(userBooksTab, SIGNAL(sendRemoveBookFromWishlistRequest(Book)), this,
            SIGNAL(sendRemoveBookFromWishlistRequest(Book)));
    connect(userBooksTab, SIGNAL(sendReserveBookRequest(Book)), this, SIGNAL(sendReserveBookRequest(Book)));
    connect(userBooksTab, SIGNAL(sendReturnBookRequest(Book)), this,
            SIGNAL(sendReturnBookRequest(Book)));
    connect(userBooksTab, SIGNAL(sendDetailsAboutBookRequest(QString)), this,
            SIGNAL(sendDetailsAboutBookRequest(QString)));
    connect(userBooksTab, SIGNAL(sendLoansRequest()), this,
            SIGNAL(sendLoansRequest()));
    connect(userPreferencesTab, SIGNAL(sendEditUserRequest(User, bool, QString)), this,
            SIGNAL(sendEditUserRequest(User, bool, QString)));
    connect(userPreferencesTab, SIGNAL(sendCurrentUserRequest()), this,
            SIGNAL(sendCurrentUserRequest()));
    connect(userPreferencesTab, SIGNAL(sendImportWishlistFromFileRequest(QString)), this,
            SIGNAL(sendImportWishlistFromFileRequest(QString)));
    connect(userPreferencesTab, SIGNAL(sendExportWishlistFromFileRequest(QString)), this,
            SIGNAL(sendExportWishlistFromFileRequest(QString)));

    // forward results
    connect(this, SIGNAL(sendSearchBookRequestResult(std::vector<Book>)), userBooksTab,
            SLOT(getSearchBookRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendWishlistRequestResult(std::vector<Book>)), userBooksTab,
            SLOT(getWishlistRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendAddBookToWishlistRequestResult(bool)), userBooksTab,
            SLOT(getAddBookToWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendRemoveBookFromWishlistRequestResult(bool)), userBooksTab,
            SLOT(getRemoveBookFromWishlistRequestResult(bool)));
    connect(this, SIGNAL(sendReserveBookRequestResult(bool)), userBooksTab,
            SLOT(getReserveBookRequestResult(bool)));
    connect(this, SIGNAL(sendReturnBookRequestResult(bool)), userBooksTab,
            SLOT(getReturnBookRequestResult(bool)));
    connect(this, SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)), userBooksTab,
            SIGNAL(sendDetailsAboutBookRequestResult(Book, bool, bool)));
    connect(this, SIGNAL(sendLoansRequestResult(std::vector<Book>)), userBooksTab,
            SLOT(getLoansRequestResult(std::vector<Book>)));
    connect(this, SIGNAL(sendEditUserRequestResult(bool)), userPreferencesTab,
            SIGNAL(sendEditUserRequestResult(bool)));
    connect(this, SIGNAL(sendUserRequestResult(User, bool)), userPreferencesTab,
            SLOT(getUserRequestResult(User, bool)));
    connect(this, SIGNAL(sendImportWishlistFromFileRequestResult(bool)), userPreferencesTab,
            SLOT(getImportWishlistFromFileRequestResult(bool)));
    connect(this, SIGNAL(sendExportWishlistFromFileRequestResult(bool)), userPreferencesTab,
            SLOT(getExportWishlistFromFileRequestResult(bool)));
}

void UserDialog::openAboutDialog() {
    QMessageBox::about(
            this,
            "About this app",
            "LibraryMgr is a desktop application to manage a generic library.\n"
                    "The purpose is to create a project for the OOP class in CS course at the University of Padova (Italy)\n"
                    "\n"
                    "Admins can add new books, edit users and so on..\n"
                    "while users can reserve/return/mark as favorite books from the library.\n"
                    "Moreover you can export/import your wishlist and your data.\n"
                    "\n"
                    "It's written in Qt5 and it performs generic operations so that you can bind the client to whatever backend management system there is.\n"
                    "It's open-source and you can find the code and more information here: https://github.com/sirfoga/LibraryMgr."
    );
}

void UserDialog::openHelpDialog() {
    QMessageBox::about(
            this,
            "Help",
            "FAQ:\n"
                    "- What is my password?: when an admin signed you up, the default password for your login is your username (e-mail). You can change it later.\n"
                    "- How to sign out: simply close the app.\n"
                    "- The app broke my PC, what have I to do?: nothing, watch it burn.\n"
                    "- What kind of PC do I need to run the app?: a PC with a CPU and more than 5MB of RAM. A disk may be needed.\n"
    );
}

QMenuBar *UserDialog::createMenuBar() {
    QMenuBar *menuBar = new QMenuBar();

    QAction *openAboutDialogAction = new QAction("About", this);  // setup about action
    openAboutDialogAction->setStatusTip(tr("Shows dialog with about content"));
    connect(openAboutDialogAction, SIGNAL(triggered()), this, SLOT(openAboutDialog()));

    QAction *openHelpDialogAction = new QAction("Help", this);
    openHelpDialogAction->setShortcuts(QKeySequence::HelpContents);  // setup help action
    openHelpDialogAction->setStatusTip(tr("Shows dialog with help content"));
    connect(openHelpDialogAction, SIGNAL(triggered()), this, SLOT(openHelpDialog()));

    QMenu *aboutMenu = menuBar->addMenu("Help");
    aboutMenu->addAction(openAboutDialogAction);
    aboutMenu->addAction(openHelpDialogAction);

    menuBar->addMenu(aboutMenu);
    return menuBar;
}
