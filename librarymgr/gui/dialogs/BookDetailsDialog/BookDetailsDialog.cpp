/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStyle>
#include <QDesktopWidget>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QMessageBox>

#include "BookDetailsDialog.h"

BookDetailsDialog::BookDetailsDialog(bool isReadOnly, QWidget *parent) : QDialog(parent) {
    isbnLabel = new InlineLabel(isReadOnly, "ISBN:", "Book Isbn", this);
    titleLabel = new InlineLabel(isReadOnly, "Title:", "Book title", this);
    descriptionLabel = new InlineLabel(isReadOnly, "Description:", "Book description", this);
}

void BookDetailsDialog::setBookId(QString isbn) {
    this->bookIsbn = isbn;

    emit sendDetailsAboutBookRequest(isbn);
}

Book BookDetailsDialog::getBook() {
    return Book(isbnLabel->getInput().toStdString());
}

void BookDetailsDialog::getDetailsAboutBookRequest(Book book, bool isReservedByUser, bool isInUserWishlist) {
    populate(book);
}

void BookDetailsDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addWidget(isbnLabel);
    layout->addWidget(titleLabel);
    layout->addWidget(descriptionLabel);

    setLayout(layout);
}

void BookDetailsDialog::populate(Book book) {
    isbnLabel->setText(QString(book.getID().c_str()));
    titleLabel->setText(QString(book.getName().c_str()));
    descriptionLabel->setText(QString(book.getDescription().c_str()));
}
