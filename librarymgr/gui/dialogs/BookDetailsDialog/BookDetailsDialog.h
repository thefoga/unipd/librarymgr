/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_BOOKDETAILSDIALOG_H
#define LIBRARYMGR_BOOKDETAILSDIALOG_H


#include <QtWidgets/QWidget>
#include <QDialog>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QPushButton>

#include "../../widgets/InlineLabel/InlineLabel.h"
#include "../../../models/Article/Book/Book.h"

/**
 * Dialog with details about book; features:
 * - ISBN code
 * - title
 * - description
 * - 2 checkboxes: is available | is booked
 */
class BookDetailsDialog : public QDialog {
Q_OBJECT

public:
    /**
     * New dialog
     * @param isReadOnly whether user can edit fields
     * @param parent parent widget to show dialog in
     * @return new dialog
     */
    BookDetailsDialog(bool isReadOnly = true, QWidget *parent = 0);

    /**
     * Setups gui
     */
    virtual void setup();

    /**
     * Sets book dialog
     * @param isbn isbn of book to display
     */
    void setBookId(QString isbn);

    /**
     * Builds book with fields set in dialog
     * @return book
     */
    Book getBook();

public slots:

    /**
     * Gets backend response on book details
     * @param book full book details
     * @param isReservedByUser true iff current user has reserved this book
     * @param isInUserWishlist true iff book is in current user wishlist
     */
    virtual void getDetailsAboutBookRequest(Book book, bool isReservedByUser, bool isInUserWishlist);

signals:

    /**
     * Asks backend for details about book with isbn
     * @param isbn isbn of book to get details about
     */
    void sendDetailsAboutBookRequest(QString isbn);

protected:
    QString bookIsbn = "";  // isbn of currently displayed book
    InlineLabel *isbnLabel;  // widgets
    InlineLabel *titleLabel;
    InlineLabel *descriptionLabel;

    /**
     * Populate dialog with book features
     * @param book book to show in dialog
     */
    void populate(Book book);
};


#endif //LIBRARYMGR_BOOKDETAILSDIALOG_H
