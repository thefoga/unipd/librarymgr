/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStyle>
#include <QDesktopWidget>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QMessageBox>

#include "BookManagerDialog.h"


BookManagerDialog::BookManagerDialog(QWidget *parent) : BookDetailsDialog(true, parent) {
    reserveOrReturnButton = new QPushButton("", this);
    addToOrRemoveFromWishListButton = new QPushButton("", this);

    setup();

    connect(reserveOrReturnButton, SIGNAL(clicked()), this, SLOT(reserveOrReturnBook()));
    connect(addToOrRemoveFromWishListButton, SIGNAL(clicked()), this, SLOT(addToOrRemoveFromWishListBook()));
}

void BookManagerDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addWidget(isbnLabel);
    layout->addWidget(titleLabel);
    layout->addWidget(descriptionLabel);
    layout->addWidget(reserveOrReturnButton);
    layout->addWidget(addToOrRemoveFromWishListButton);

    setLayout(layout);
}

void BookManagerDialog::updateBookStatus(bool isReservedByUser, bool isInUserWishlist) {
    this->isReservedByUser = isReservedByUser;
    this->isInUserWishlist = isInUserWishlist;

    if (isReservedByUser) {
        reserveOrReturnButton->setText("Return");
    } else {
        reserveOrReturnButton->setText("Reserve");
    }

    if (isInUserWishlist) {
        addToOrRemoveFromWishListButton->setText("Remove from wishlist");
    } else {
        addToOrRemoveFromWishListButton->setText("Add to wishlist");
    }
}

void BookManagerDialog::reserveOrReturnBook() {
    Book book = getBook();

    if (isReservedByUser) {
        emit sendReturnBookRequest(book);
    } else {
        emit sendReserveBookRequest(book);
    }

    emit sendDetailsAboutBookRequest(bookIsbn);
}

void BookManagerDialog::addToOrRemoveFromWishListBook() {
    Book book = getBook();

    if (isInUserWishlist) {
        emit sendRemoveBookFromWishlistRequest(book);
    } else {
        emit sendAddBookToWishlistRequest(book);
    }

    emit sendDetailsAboutBookRequest(bookIsbn);
}

void BookManagerDialog::getAddBookToWishlistRequestResult(bool ok) {
    if (isVisible()) {
        if (ok) {
            QMessageBox messageBox;
            messageBox.information(0, "Success",
                                   "The book has been added correctly to your wishlist!");

            isInUserWishlist = true;  // book now is in user wishlist
            updateBookStatus(isReservedByUser, isInUserWishlist);
        } else {
            QMessageBox messageBox;
            messageBox.warning(0, "Error",
                               "The book is already in your wishlist!");
        }
    }
}

void BookManagerDialog::getRemoveBookFromWishlistRequestResult(bool ok) {
    if (isVisible()) {
        if (ok) {
            QMessageBox messageBox;
            messageBox.critical(0, "Success",
                                "The book has been removed successfully from your wishlist!");

            isInUserWishlist = false;  // book now is NOt in user wishlist
            updateBookStatus(isReservedByUser, isInUserWishlist);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error",
                                "The book could not be removed from your wishlist!");
        }
    }
}

void BookManagerDialog::getReserveBookRequestResult(bool ok) {
    if (isVisible()) {
        if (ok) {
            QMessageBox messageBox;
            messageBox.information(0, "Success", "You've successfully reserved this book!");

            isReservedByUser = true;  // now book is reserved by user
            updateBookStatus(isReservedByUser, isInUserWishlist);
        } else {
            QMessageBox messageBox;
            messageBox.warning(0, "Error",
                               "You cannot reserve this book! This book is already loaned to another user!");
        }
    }
}

void BookManagerDialog::getReturnBookRequestResult(bool ok) {
    if (ok) {
        QMessageBox messageBox;
        messageBox.information(0, "Success",
                               "You've successfully returned this book!");

        isReservedByUser = false;  // book now is reserved by user
        updateBookStatus(isReservedByUser, isInUserWishlist);
    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error",
                           "You cannot return this book.");
    }
}

void BookManagerDialog::getDetailsAboutBookRequest(Book book, bool isReservedByUser, bool isInUserWishlist) {
    BookDetailsDialog::getDetailsAboutBookRequest(book, isReservedByUser, isInUserWishlist);
    updateBookStatus(isReservedByUser, isInUserWishlist);
}
