/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_BOOKMANAGERSDIALOG_H
#define LIBRARYMGR_BOOKMANAGERDIALOG_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>

#include "../BookDetailsDialog/BookDetailsDialog.h"
#include "../../widgets/InlineLabel/InlineLabel.h"
#include "../../../models/Article/Book/Book.h"

/**
 * Dialog with details about book and possibility to return/add to wishlist.
 */
class BookManagerDialog : public BookDetailsDialog {
Q_OBJECT

public:
    /**
     * New dialog
     * @param parent parent widget to show dialog in
     * @return new dialog
     */
    BookManagerDialog(QWidget *parent = 0);

public slots:

    /**
     * Gets backend response on book details
     * @param book full book details
     * @param isReservedByUser true iff current user has reserved this book
     * @param isInUserWishlist true iff book is in current user wishlist
     */
    void getDetailsAboutBookRequest(Book book, bool isReservedByUser, bool isInUserWishlist);

    /**
     * Checks if adding requested book to user wishlist operation went well
     * @param ok true iff operation went well
     */
    void getAddBookToWishlistRequestResult(bool ok);

    /**
     * Checks if removing requested book from user wishlist operation went well
     * @param ok true iff operation went well
     */
    void getRemoveBookFromWishlistRequestResult(bool ok);

    /**
     * Checks if reserving requested book operation went well
     * @param ok true iff operation went well
     */
    void getReserveBookRequestResult(bool ok);

    /**
     * Checks if returning requested book operation went well
     * @param ok true iff operation went well
     */
    void getReturnBookRequestResult(bool ok);

    /**
     * Sends to backend request to reserve (or return) book based on books status
     */
    void reserveOrReturnBook();

    /**
     * Sends to backend request to add book to (or remove from) user wishlist
     */
    void addToOrRemoveFromWishListBook();

signals:

    /**
     * Sends request to backend to add new book to user wishlist
     * @param book
     */
    void sendAddBookToWishlistRequest(Book book);

    /**
     * Sends request to backend to remove book from user wishlist
     * @param book
     */
    void sendRemoveBookFromWishlistRequest(Book book);

    /**
     * Sends request to backend to reserve book
     * @param book book to reserve
     */
    void sendReserveBookRequest(Book book);

    /**
     * Sends request to backend to return book
     * @param book book to return
     */
    void sendReturnBookRequest(Book book);

private:
    bool isReservedByUser = false;
    bool isInUserWishlist = false;

    // buttons to add to wishlist/reserve book
    QPushButton *reserveOrReturnButton;
    QPushButton *addToOrRemoveFromWishListButton;

    /**
     * Setups gui
     */
    void setup();

    /**
     * Updates gui status based on book properties
     * @param isReservedByUser whether book is reserved by the current user logged in
     * @param isInUserWishlist whether book is in the wishlist of the current user logged in
     */
    void updateBookStatus(bool isReservedByUser, bool isInUserWishlist);
};


#endif //LIBRARYMGR_BOOKDETAILSDIALOG_H
