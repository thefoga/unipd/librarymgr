/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtWidgets/QWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QLabel>

#include "UserEditorDialog.h"

UserEditorDialog::UserEditorDialog(bool canEditAdminStatus, QWidget *parent) : UserDetailsDialog(false, parent) {
    passwordInput = new InlineLabel(false, "Password", "");
    adminStatusCheckBox = new QCheckBox("Is an admin?");
    adminStatusCheckBox->setEnabled(canEditAdminStatus);

    applySettingsButton = new QPushButton("Apply");
    revertSettingsButton = new QPushButton("Revert");

    setup();
    connectSignalsSlots();
}

void UserEditorDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addWidget(nameLabel);
    layout->addWidget(surnameLabel);

    QWidget *birthDateLabel = new QWidget();
    QHBoxLayout *birthDateLayout = new QHBoxLayout();
    birthDateLayout->addWidget(new QLabel("Birth date:"));
    birthDateLayout->addWidget(birthDate);
    birthDateLabel->setLayout(birthDateLayout);
    layout->addWidget(birthDateLabel);

    layout->addWidget(emailLabel);
    layout->addWidget(passwordInput);
    layout->addWidget(adminStatusCheckBox);

    layout->addWidget(applySettingsButton);
    layout->addWidget(revertSettingsButton);

    setLayout(layout);
}

void UserEditorDialog::connectSignalsSlots() {
    connect(applySettingsButton, SIGNAL(clicked()), this, SLOT(applySettings()));
    connect(revertSettingsButton, SIGNAL(clicked()), this, SLOT(revertSettings()));
}

void UserEditorDialog::getEditUserRequestResult(bool ok) {
    if (ok) {
        QMessageBox messageBox;
        messageBox.information(0, "Success",
                               "Settings edits have been applied successfully!");
        currentUser = getUser();
        isAdmin = adminStatusCheckBox->isChecked();
    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error",
                           "Some of the edits you made could not have been made");
    }
}

void UserEditorDialog::applySettings() {
    emit sendEditUserRequest(getUser(), adminStatusCheckBox->isChecked(),
                             passwordInput->getInput());  // get new user and make edits
}

void UserEditorDialog::revertSettings() {
    populate(currentUser, isAdmin);  // restore edits made
    passwordInput->setText("");  // reset password input
}

void UserEditorDialog::populate(User user, bool isAdmin) {
    UserDetailsDialog::populate(user);
    currentUser = user;

    adminStatusCheckBox->setChecked(isAdmin);
    this->isAdmin = isAdmin;
}
