/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USEREDITORDIALOG_H
#define LIBRARYMGR_USEREDITORDIALOG_H


#include "../UserDetailsDialog/UserDetailsDialog.h"

class UserEditorDialog : public UserDetailsDialog {
Q_OBJECT

public:
    /**
     * New dialog
     * @param canEditAdminStatus whether user can make admin or not
     * @param parent parent widget to show dialog in
     * @return new dialog
     */
    UserEditorDialog(bool canEditAdminStatus = false, QWidget *parent = 0);

    /**
     * Populate dialog with book features
     * @param user user to show in dialog
     * @param isAdmin true iff user to display is an admin
     */
    void populate(User user, bool isAdmin);

public slots:

    /**
     * Gets confirmation of operation success
     * @param ok true iff operation went well
     */
    void getEditUserRequestResult(bool ok);

    /**
     * Gets inputs and asks backend to apply edits
     */
    void applySettings();

    /**
     * Reversts ui to last edit made
     */
    void revertSettings();

signals:

    /**
     * Asks backend to change current user to this new one
     * @param user new user
     * @param isAdmin true iff user to display is an admin
     * @param password new password
     */
    void sendEditUserRequest(User user, bool isAdmin, QString password);

private:
    User currentUser = User("name", "email", QDateTime::currentDateTime().date(), "email");  // current user displayed
    bool isAdmin = false;

    QCheckBox *adminStatusCheckBox;  // checkbox to set admin or not
    InlineLabel *passwordInput;
    QPushButton *applySettingsButton;  // button to apply settings edits
    QPushButton *revertSettingsButton;  // button to revert settings to last edit made

    /**
     * Setups gui
     */
    void setup();

    /**
     * Connect signal and slots of object managed by backend
     */
    void connectSignalsSlots();
};


#endif //LIBRARYMGR_USEREDITORDIALOG_H
