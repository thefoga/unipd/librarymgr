/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_LOGINDIALOG_H
#define LIBRARYMGR_LOGINDIALOG_H


#include <QDialog>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>

#include "../SignInDialog/SignInDialog.h"
#include "../SignUpDialog/SignUpDialog.h"
#include "../../../models/People/User/User.h"

/**
 * Dialog to login
 */
class LoginDialog : public QDialog {
Q_OBJECT

public:
    /**
     * New dialog with UI
     * @param parent parent dialog to show dialog in
     * @return New dialog to insert user
     */
    explicit LoginDialog(QDialog *parent = 0);

public slots:

    /**
     * Open new window with user login
     */
    void signIn();

    /**
     * Open new window with user registration
     */
    void signUp();

    /**
     * Sign in process is complete!
     */
    void getSignInProcessIsComplete();

    /**
     * Exit login dialog
     */
    void exit();

signals:
    /**
     * Mark when a sign in request is made by the user via the GUI
     * @param username username to sign in with
     * @param password password of user to authenticate
     */
    void sendSignInRequest(QString username, QString password);

    /**
     * Sends sign in result
     * @param signInOk whether sign authentication went well
     */
    void sendSignInRequestResult(bool signInOk);

    /**
     * Signal to mark successful sign in process
     */
    void sendSignInProcessIsComplete();

    /**
     * Mark when a sign in request is made by the user via the GUI
     * @param user new user to sign up
     * @param password password of user to authenticate
     */
    void sendSignUpRequest(User user, QString password);

    /**
     * Sends sign up result
     * @param signUpOk whether sign up process went well
     */
    void sendSignUpRequestResult(bool signUpOk);

private:
    // gui
    QPushButton *userSignInButton;  // login as user button
    QPushButton *userSignUpButton;  // register new user
    QPushButton *exitButton;  // exit button

    SignUpDialog *signUpDialog;  // dialog to register new user
    SignInDialog *signInDialog;  // dialog to login

    /**
     * Setups gui
     */
    void setup();

    /**
     * Setups user area gui
     */
    QGroupBox *createUserPanel();
};

#endif // LIBRARYMGR_LOGINDIALOG_H