/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "../../../models/People/User/User.h"
#include "LoginDialog.h"

LoginDialog::LoginDialog(QDialog *parent) : QDialog(parent) {
    setWindowTitle("LibraryMgr");  // set window gui
    setMinimumSize(320, 240);

    signInDialog = new SignInDialog();  // create dialogs
    signUpDialog = new SignUpDialog();
    setup();  // finish GUI setup

    // connect gui slots
    connect(userSignInButton, SIGNAL(clicked()), this, SLOT(signIn()));
    connect(userSignUpButton, SIGNAL(clicked()), this, SLOT(signUp()));
    connect(exitButton, SIGNAL(clicked()), this, SLOT(exit()));

    // connect dialogs slots
    connect(signInDialog, SIGNAL(sendSignInRequest(QString, QString)), this,
            SIGNAL(sendSignInRequest(QString, QString)));
    connect(this, SIGNAL(sendSignInRequestResult(bool)), signInDialog, SLOT(getSignInRequestResult(bool)));
    connect(signInDialog, SIGNAL(sendSignInProcessIsComplete()), this, SLOT(getSignInProcessIsComplete()));

    connect(signUpDialog, SIGNAL(sendSignUpRequest(User, QString)), this, SIGNAL(sendSignUpRequest(User, QString)));
    connect(this, SIGNAL(sendSignUpRequestResult(bool)), signUpDialog, SLOT(getSignUpRequestResult(bool)));
}

void LoginDialog::signIn() {
    signInDialog->show();
}

void LoginDialog::signUp() {
    signUpDialog->show();
}

void LoginDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);  // layouts
    setLayout(layout);

    createUserPanel();  // login panels
    exitButton = new QPushButton("Exit", this);

    layout->addWidget(new QLabel("Welcome to LibraryMgr!"));  // layout position
    layout->addSpacing(20);
    layout->addWidget(createUserPanel());
    layout->addSpacing(20);
    layout->addSpacing(20);
    layout->addWidget(exitButton);
}

QGroupBox *LoginDialog::createUserPanel() {
    QVBoxLayout *userAreaLayout = new QVBoxLayout();  // layout
    QGroupBox *gBoxUser = new QGroupBox("Login");  // panels
    gBoxUser->setLayout(userAreaLayout);

    userSignInButton = new QPushButton("Sign-in", this);
    userSignUpButton = new QPushButton("Sign-up", this);

    userAreaLayout->addWidget(userSignInButton);
    userAreaLayout->addSpacing(20);
    userAreaLayout->addWidget(userSignUpButton);

    return gBoxUser;
}

void LoginDialog::exit() {
    close();
}

void LoginDialog::getSignInProcessIsComplete() {
    close();
    emit sendSignInProcessIsComplete();
}
