/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USERADDERDIALOG_H
#define LIBRARYMGR_USERADDERDIALOG_H


#include "../UserDetailsDialog/UserDetailsDialog.h"

class UserAdderDialog : public UserDetailsDialog {
Q_OBJECT

public:
    /**
     * New dialog
     * @param parent parent widget to show dialog in
     * @return new dialog
     */
    UserAdderDialog(QWidget *parent = 0);

public slots:

    /**
     * Sends request to backend to add new user to library
     */
    void addNewUser();

    /**
     * Sends request to backend to add new user to library
     * @param ok true iff operation went well
     */
    void getAddUserToLibraryRequestResult(bool ok);

signals:

    /**
     * Sends request to backend to add new user to library
     * @param user new user to add
     * @param makeAdmin true iff new user should be an admin
     */
    void sendAddUserToLibraryRequest(User user, bool makeAdmin);

private:
    QCheckBox *adminStatusCheckBox;  // checkbox to set admin or not
    QPushButton *addNewUserButton;  // button to add new user

    /**
     * Setups gui
     */
    void setup();
};


#endif //LIBRARYMGR_USERADDERDIALOG_H
