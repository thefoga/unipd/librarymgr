/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStyle>
#include <QDesktopWidget>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QLabel>

#include "UserAdderDialog.h"

UserAdderDialog::UserAdderDialog(QWidget *parent) : UserDetailsDialog(false, parent) {
    adminStatusCheckBox = new QCheckBox("Is an admin?");
    addNewUserButton = new QPushButton("Add");
    setup();

    connect(addNewUserButton, SIGNAL(clicked()), this, SLOT(addNewUser()));
}

void UserAdderDialog::addNewUser() {
    emit sendAddUserToLibraryRequest(getUser(), adminStatusCheckBox->isChecked());
}

void UserAdderDialog::getAddUserToLibraryRequestResult(bool ok) {
    if (isVisible()) {
        if (ok) {
            QMessageBox messageBox;
            messageBox.information(0, "Success",
                                   "The user has been added correctly to the library!");
        } else {
            QMessageBox messageBox;
            messageBox.warning(0, "Error",
                               "The user cannot be added to the library! There is already one with the same id!");
        }
    }
}

void UserAdderDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addWidget(nameLabel);
    layout->addWidget(surnameLabel);

    QWidget *birthDateLabel = new QWidget();
    QHBoxLayout *birthDateLayout = new QHBoxLayout();
    birthDateLayout->addWidget(new QLabel("Birth date:"));
    birthDateLayout->addWidget(birthDate);
    birthDateLabel->setLayout(birthDateLayout);
    layout->addWidget(birthDateLabel);

    layout->addWidget(emailLabel);
    layout->addWidget(adminStatusCheckBox);
    layout->addWidget(addNewUserButton);

    setLayout(layout);
}
