/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "SignUpDialog.h"

SignUpDialog::SignUpDialog(QDialog *parent) : QDialog(parent) {
    setWindowTitle("Sign up");

    setup();  // setups gui

    connect(signUpButton, SIGNAL(clicked()), this, SLOT(signUp()));
}

void SignUpDialog::signUp() {
    User user(
            nameInputLabel->text().toStdString(),
            surnameInputLabel->text().toStdString(),
            birthDateInputLabel->date(),
            emailInputLabel->text().toStdString()
    );

    emit sendSignUpRequest(user, passwordInputLabel->text());
}

void SignUpDialog::setup() {
    QGridLayout *layout = new QGridLayout(this);

    emailInputLabel = new QLineEdit(this);
    nameInputLabel = new QLineEdit(this);
    surnameInputLabel = new QLineEdit(this);

    birthDateInputLabel = new QDateEdit(this);
    birthDateInputLabel->setDisplayFormat("yyyy.MM.dd");
    birthDateInputLabel->setMinimumDate(QDate(1, 1, 1900));
    birthDateInputLabel->setMaximumDate(QDateTime::currentDateTime().date());
    passwordInputLabel = new QLineEdit(this);

    signUpButton = new QPushButton("Sign up", this);

    layout->addWidget(new QLabel("Email:"), 1, 0);  // setups positions
    layout->addWidget(emailInputLabel, 1, 1);

    layout->addWidget(new QLabel("Name:"), 2, 0);
    layout->addWidget(nameInputLabel, 2, 1);

    layout->addWidget(new QLabel("Surname:"), 3, 0);
    layout->addWidget(surnameInputLabel, 3, 1);

    layout->addWidget(new QLabel("Birth date (optional):"), 4, 0);
    layout->addWidget(birthDateInputLabel, 4, 1);

    layout->addWidget(new QLabel("Password:", this), 5, 0);
    layout->addWidget(passwordInputLabel, 5, 1);

    layout->addWidget(signUpButton, 6, 1);
    signUpButton->setDefault(true);

    setLayout(layout);
}

void SignUpDialog::getSignUpRequestResult(bool signUpOk) {
    if (signUpOk) {  // show dialog successful signup
        QMessageBox messageBox;
        messageBox.information(0, "Sign-up completed",
                               "Now you can sign-in with the username and the password provided");
        messageBox.setFixedSize(this->size());
    } else {
        QMessageBox messageBox;
        messageBox.warning(0, "Failed sign-up process",
                           "The username \"" + emailInputLabel->text() + "\" is already registered!");
        messageBox.setFixedSize(this->size());
    }

    close();
}

void SignUpDialog::show() {
    emailInputLabel->clear();
    nameInputLabel->clear();
    surnameInputLabel->clear();
    birthDateInputLabel->clear();
    passwordInputLabel->clear();

    QDialog::show();
}
