/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_SIGNUPDIALOG_H
#define LIBRARYMGR_SIGNUPDIALOG_H


#include <QDateTime>
#include <QDateEdit>
#include <QDialog>
#include <QComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

#include "../../../models/People/User/User.h"

/**
 * Dialog to sign-up new users
 */
class SignUpDialog : public QDialog {
Q_OBJECT

public:
    /**
     * New dialog with UI
     * @param parent parent dialog to show dialog in
     * @return New dialog to register user
     */
    explicit SignUpDialog(QDialog *parent = 0);

    /**
     * Overrides parent show method to clear inputs before showing
     */
    void show();

public slots:
    /**
     * Sign up
     */
    void signUp();

    /**
     * Gets sign up result
     * @param signUpOk whether sign up process went well
     */
    void getSignUpRequestResult(bool signUpOk);

signals:

    /**
     * Signal to mark when a sign in request is made by the user via the GUI
     * @param user new user to sign up
     * @param password password of user to authenticate
     */
    void sendSignUpRequest(User user, QString password);

private:
    // gui
    QLineEdit *emailInputLabel;
    QLineEdit *nameInputLabel;
    QLineEdit *surnameInputLabel;
    QDateEdit *birthDateInputLabel;
    QLineEdit *passwordInputLabel;

    QPushButton *signUpButton;

    /**
     * Setups gui
     */
    void setup();
};


#endif //LIBRARYMGR_SIGNUPDIALOG_H
