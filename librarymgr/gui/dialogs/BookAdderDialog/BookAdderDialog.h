/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_BOOKADDERDIALOG_H
#define LIBRARYMGR_BOOKADDERDIALOG_H


#include <QtWidgets/QWidget>
#include <QtWidgets/QPushButton>

#include "../BookDetailsDialog/BookDetailsDialog.h"

class BookAdderDialog : public BookDetailsDialog {
Q_OBJECT

public:
    /**
     * New dialog
     * @param parent parent widget to show dialog in
     * @return new dialog
     */
    BookAdderDialog(QWidget *parent = 0);

    /**
     * Builds book with fields set in dialog
     * @return book
     */
    Book getBook();

public slots:

    /**
     * Sends request to backend to add new book to library
     * @param ok true iff operation went well
     */
    void getAddBookToLibraryRequest(bool ok);

    /**
     * Sends request to backend to add new book to library
     */
    void addNewBookToLibrary();

signals:

    /**
     * Sends request to backend to add new book to library
     * @param book new book to add to library
     */
    void sendAddBookToLibraryRequest(Book book);

private:
    QPushButton *addNewBookButton;

    /**
     * Setups gui
     */
    void setup();
};


#endif //LIBRARYMGR_BOOKADDERDIALOG_H
