/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStyle>
#include <QDesktopWidget>
#include <QtCore/QCoreApplication>
#include <QtWidgets/QMessageBox>

#include "BookAdderDialog.h"

BookAdderDialog::BookAdderDialog(QWidget *parent) : BookDetailsDialog(false, parent) {
    addNewBookButton = new QPushButton("Add", this);
    setup();

    connect(addNewBookButton, SIGNAL(clicked()), this, SLOT(addNewBookToLibrary()));
}

void BookAdderDialog::getAddBookToLibraryRequest(bool ok) {
    if (isVisible()) {
        if (ok) {
            QMessageBox messageBox;
            messageBox.information(0, "Success",
                                   "The book has been added correctly to the library!");
        } else {
            QMessageBox messageBox;
            messageBox.warning(0, "Error",
                               "The book cannot be added to the library! There is already one with the same id!");
        }
    }
}

void BookAdderDialog::setup() {
    QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addWidget(isbnLabel);
    layout->addWidget(titleLabel);
    layout->addWidget(descriptionLabel);
    layout->addWidget(addNewBookButton);

    setLayout(layout);
}

void BookAdderDialog::addNewBookToLibrary() {
    emit sendAddBookToLibraryRequest(getBook());
}

Book BookAdderDialog::getBook() {
    return Book(isbnLabel->getInput().toStdString(), titleLabel->getInput().toStdString(),
                descriptionLabel->getInput().toStdString());
}
