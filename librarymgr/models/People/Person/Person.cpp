/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <iostream>
#include "Person.h"

Person::Person(std::string name, std::string surname, QDate date)
        : name(name), surname(surname), dateOfBirth(date) {}

Person::Person(const Person &p) : Person(p.getName(), p.getSurname(), p.getDateOfBirth()) {}

Person &Person::operator=(const Person &p) {
    if (*this != p) {  // set features
        name = p.getName();
        surname = p.getSurname();
        dateOfBirth = p.getDateOfBirth();
    }

    return *this;
}

Person::~Person() {}

bool Person::operator==(const Person &other) const {
    return name == other.name
           && surname == other.surname
           && dateOfBirth == other.dateOfBirth;
}

bool Person::operator!=(const Person &other) const {
    return name != other.name
           || surname != other.surname
           || dateOfBirth != other.dateOfBirth;
}

void Person::setName(std::string name) {
    this->name = name;
}

void Person::setSurname(std::string surname) {
    this->surname = surname;
}

void Person::setDateOfBirth(QDate dateOfBirth) {
    this->dateOfBirth = dateOfBirth;
}

std::string Person::getName() const {
    return name;
}

std::string Person::getSurname() const {
    return surname;
}

QDate Person::getDateOfBirth() const {
    return dateOfBirth;
}

std::ostream &operator<<(std::ostream &out, const Person &person) {
    return out << "Person {" << std::endl
               << "name: " << person.getName() << std::endl
               << "surname: " << person.getSurname() << std::endl
               << "date of birth: " << person.getDateOfBirth().toString().toUtf8().constData() << std::endl
               << "}";
}

std::ostream &operator<<(std::ostream &out, const Person *person) {
    return out << *person;
}
