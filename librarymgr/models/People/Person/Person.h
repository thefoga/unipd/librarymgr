/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_PERSON_H
#define LIBRARYMGR_PERSON_H

#include <QDate>
#include <string>

/**
 * @brief Wrap for generic person personal info
 */
class Person {
public:
    // constructors

    /**
     * @brief Person default constructor with name, surname and date of birth
     * @param name name of person
     * @param surname surname of person
     * @param date date of birth of person
     */
    Person(std::string name, std::string surname, QDate date);

    /**
     * @brief Person deep copy of person
     * @param p person to copy
     */
    Person(const Person &p);

    /**
     * @brief operator = Person deep copy of person
     * @param p person to copy
     * @return deep copy of person
     */
    Person &operator=(const Person &p);

    /**
     * @brief ~Person Destroy person fields
    */
    virtual ~Person();

    /**
     * @brief virtual because there can be 2 persons with same name and surname and date of birth
     * @param other other object to compare with
     * @return True if the 2 persons are the same
     */
    virtual bool operator==(const Person &other) const;

    /**
     * @brief virtual because there can be 2 persons with same name and surname and date of birth
     * @param other other object to compare with
     * @return True if the 2 persons are different
     */
    virtual bool operator!=(const Person &other) const;

    // setters

    /**
     * @brief set new name
     * @param name name of person
     */
    void setName(std::string name);

    /**
     * @brief set new surname
     * @param surname surname of person
     */
    void setSurname(std::string surname);

    /**
     * @brief set new date of birth
     * @param dateOfBirth new date of birth
     */
    void setDateOfBirth(QDate dateOfBirth);

    // getters

    /**
     * @brief gets name
     * @return name of person
     */
    std::string getName() const;

    /**
     * @brief gets surname
     * @return surname of person
     */
    std::string getSurname() const;

    /**
     * @brief gets date of birth
     * @return date of birth
     */
    QDate getDateOfBirth() const;

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Person &person);

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Person *person);

private:
    std::string name;  // name of person
    std::string surname;  // surname of person
    QDate dateOfBirth;  // assuming one does not change own date of birth
};

#endif // LIBRARYMGR_PERSON_H
