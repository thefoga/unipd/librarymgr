/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_USER_H
#define LIBRARYMGR_USER_H

#include <string>
#include <vector>

#include "../../../models/Database/Database.h"
#include "../Person/Person.h"
#include "../../Article/Book/Book.h"

/**
 * User of app
 */
class User : public Person {
    friend class IOStream;

public:
    // constructors

    /**
     * @brief User New Person with email and optional date of birth
     * @param name name of person
     * @param surname surname of person
     * @param date date of birth of person
     * @param email email to register user
     */
    User(
            std::string name,
            std::string surname,
            QDate date,
            std::string email
    );

    /**
     * New Person with email and optional date of birth
     * @param email email to register user
     */
    User(
            std::string email
    );

    /**
     * @brief User deep copy of user
     * @param b user to copy
     */
    User(const User &u);

    /**
     * @brief isAdmin checks whether user is an admin
     * @return true iff user is an admin
     */
    bool isAdmin();

    /**
     * Reserve given book from library
     * @param book book to reserve
     * @return true iff operation went well
     */
    bool reserveBook(Book *book);

    /**
     * Return given book to library
     * @param book book to return to library
     * @return true iff operation went well
     */
    bool returnBook(Book *book);

    /**
     * Checks if given book is reserved by user
     * @param book book to check in user reservations
     * @return true iff user is reserving this book
     */
    bool isBookReservedByUser(Book book);

    /**
     * Checks if given book is user wishlist
     * @param book book to check in user wishlist
     * @return true iff user is in user wishlist
     */
    bool isBookInWishlist(Book book);

    // setters

    /**
     * Sets user email
     * @param email email to register user
     */
    void setEmail(std::string email);

    /**
     * @brief appends book to list of books user wants to read
     * @param book book to add
     */
    void addBookToWishList(Book book);

    /**
     * @brief removes book from list of books user wants to read
     * @param book book to remove
     */
    void removeBookFromWishList(Book book);

    // getters

    /**
     * @brief gets user email
     * @return user email
     */
    std::string getEmail() const;

    /**
     * Gets list of books user wants to read
     * @return list of books user wants to read
     */
    std::vector<Book> getWishList() const;

    /**
     * Gets list of books loaned to user
     * @return list of books loaned to user
     */
    std::vector<Book> getLoans() const;

    // operators

    /**
     * @brief operator = User deep copy of person
     * @param u user to copy
     * @return deep copy of user
     */
    User &operator=(const User &u);

    /**
     * @brief equals 2 users by comparing emails
     * @param other other object to compare with
     * @return true iff 2 users are the same
     */
    bool operator==(const User &other) const;

    /**
     * @brief equals 2 users by comparing emails
     * @param other other object to compare with
     * @return true iff 2 users are different
     */
    bool operator!=(const User &other) const;

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const User &user);

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const User *user);

protected:
    bool amIAdmin = false;
    std::string email;  // email of user
    Database<Book> wishList;  // list of books user wants to read
    Database<Book> loans;  // list of books loaned to the user
};

#endif // LIBRARYMGR_USER_H
