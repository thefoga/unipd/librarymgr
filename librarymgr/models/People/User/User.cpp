/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "User.h"

User::User(std::string name, std::string surname, QDate date, std::string email)
        : Person(name, surname, date), email(email) {}

User::User(std::string email) : User("Name", "Surname", QDateTime::currentDateTime().date(), email) {}

User::User(const User &u)
        : User(u.getName(), u.getSurname(), u.getDateOfBirth(), u.getEmail()) {}

User &User::operator=(const User &u) {
    setName(u.getName());
    setSurname(u.getSurname());
    setEmail(u.getEmail());
    setDateOfBirth(u.getDateOfBirth());

    return *this;
}

bool User::operator==(const User &other) const {
    return getEmail() == other.getEmail();
}

bool User::operator!=(const User &other) const {
    return !(*this == other);
}

bool User::isAdmin() {
    return amIAdmin;
}

void User::setEmail(std::string email) {
    this->email = email;
}

void User::addBookToWishList(Book book) {
    wishList.add(book);
}

void User::removeBookFromWishList(Book book) {
    wishList.remove(book);
}

std::string User::getEmail() const {
    return email;
}

std::vector<Book> User::getWishList() const {
    return wishList.getItems();
}

std::ostream &operator<<(std::ostream &out, const User &user) {
    return out << "User {" << std::endl
               << "name: " << user.getName() << std::endl
               << "surname: " << user.getSurname() << std::endl
               << "email: " << user.getEmail() << std::endl
               << "}";
}

std::ostream &operator<<(std::ostream &out, const User *user) {
    return out << *user;
}

bool User::reserveBook(Book *book) {
    if (book->reserve()) {  // if it's available
        loans.add(*book);  // add to list of currently reserved books
        return true;
    }

    return false;
}

bool User::returnBook(Book *book) {
    if (book->returnToLibrary()) {
        loans.remove(*book);  // remove from list of currently reserved books
        return true;
    }

    return false;
}

bool User::isBookReservedByUser(Book book) {
    return loans.isInDatabase(book);  // search list
}

bool User::isBookInWishlist(Book book) {
    return wishList.isInDatabase(book);
}

std::vector<Book> User::getLoans() const {
    return loans.getItems();
}
