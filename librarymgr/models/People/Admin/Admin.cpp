/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "Admin.h"

Admin::Admin(
        std::string name,
        std::string surname,
        QDate date,
        std::string email,
        Database<SmartPointer<Book>> *books,
        Database<SmartPointer<User>> *users
) : User(name, surname, date, email), books(books), users(users) {
    amIAdmin = true;
}

Admin::Admin(const Admin &a) : Admin(a.getName(), a.getSurname(), a.getDateOfBirth(), a.getEmail(), a.books, a.users) {
    amIAdmin = true;
}

Admin::Admin(SmartPointer<User> u) : Admin(u->getName(), u->getSurname(), u->getDateOfBirth(), u->getEmail(), NULL,
                                           NULL) {
    amIAdmin = true;
}

Admin::Admin(
        SmartPointer<User> u,
        Database<SmartPointer<Book>> *books,
        Database<SmartPointer<User>> *users
) : Admin(u->getName(), u->getSurname(), u->getDateOfBirth(), u->getEmail(), books, users) {
    amIAdmin = true;
}

Admin &Admin::operator=(const Admin &a) {
    if (*this != a) {  // set features
        setName(a.getName());
        setSurname(a.getSurname());
        setEmail(a.getEmail());
        setDateOfBirth(a.getDateOfBirth());
    }

    return *this;
}

Admin::~Admin() {}

void Admin::addBook(SmartPointer<Book> book) {
    books->add(book);
}

void Admin::removeBook(SmartPointer<Book> book) {
    books->remove(book);
}

void Admin::replaceBook(SmartPointer<Book> oldBook, SmartPointer<Book> newBook) {
    books->replace(oldBook, newBook);
}

void Admin::addUser(SmartPointer<User> user) {
    users->add(user);
}

void Admin::removeUser(SmartPointer<User> user) {
    users->remove(user);
}

SmartPointer<User> Admin::makeAdmin(User *u) {
    return SmartPointer<User>(new Admin(new User(*u), books, users));  // copy user and create new admin
}

std::ostream &operator<<(std::ostream &out, const Admin &admin) {
    return out << "Admin {"
               << "name: " << admin.getName() << std::endl
               << "surname: " << admin.getSurname() << std::endl
               << "email: " << admin.getEmail() << std::endl
               << "}";
}

std::ostream &operator<<(std::ostream &out, const Admin *admin) {
    return out << *admin;
}
