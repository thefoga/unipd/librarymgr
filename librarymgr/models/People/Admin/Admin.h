/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_ADMIN_H
#define LIBRARYMGR_ADMIN_H

#include <iostream>

#include "../User/User.h"
#include "../../Database/Database.h"
#include "../../../utils/SmartPointer/SmartPointer.h"

/**
 * @brief A library user who is also an admin (i.e can manage books of library)
 */
class Admin : public User {
public:
    // constructors

    /**
     * @brief Admin New Person with email and optional date of birth
     * @param name name of person
     * @param surname surname of person
     * @param date date of birth of person
     * @param email email to register user
     * @param books library books to manage
     */
    Admin(
            std::string name,
            std::string surname,
            QDate date,
            std::string email,
            Database<SmartPointer<Book>> *books,
            Database<SmartPointer<User>> *users
    );

    /**
     * @brief Admin deep copy of admin
     * @param a admin to copy
     */
    Admin(const Admin &a);

    /**
     * @brief Admin make user an admin
     * @param u user to make admin
     */
    Admin(SmartPointer<User> u);

    /**
     * @brief Admin make user an admin
     * @param u user to make admin
     * @param books books of admin to manage
     * @param users users of admin to manage
     */
    Admin(SmartPointer<User> u, Database<SmartPointer<Book>> *books,
          Database<SmartPointer<User>> *users);  // to make admin a simple user

    /**
     * @brief ~Admin Destroy admin fields (but not shared books library)
    */
    ~Admin();

    // books

    /**
     * @brief adds new book to library
     * @param book new book to add
     */
    void addBook(SmartPointer<Book> book);

    /**
     * @brief adds new book to library
     * @param book new book to add
     */
    void removeBook(SmartPointer<Book> book);

    /**
     * @brief replaces old books with new books
     * @param oldBook book to replace with new
     * @param newBook new book
     */
    void replaceBook(SmartPointer<Book> oldBook, SmartPointer<Book> newBook);

    // users

    /**
     * @brief adds new user to library
     * @param book new user to add
     */
    void addUser(SmartPointer<User> user);

    /**
     * @brief adds new user to library
     * @param book new user to add
     */
    void removeUser(SmartPointer<User> user);

    /**
     * @brief makes user an admin
     * @param u user to make admin
     * @return admin
     */
    SmartPointer<User> makeAdmin(User *u);

    // operators

    /**
    * @brief operator = Admin deep copy of person
    * @param a admin to copy
    * @return deep copy of admin
    */
    Admin &operator=(const Admin &a);

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Admin &admin);

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Admin *admin);

private:
    Database<SmartPointer<Book>> *books;  // database of books
    Database<SmartPointer<User>> *users;  // list of users in library
};

#endif // LIBRARYMGR_ADMIN_H
