/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_ITEM_H
#define LIBRARYMGR_ITEM_H

#include <string>
#include <ostream>

/**
 * @brief general item to buy with ID
 */
class Item {
public:
    /**
     * @brief Item New item with ID, name and description
     * @param IDCode unique code of item
     * @param name name of article
     * @param description comment on article
     */
    Item(const std::string IDCode = 0, std::string name = "", std::string description = "");

    /**
     * @brief Item new item with ID code and default features
     * @param i item to copy
     */
    Item(const Item &i);

    // setters

    /**
     * @brief sets new ID of item
     * @param IDCode new ID of item
     */
    void setID(std::string IDCode);

    /**
     * @brief sets new name of item
     * @param name new name of item
     */
    void setName(std::string name);

    /**
     * @brief sets new description of item
     * @param description new description of item
     */
    void setDescription(std::string description);

    // getters

    /**
     * @brief gets ID code of book
     * @return ID code of book
     */
    const std::string getID() const;

    /**
     * @brief gets name
     * @return name of item
     */
    std::string getName() const;

    /**
     * @brief gets description
     * @return description of item
     */
    std::string getDescription() const;

    // operators

    /**
     * @brief equals other item
     * @param other other object to compare with
     * @return true iff 2 items are the same
     */
    bool operator==(const Item &other) const;

    /**
     * @brief operator << printer
     * @param out stream
     * @param item object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Item &item);

    /**
     * @brief operator << printer
     * @param out stream
     * @param item object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Item *item);

private:
    std::string IDCode;  // code of item
    std::string name;  // name of item
    std::string description;  // description
};

#endif // LIBRARYMGR_ITEM_H
