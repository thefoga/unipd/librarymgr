/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <iostream>
#include "Item.h"

Item::Item(const std::string IDCode, std::string name, std::string description)
        : IDCode(IDCode), name(name), description(description) {}

Item::Item(const Item &i) : Item(i.IDCode, i.name, i.description) {}

bool Item::operator==(const Item &other) const {
    return IDCode == other.getID();
}

void Item::setID(std::string IDCode) {
    this->IDCode = IDCode;
}

void Item::setName(std::string name) {
    this->name = name;
}

void Item::setDescription(std::string description) {
    this->description = description;
}

const std::string Item::getID() const {
    return IDCode;
}

std::string Item::getName() const {
    return name;
}

std::string Item::getDescription() const {
    return description;
}

std::ostream &operator<<(std::ostream &out, const Item &item) {
    return out << "T {" << std::endl
               << "name: " << item.getName() << std::endl
               << "ID code: " << item.getID() << std::endl
               << "description: " << item.getDescription() << std::endl
               << "}";
}

std::ostream &operator<<(std::ostream &out, const Item *item) {
    return out << *item;
}
