/**
 * Copyright {yyyy} Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <typeinfo>
#include <iostream>

#include "Book.h"

Book::Book(const std::string isbnCode, std::string title, std::string description, bool isAvailable, bool isBooked)
        : Item(isbnCode, title, description), isAvailable(isAvailable), isBooked(isBooked) {}

Book::Book(const std::string isbnCode)
        : Item(isbnCode) {}

Book::Book(const Book &b) : Book(b.getID(), b.getName(), b.getDescription(), b.isAvailableToUsers(),
                                 b.isBookedByUser()) {}

Book &Book::operator=(const Book &b) {
    if (*this != b) {  // set features
        setName(b.getName());
        setDescription(b.getDescription());
        setID(b.getID());

        if (b.isAvailableToUsers()) {
            makeAvailableToUsers();
        } else {
            makeUnAvailableToUsers();
        }

        if (b.isBookedByUser()) {
            returnToLibrary();
        } else {
            reserve();
        }
    }

    return *this;
}

bool Book::operator==(const Book &other) const {
    return getID() == other.getID();
}

bool Book::operator!=(const Book &other) const {
    return !(*this == other);
}

void Book::makeUnAvailableToUsers() {
    isAvailable = false;
}

void Book::makeAvailableToUsers() {
    isAvailable = true;
}

bool Book::returnToLibrary() {
    isBooked = false;
    return true;
}

bool Book::reserve() {
    if (isAvailable && !isBooked) {  // first check if it's available to users
        isBooked = true;
        return true;
    }

    return false;
}

bool Book::isAvailableToUsers() const {
    return isAvailable;
}

bool Book::isBookedByUser() const {
    return isBooked;
}

std::ostream &operator<<(std::ostream &out, const Book &book) {
    return out << "Book {" << std::endl
               << "name: " << book.getName() << std::endl
               << "ID code: " << book.getID() << std::endl
               << "description: " << book.getDescription() << std::endl
               << "available: " << book.isAvailableToUsers() << std::endl
               << "booked: " << book.isBookedByUser() << std::endl
               << "}";
}

std::ostream &operator<<(std::ostream &out, const Book *book) {
    return out << *book;
}

