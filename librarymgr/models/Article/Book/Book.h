/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_BOOK_H
#define LIBRARYMGR_BOOK_H

#include <ostream>

#include "../Item/Item.h"

/**
 * @brief Library book with author, ISBN and status
 */
class Book : public Item {
public:
    // constructors

    /**
     * @brief Book new library book with initial status
     * @param isbnCode ISBN code of book
     * @param title title of book
     * @param description comment on book
     * @param isAvailable whether book is available to public
     * @param isBookable whether book can be reserved by the public
     */
    Book(
            const std::string isbnCode,
            std::string title,
            std::string description,
            bool isAvailable = true,
            bool isBooked = false
    );

    /**
     * @brief Book new book with ISBN code and default features
     * @param isbnCode ISBN code of book
     */
    Book(const std::string isbnCode);

    /**
     * @brief Book deep copy book
     * @param b book to copy
     */
    Book(const Book &b);

    // setters

    /**
     * @brief makes this book unavailable to library users
     */
    void makeUnAvailableToUsers();

    /**
     * @brief makes this book available to library users
     */
    void makeAvailableToUsers();

    /**
     * Returns this book to library so it's bookable by other users
     * @return true iff operation went well
     */
    bool returnToLibrary();

    /**
     * Reserves this book -> not bookable by other users
     * @return true iff operation went well
     */
    bool reserve();

    // getters

    /**
     * @brief find if this book is available to users
     * @return true only if it's available to users
     */
    bool isAvailableToUsers() const;

    /**
     * @brief find if this book is bookable by users
     * @return true only if it's bookable by users
     */
    bool isBookedByUser() const;

    // operators

    /**
     * @brief operator = Book deep copy of person
     * @param b book to copy
     * @return deep copy of book
     */
    Book &operator=(const Book &b);

    /**
     * @brief equals other item
     * @param other other object to compare with
     * @return true iff 2 items are the same
     */
    bool operator==(const Book &other) const;

    /**
     * @brief compare with other object
     * @param other other object to compare with
     * @return True if the 2 books are different
     */
    bool operator!=(const Book &other) const;

    /**
     * @brief operator << printer
     * @param out stream
     * @param book object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Book &book);

private:
    bool isAvailable;  // can users view it
    bool isBooked;  // is booked by other user
};

#endif // LIBRARYMGR_BOOK_H
