/**
* Copyright 2016-2017 Stefano Fogarollo
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
        *
        * http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
        * limitations under the License.
*/


#ifndef LIBRARYMGR_DATABASE_H
#define LIBRARYMGR_DATABASE_H

#include <algorithm>
#include <vector>
#include <ostream>
#include <iostream>

/**
 * @brief Abstract collection of object
 */
template<class T>
class Database {
public:
    // constructors

    /**
     * @brief Database new database from scratch
     */
    Database() {}

    /**
     * @brief Database deep copy of database
     * @param d database to copy
     */
    Database(Database &d) : items(d.items) {}

    /**
     * @brief ~Database delete database
     */
    ~Database() {}

    /**
     * @brief operator [] reference to item
     * @param i iterator to get reference of
     * @return reference to item
     */
    T operator[](int i) const {
        return items[i];
    }

    /**
     * @brief adds new item (if there is no duplicate)
     * @param item stuff to add
     * @param allowDuplicates whether to allow duplicates in database or not
     */
    void add(T item, bool allowDuplicates = false) {
        if (allowDuplicates) {
            items.push_back(item);
        } else {  // no duplicates -> find if there is already the item
            if (!isInDatabase(item)) {
                items.push_back(item);
            }
        }
    }

    /**
     * @brief removes from database all items equals to given one
     * @param item stuff to remove
     */
    void remove(T item) {
        for (unsigned i = 0; i < items.size(); i++) {
            if (items[i] == item) {
                items.erase(items.begin() + i);  // remove item
            }
        }
    }

    /**
     * @brief replaces item with new item if old item is in database
     * @param oldItem old stuff to replace with new
     * @param newItem new stuff
     */
    void replace(T oldItem, T newItem) {
        if (isInDatabase(oldItem)) {  // first check if item is in database
            remove(oldItem);  // first remove old item
            add(newItem);  // then replace with new
        }
    }

    // getters

    /**
     * Checks if item is in database
     * @param item item to check
     * @return true iff item is in database
     */
    bool isInDatabase(T item) const {
        for (unsigned int i = 0; i < items.size(); i++) {
            if (items[i] == item) {
                return true;
            }
        }

        return false;
    }

    /**
     * @brief searches in database for given item
     * @return item (if found) or null
     */
    T search(T item) const {
        for (unsigned i = 0; i < items.size(); i++) {
            if (items[i] == item) {
                return items[i];
            }
        }

        return NULL;
    }

    /**
     * @brief gets size of database
     * @return number of items in database
     */
    unsigned long size() const {
        return items.size();
    }

    /**
     * @brief checks if database is empty
     * @return true if number of items is 0
     */
    bool isEmpty() const {
        return items.empty();
    }

    /**
     * @brief getItems gets items in database
     * @return items in database
     */
    std::vector<T> getItems() const {
        return items;
    }

    /**
     * @brief operator << printer
     * @param out stream
     * @param object to print
     * @return print object
     */
    friend std::ostream &operator<<(std::ostream &out, const Database<T> &database) {
        std::vector<T> itemsList = database.getItems();

        out << "Database" << std::endl
            << "[" << std::endl;
        for (typename std::vector<T>::iterator i = itemsList.begin(); i != itemsList.end(); i++) {
            out << "\t" << *i << std::endl;
        }
        out << "]";

        return out;
    }

private:
    std::vector<T> items;
};

#endif // LIBRARYMGR_DATABASE_H
