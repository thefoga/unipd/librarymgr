/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "IOStream.h"

QString IOStream::usersDatabaseFileName = QString::fromUtf8("users.json");
QString IOStream::booksDatabaseFileName = QString::fromUtf8("books.json");
QString IOStream::authDatabaseFileName = QString::fromUtf8("auth.json");

IOStream::IOStream() {
    setupAppDataPathsOrFail();
}

void IOStream::setupAppDataFileNames() {
    appDirPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);  // path to app data directory
    usersDataFilePath = Utils::concatenatePaths(getAppDirPath(), usersDatabaseFileName);
    booksDataFilePath = Utils::concatenatePaths(getAppDirPath(), booksDatabaseFileName);
    authDataFilePath = Utils::concatenatePaths(getAppDirPath(), authDatabaseFileName);
}

bool IOStream::setupAppDataPathsOrFail() {
    setupAppDataFileNames();

    try {
        if (!appDirPath.exists()) {
            appDirPath.mkpath(appDirPath.absolutePath());
        }

        return true;
    } catch (...) {
        return false;
    }
}

const QString IOStream::getAppDirPath() const {
    return appDirPath.absolutePath();
}

const QString IOStream::getUsersDataFilePath() const {
    return usersDataFilePath;
}

const QString IOStream::getBooksDataFilePath() const {
    return booksDataFilePath;
}

const QString IOStream::getAuthDataFilePath() const {
    return authDataFilePath;
}

const QJsonObject IOStream::readFileToJson(QString filePath) const {
    QFile file;  // initialize file
    file.setFileName(filePath);
    file.open(QIODevice::ReadOnly);

    QString fileContent = file.readAll();  // read file content
    file.close();  // close file stream

    return QJsonDocument::fromJson(fileContent.toUtf8()).object();
}

std::vector<User *> IOStream::readUsers(QString filePath) const {
    QJsonArray rawUserArray = readFileToJson(filePath)["root"].toArray();  // get raw user
    std::vector<User *> listOfUsers;

    for (int i = 0; i < rawUserArray.size(); i++) {  // loop through raw users
        listOfUsers.push_back(parseUser(rawUserArray[i].toObject()));  // parse and add to list
    }

    return listOfUsers;
}

std::vector<Book> IOStream::readBooks(QString filePath) const {
    QJsonArray rawBookArray = readFileToJson(filePath)["root"].toArray();  // get raw book
    std::vector<Book> listOfBooks;

    for (int i = 0; i < rawBookArray.size(); i++) {  // loop through raw objects
        listOfBooks.push_back(parseBook(rawBookArray[i].toObject()));  // parse and add to list
    }

    return listOfBooks;
}

OAuth IOStream::readOAuth(QString filePath) const {
    QJsonArray rawOAuthIDArray = readFileToJson(filePath)["root"].toArray();  // get raw list of OAuthId(s)
    OAuth oAuth;  // authentications pool

    for (int i = 0; i < rawOAuthIDArray.size(); i++) {  // loop through raw objects
        oAuth.addId(parseOauthId(rawOAuthIDArray[i].toObject()));  // parse and add to list
    }

    return oAuth;  // build OAuth pool with database
}

void IOStream::writeUsers(std::vector<User *> users, QString filePath) const {
    QJsonObject root;  // root object of json document
    QJsonArray arrayData;  // json array that contains list of users

    for (unsigned int i = 0; i < users.size(); i++) {
        arrayData.append(toJson(users[i]));  // add user to list
    }

    root["root"] = arrayData;  // set root data
    writeFileAsJson(QJsonDocument(root), filePath);  // write to file
}

void IOStream::writeBooks(std::vector<Book> books, QString filePath) const {
    QJsonObject root;  // root object of json document
    QJsonArray arrayData;  // json array that contains list of books

    for (unsigned int i = 0; i < books.size(); i++) {
        arrayData.append(toJson(books[i]));  // add book to list
    }

    root["root"] = arrayData;  // set root data
    writeFileAsJson(QJsonDocument(root), filePath);  // write to file
}

void IOStream::writeOAuth(OAuth oauth, QString filePath) const {
    QJsonObject root;  // root object of json document
    QJsonArray arrayData;  // json array that contains list of credentials
    std::vector<OAuthId> listOfCredentials = oauth.getDatabase().getItems();  // get list of credentials

    for (unsigned int i = 0; i < listOfCredentials.size(); i++) {
        arrayData.append(toJson(listOfCredentials[i]));  // add user to list
    }

    root["root"] = arrayData;  // set root data
    writeFileAsJson(QJsonDocument(root), filePath);  // write to file
}

void IOStream::writeFileAsJson(QJsonDocument jsonData, QString filePath) const {
    QFile file;  // initialize file
    file.setFileName(filePath);
    file.open(QIODevice::WriteOnly);

    file.write(jsonData.toJson());  // write
    file.close();  // close stream
}

const Book IOStream::parseBook(QJsonObject rawBook) const {
    return Book(
            rawBook["isbn"].toString().toStdString(),
            rawBook["title"].toString().toStdString(),
            rawBook["description"].toString().toStdString(),
            rawBook["isAvailable"].toBool(),
            rawBook["isBooked"].toBool()
    );
}

const OAuthId IOStream::parseOauthId(QJsonObject rawOauthId) const {
    return OAuthId(
            rawOauthId["userNameHash"].toString().toUtf8(),
            rawOauthId["passwordHash"].toString().toUtf8()
    );  // parse data
}

User *IOStream::parseUser(QJsonObject rawUser) const {
    QDate userDate = QDateTime::fromString(rawUser["date"].toString(), "yyyy/M/d").date();  // parse date
    bool isAdmin = rawUser["isAdmin"].toBool();  // checks if user is an admin
    User *user = new User(
            rawUser["name"].toString().toStdString(),
            rawUser["surname"].toString().toStdString(),
            userDate,
            rawUser["email"].toString().toStdString()
    );  // parse user data

    if (isAdmin) {
        user = new Admin(user);  // convert to admin
    }

    // parse wish list books data
    QJsonArray wishListData = rawUser["wishlist"].toArray();
    for (int i = 0; i < wishListData.size(); i++) {
        user->addBookToWishList(parseBook(wishListData[i].toObject()));  // update user wishlist
    }

    // parse wish list books data
    QJsonArray loansData = rawUser["loans"].toArray();
    for (int i = 0; i < loansData.size(); i++) {
        user->loans.add(parseBook(loansData[i].toObject()));  // update user loans
    }

    return user;
}

const QJsonObject IOStream::toJson(Book book) const {
    QJsonObject jsonBook;  // create book json object
    jsonBook["isbn"] = QString::fromUtf8(book.getID().c_str());
    jsonBook["title"] = QString::fromUtf8(book.getName().c_str());
    jsonBook["description"] = QString::fromUtf8(book.getDescription().c_str());
    jsonBook["isAvailable"] = book.isAvailableToUsers();
    jsonBook["isBooked"] = book.isBookedByUser();

    return jsonBook;
}

const QJsonObject IOStream::toJson(User *user) const {
    QJsonObject jsonUser;  // create user json object
    jsonUser["name"] = QString::fromUtf8(user->getName().c_str());
    jsonUser["surname"] = QString::fromUtf8(user->getSurname().c_str());
    jsonUser["email"] = QString::fromUtf8(user->getEmail().c_str());
    jsonUser["date"] = user->getDateOfBirth().toString("yyyy/M/d");

    Admin *adminCandidate = dynamic_cast<Admin *> (user);  // check if user is admin
    jsonUser["isAdmin"] = adminCandidate && adminCandidate->isAdmin();

    // book wishlist
    QJsonArray wishListData;  // json array that contains list of books
    std::vector<Book> booksWishList = user->getWishList();
    for (unsigned int i = 0; i < booksWishList.size(); i++) {
        wishListData.append(toJson(booksWishList[i]));
    }
    jsonUser["wishlist"] = wishListData;

    // loans
    QJsonArray loans;  // json array that contains list of books
    std::vector<Book> loansList = user->getLoans();
    for (unsigned int i = 0; i < loansList.size(); i++) {
        loans.append(toJson(loansList[i]));
    }
    jsonUser["loans"] = loans;

    return jsonUser;
}

const QJsonObject IOStream::toJson(OAuthId id) const {
    QJsonObject jsonOAuthId;  // create user json object
    jsonOAuthId["userNameHash"] = id.getUserHash();
    jsonOAuthId["passwordHash"] = id.getPasswordHash();

    return jsonOAuthId;
}
