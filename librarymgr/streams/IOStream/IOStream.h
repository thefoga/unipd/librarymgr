/**
 * Copyright 2016-2017 Stefano Fogarollo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef LIBRARYMGR_IOSTREAM_H
#define LIBRARYMGR_IOSTREAM_H


#include <QtCore/QFile>
#include <QApplication>
#include <QStandardPaths>
#include <QDir>
#include <QString>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>

#include "../../utils/utils/Utils.h"
#include "../../models/Database/Database.h"
#include "../../utils/SmartPointer/SmartPointer.h"
#include "../../models/People/User/User.h"
#include "../../models/People/Admin/Admin.h"
#include "../../models/Article/Book/Book.h"
#include "../../auth/OAuth/OAuth.h"

/**
 * Provides methods to load/save app files
 */
class IOStream {
public:
    /**
     * Streamer for app
     * @return IO app streams manager
     */
    IOStream();

    // getters

    /**
     * Gets path to app data directory
     * @return path to app data directory
     */
    const QString getAppDirPath() const;

    /**
     * Gets path to app users database
     * @return path to app users database
     */
    const QString getUsersDataFilePath() const;

    /**
     * Gets path to app books database
     * @return path to app books database
     */
    const QString getBooksDataFilePath() const;

    /**
     * Gets path to app authentications database
     * @return path to app authentications database
     */
    const QString getAuthDataFilePath() const;

    /**
     * Possible data to read
     */
    enum DATA_FILES {
        USERS = 0,
        BOOKS = 1,
        AUTH = 2
    };

    // read

    /**
     * Retrieves users data from database
     * @param filePath path of file to read
     * @return list of users in database
     */
    std::vector<User *> readUsers(QString filePath) const;

    /**
     * Retrieves books data from database
     * @param filePath path of file to read
     * @return list of books in database
     */
    std::vector<Book> readBooks(QString filePath) const;

    /**
     * Retrieves credentials data from database
     * @param filePath path of file to read
     * @return list of user credentials database
     */
    OAuth readOAuth(QString filePath) const;

    /**
     * Reads file in json format.
     * @param filePath path of file to read
     * @return json representation of file.
     */
    const QJsonObject readFileToJson(QString filePath) const;

    // write

    /**
     * Saves users data to database
     */
    void writeUsers(std::vector<User *> users, QString filePath) const;

    /**
     * Saves books data to database
     */
    void writeBooks(std::vector<Book> books, QString filePath) const;

    /**
     * Saves user credentials data to database
     */
    void writeOAuth(OAuth oauth, QString filePath) const;

    /**
     * Saves json data in json format.
     * @param jsonData data to save.
     * @param filePath path of file to write
     */
    void writeFileAsJson(QJsonDocument jsonData, QString filePath) const;

private:
    static QString usersDatabaseFileName;  // name of file of users database
    static QString booksDatabaseFileName;  // name of file of books database
    static QString authDatabaseFileName;  // name of file of authentications database

    QDir appDirPath;  // path to app data directory
    QString usersDataFilePath;  // path to app users database
    QString booksDataFilePath;  // path to app books database
    QString authDataFilePath;  // path to app authentications database

    /**
     * Prepares app data dirs and files names
     */
    void setupAppDataFileNames();

    /**
     * Prepares app data dirs and files or fail
     * @return true if everything went well
     */
    bool setupAppDataPathsOrFail();

    // parsers

    /**
     * Parses raw book and builds object
     * @param rawBook raw book to parse
     * @return creates object from json representation
     */
    const Book parseBook(QJsonObject rawBook) const;

    /**
     * Parses raw oauthid and builds object
     * @param rawOauthId raw json to parse
     * @return creates object from json representation
     */
    const OAuthId parseOauthId(QJsonObject rawOauthId) const;

    /**
     * Parses raw user and builds object
     * @param rawUser raw json to parse
     * @return creates object from json representation
     */
    User *parseUser(QJsonObject rawUser) const;

    // converters

    /**
     * Converts object to its json representation
     * @param book object to convert
     * @return json representation of object
     */
    const QJsonObject toJson(Book book) const;

    /**
     * Converts object to its json representation
     * @param book object to convert
     * @return json representation of object
     */
    const QJsonObject toJson(User *user) const;

    /**
     * Converts object to its json representation
     * @param book object to convert
     * @return json representation of object
     */
    const QJsonObject toJson(OAuthId id) const;
};

#endif //LIBRARYMGR_IOSTREAM_H
